//
//  AppDelegate+APNS.swift
//  imCelebrity
//
//  Created by Paramita  on 22/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import PushKit
import UIKit

extension AppDelegate{
    //Mark: VOIP PUSH
    
    func voipRegistration() {
        let mainQueue = DispatchQueue.main
        // Create a push registry object
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
        // Set the registry's delegate to self
        voipRegistry.delegate = self
        // Set the push type to VoIP
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenForType type: PKPushType) {
        print( "Invalid" )
    }
    // Handle updated push credentials
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let tokenString = String(data: pushCredentials.token, encoding: .utf8)
        print( "Credentials \(tokenString ?? "0")" )
        let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        UserDefaults.standard.set(token , forKey: Constant.UserDefaultKeyName.APNSToken)
        print(token)
    }
}

extension AppDelegate: PKPushRegistryDelegate{
    // Handle incoming pushes
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        print(payload.dictionaryPayload)
        print("Push Recieved Data : \(payload.dictionaryPayload.description)")
        
        if UIApplication.shared.applicationState != .active {
            if payload.dictionaryPayload["notificationType"] as? String == "call"
            {
                if let callBlock = payload.dictionaryPayload["callBlock"] as? NSDictionary
                {
                    let notification = UILocalNotification()
                    notification.fireDate = Date()
                    notification.alertTitle = (callBlock["calltype"] as? String)!.uppercased() + " CALL"
                    notification.alertBody = (callBlock["callersername"] as? String)! + " calling..."
                    notification.soundName = "iphone_8_8_plus_ring.mp3"//UILocalNotificationDefaultSoundName
                    notification.userInfo = callBlock as? [AnyHashable : Any]
                    notification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                    UIApplication.shared.scheduleLocalNotification(notification)
                }
            }
            else if payload.dictionaryPayload["notificationType"] as? String == "GOAL" {
                let notification = UILocalNotification()
                notification.fireDate = Date()
                if let message = payload.dictionaryPayload["message"] as? String {
                    notification.alertBody = message
                } else {
                    notification.alertBody = "Goal reminder notification"
                }
                if let goalTitle = payload.dictionaryPayload["goaltitle"] as? String {
                    notification.alertTitle = goalTitle
                } else {
                    notification.alertTitle = "ImCelebrity"
                }
                notification.userInfo = payload.dictionaryPayload
                notification.soundName = UILocalNotificationDefaultSoundName
                notification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                UIApplication.shared.scheduleLocalNotification(notification)
            }
            else if payload.dictionaryPayload["notificationType"] as? String == "FEED" {
                let notification = UILocalNotification()
                notification.fireDate = Date()
                if let message = payload.dictionaryPayload["chatText"] as? String {
                    notification.alertBody = message
                } else {
                    notification.alertBody = "A new Golive has started"
                }
                notification.alertTitle = "ImCelebrity"
                notification.userInfo = payload.dictionaryPayload
                notification.soundName = UILocalNotificationDefaultSoundName
                notification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                UIApplication.shared.scheduleLocalNotification(notification)
            }
            else
            {
                let notification = UILocalNotification()
                notification.fireDate = Date()
                notification.alertTitle = payload.dictionaryPayload["username"] as? String
                notification.alertBody = payload.dictionaryPayload["chatText"] as? String
                notification.soundName = UILocalNotificationDefaultSoundName
                notification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                UIApplication.shared.scheduleLocalNotification(notification)
            }
        }
        else
        {
            print("Application Active")
        }
    }
    
}
