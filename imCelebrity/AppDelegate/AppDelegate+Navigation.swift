//
//  AppDelegate+Extention.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate{
    
    func validateUserDetailsAndMoveToParticularVC(){
        self.moveToDashboardVC()
    }
    
    
    
    func moveToDashboardVC(){
        if let wd = UIApplication.shared.delegate?.window {
            var vc = wd!.rootViewController
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).visibleViewController
            }
            if(!(vc is DashboardVC)){
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let objDashboardViewController = mainStoryboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                let rootViewController = self.window!.rootViewController as! UINavigationController
                rootViewController.pushViewController(objDashboardViewController, animated: true)
            }
        }
    }
}
