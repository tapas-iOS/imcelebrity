//
//  AppDelegate+SocialLogin.swift
//  SocialLogin
//
//  Created by Weaver web 5 on 31/01/19.
//  Copyright © 2019 SocialLogin. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FirebaseAuth
import GoogleSignIn


extension AppDelegate: GIDSignInDelegate {
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
        -> Bool {
            let whetherGoogleCanHandle = GIDSignIn.sharedInstance().handle(url, sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
            
            let whetherFacebookCanHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation])
            return (whetherFacebookCanHandle||whetherGoogleCanHandle)
            
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            print("\(error)")
            return
        }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("\(String(describing: error))")
    }
}
