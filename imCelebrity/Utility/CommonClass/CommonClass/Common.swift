//
//  Common.swift
//  DemoApp
//
//  Created by cn02 on 24/08/18.
//  Copyright © 2018 Tapas. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire

var loadingBackground = UIView()
var indicator = UIActivityIndicatorView()
var loadingText = UILabel()
var loadingView = CustomLoaderClass()
var COUNTRY_ISD_ARRAY : [NSDictionary] = []

enum PhotoPickerType{
    case camera
    case library
}

struct TextInputAttributes {
    var placeholderText: String
    var textFieldText: String
    var keyboardType: UIKeyboardType
    var isSecureTextEntry: Bool
    var errorMessage: String
    var returnKey: UIReturnKeyType
    var isViewPasswordButton: Bool
    var isShowCalenderButton: Bool
    var isShowCountryCodes: Bool
    var isShowDropdownArrow: Bool
}
struct CheckBoxAttributes {
    var text: String
    var status: Bool
}
func changeDateTimeFormat( inputDateTime : Date, outPutTimeZone : TimeZone, outPutFormat : String ) -> String {
    let outPutDateFormatter = DateFormatter()
    outPutDateFormatter.timeZone = outPutTimeZone
    outPutDateFormatter.dateFormat = outPutFormat
    let outputDateTimeString = outPutDateFormatter.string(from: inputDateTime)
    return outputDateTimeString
}



class CommonClass: NSObject {
    
    struct ScreenSize {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_XS_MAX         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH   == 1024.0
    }
    
    struct iOSVersion {
        static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
        static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
        static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
        static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
        static let iOS10 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 11.0)
        static let iOS11 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 12.0)
        static let iOS12 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT == 12.0)
    }
    
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    
    func DPrint(_ items: Any...) {
        #if DEBUG
        Swift.print(items[0])
        #endif
    }
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
        
    }
    
    //MARK: Showing loader
    class func addLoading(view:UIView) -> Void {
        
        loadingView = CustomLoaderClass(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(173), height: CGFloat(131)))
        loadingView.layer.cornerRadius = 10.0
        loadingView.layer.masksToBounds = true
        let image = UIImage.gifImageWithName("loading_2")
        loadingView.customViewImageOutlet.image = image
        view.addSubview(loadingView)
        loadingView.center = view.center
        view.isUserInteractionEnabled = false
        
    }
    
    class func removeLoading(view:UIView) -> Void {
        loadingView.removeFromSuperview()
        loadingView.customViewImageOutlet.stopAnimating()
        view.isUserInteractionEnabled = true
        
    }
    
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func appVersion() -> String{
        if(CommonClass.isKeyPresentInUserDefaults(key: Constant.UserDefaultKeyName.AppVersion)){
            return CommonClass.userDefaultValueGet( Constant.UserDefaultKeyName.AppVersion)
        }else{
            return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!
        }
    }
    
    class func headerDetails() -> HTTPHeaders{
        let basicAuthToken = CommonClass.userDefaultValueGet(Constant.UserDefaultKeyName.AccessToken)
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "x-access-token": basicAuthToken
        ]
        
        return headers
    }
    
    class func getDeviceToken() -> String{
        if(CommonClass.isKeyPresentInUserDefaults(key: Constant.UserDefaultKeyName.FcmToken)){
            return CommonClass.userDefaultValueGet(Constant.UserDefaultKeyName.FcmToken)
        }else{
            return ""
        }
    }
    
    
    
    class func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    class func userDefaultValueGet(_ key: String) -> String {
        return (UserDefaults.standard.value(forKey: key) as! String)
    }
    
    static func getCountries() -> [Country]{
        if let fileURL = Bundle.main.url(forResource: "countryCodes", withExtension: "json"){
            do {
                let data = try Data(contentsOf: fileURL)
                do {
                    let allCountryResponse = try JSONDecoder().decode([Country.Response].self, from: data)
                    let countries = allCountryResponse.map { (eachCountryResponse) -> Country in
                        return Country.init(from: eachCountryResponse)
                    }
                    return countries
                }
                catch{
                    print("error to decode = \(error.localizedDescription)")
                    return []
                }
            }
            catch {
                print("error to get file URL; error = \(error.localizedDescription)")
                return []
            }
        }
        return []
    }
    
    class func isALoginUser() -> Bool{
        if(CommonClass.isKeyPresentInUserDefaults(key: Constant.UserDefaultKeyName.AccessToken)){
            return true
        }else{
            return false
        }
    }
}


