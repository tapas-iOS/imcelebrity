//
//  APIConstants.swift
//  FollowUp
//
//  Created by SumitavaDatta on 08/02/18.
//  Copyright © 2018 CapitalNumbers. All rights reserved.
//

import Foundation
struct APIConstants {
    
    static let BaseURL                  = "https://admin.imcelebrity.com/apicelebrity/"  //Live
    //static let BaseURL                  = ""  //Development For Our Scale
    //static let BaseURL                      = "" //Development Server URL But Replica as Live server.
    static let DeviceType               = "iOS"
    
    struct APIServicePath{
        static let Login                            = BaseURL + "login"
        static let SocialLogin                  = BaseURL + "socialSignup"
        static let VerifyOTP                    = BaseURL + "verifyOtp"
        static let EmailSignup                = BaseURL + "signupUser"
        static let RsendOTP                    = BaseURL + "resend OTP"

    }
    
    struct StaticURL {
        static let TermsAndCondition            = "https://www.google.com/"
    }
    
    
    struct RequestParameters {
        
        struct SocialLogin {
            static let Email                          = "email"
            static let Name                         = "name"
            static let AppType                    = "apptype"
            static let DeviceToken              = "devicetoken"
            static let UserType                   = "user_type"
            static let ImageURL                  = "image_url"
            static let socialID                      = "social_id"
            static let sex                              = "sex"
            static let socialType                  = "social_type"
        }
        
        struct EmailSignup {
            static let Name                         = "name"
            static let Mobile                    = "mobile"
            static let CountryCode              = "country_code"
            static let AppType                   = "apptype"
            static let DoB                  = "dob"
            static let Password                      = "password"
        }

        
        struct Login {
            static let Email                = "email"
            static let Password             = "password"
            static let AppType           = "apptype"
//            static let DeviceToken          = "devicetoken"
            static let CountryCode          = "country_code"
        }
        
        struct VerifyOTP {
            static let UserId                             = "userId"
            static let VerificationCode             = "verificationCode"
            static let AppType                          = "apptype"
            static let DeviceToken                   = "devicetoken"
        }
        
        struct ResendOTP {
            static let mobileNumber                             = "mobile"
            static let countryCode             = "Country_code"
            static let SendSms                          = "sendSms"
        }
                
        
        
    }
    
//    struct ResponseParameters {
//        
//        struct Login {
//            static let Email                = ""
//        }
//        
//        struct Signup {
//            static let Email                = ""
//        }
//        
//        struct ForgotPassword {
//            static let Email                = ""
//        }
//        
//        struct MyProfile {
//            static let Email                = ""
//        }
//        
//        struct EditProfile {
//            static let Email                = ""
//        }
//        
//    }
    
    
}
