//
//  AlertConstant.swift
//  FollowUp
//
//  Created by SumitavaDatta on 05/02/18.
//  Copyright © 2018 CapitalNumbers. All rights reserved.
//

import Foundation
struct AlertConstants {
    
    struct SignupAlertController {
        static let ChooseImage              = "Choose Image from"
        static let OpenCamera               = "Camera"
        static let OpenGallery              = "Gallery"
        static let Cancel                   = "Cancel"
    }
    
    struct LoginAlertController {
        static let AlertTitleForgetPassword            = "Forget Password"
        static let AlertButtonTitleSubmit              = "Submit"
        static let AlertButtonTitleCancel              = "Cancel"
    }
    
    struct LogoutAlertController {
        static let AlertTitleLogout                 = "Are you sure?"
        static let AlertButtonTitleOk               = "Yes, Logout"
        static let AlertButtonTitleCancel           = "Cancel"
    }
    
    struct SessionExpireAlert {
        static let AlertTitleSessionExpire          = "Your session has expire. Please login again."
        static let AlertButtonTitleOk               = "Login"
    }
}
