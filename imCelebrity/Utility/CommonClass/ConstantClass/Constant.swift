//
//  Constant.swift
//  DemoApp
//
//  Created by cn02 on 24/08/18.
//  Copyright © 2018 Tapas. All rights reserved.
//

import Foundation

struct Constant {
    static let AppName = "imCelebrity"//Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
    static let BundleID = Bundle.main.bundleIdentifier!
    static let NoInternentMessgae = "No internet connection. Make sure Wi-Fi or cellular data is turned on, then try again."
    static let userDefaultIcon = "upload_img_placeholder"
    static let UnderDevelopmetAlert = "Under development"
    static let BlankDetailsText     = "N/A"
    static let appTypeiOS = "IOS"
    
    struct APIName {
        static let AccessToken = "AccessToken"
        static let Login = "login"
        static let Registration = "Registration"
        static let CartList = "getCartList"
        static let Logout = "Logout"
        static let PlaceOrderToken = "GetPlaceOrderToken"
        static let PlaceOrder = "PlaceOrder"
    }
    
    struct UserDefaultKeyName {
        static let UserID                               = "UserID"
        static let UserName                         = "UserName"
        static let UserImageUrl                    = "UserImageUrl"
        static let UserEmail                            = "UserEmail"
        static let AccessToken                      = "AccessToken"
        static let CountryCode                      = "CountryCode"
        static let PhoneNumber                      = "PhoneNumber"

        static let AppVersion                       = "AppVersion"
        static let FcmToken                         = "FCMToken"
        static let APNSToken                         = "APNSToken"
    }
}
