//
//  LoginConstants.swift
//  FollowUp
//
//  Created by SumitavaDatta on 06/02/18.
//  Copyright © 2018 CapitalNumbers. All rights reserved.
//

import Foundation
struct LoginConstants {
    
    struct LabelName {
        static let EMAILID              = "EMAIL ID"
        static let PASSWORD             = "PASSWORD"
    }
    
    struct PlaceHolderText {
        static let Email                = "Email"
        static let Password             = "Password"
        static let ForgetPassword       = "Forget Password"
    }
    
    struct ButtonTitle{
        
    }
    
    struct ErrorConstant{
        static let ErrorKeyLoginEmptyEmail          = "Please enter you email id"
        static let ErrorKeyLoginEmptyPassword       = "Please enter your password"
        static let ErrorKeyLoginValidationPassword  = "Password must be greater than 8 charcters"
        static let ErrorKeyLoginValidationEmail     = "Please enter a valid email address"
        static let ErrorKeySocialFBError            = "Facbook Login Error"
        static let ErrorKeySocialFBCancelDialog     = "Cancel Facbook dialog"
        static let ErrorKeySocialIDEmpty            = "Cannot Fetch Your account details"
        static let ErrorKeySocialTokenEmpty         = "Cannot Fetch Your account Access token"
        
    }
}
