//
//  SignUpConstants.swift
//  FollowUp
//
//  Created by SumitavaDatta on 05/02/18.
//  Copyright © 2018 CapitalNumbers. All rights reserved.
//

import Foundation

struct SignUpConstants {
    
    struct LabelName {
        //static let FullName             = "Full Name"
        static let FirstName            = "First Name"
        static let MiddleName           = "Middle Name"
        static let LastName             = "Last Name"
        static let Email                = "Email Id"
        static let Password             = "Password"
        static let PhoneNumber          = "Phone Number"
        static let PrefferedUnits       = "Preffered Units"
        static let Gender               = "Gender"
        //static let DOB                  = "DOB"
        static let Age                  = "Age"
        static let Height               = "Height"
        static let DesiredWeight        = "Desired Weight"
        static let TimeToloseWeight     = "Time To Lose Weight"
        static let ScaleMACAddress      = "Scale Id"
        static let WeightManagementGoal = "Weight Management Goal"
        static let UserSelection = "Desired Weight Selection"
    }
    
    struct PlaceHolderText {
        //static let FullName             = "Enter Full Name"
        static let FirstName            = "Enter First Name"
        static let MiddleName           = "Enter Middle Name"
        static let LastName             = "Enter Last Name"
        static let Email                = "Enter Email Id"
        static let Password             = "Enter Password"
        static let PhoneNumber          = "Enter Phone Numbers"
        static let PrefferedUnits       = "Select Preffered Units"
        static let Gender               = "Select Gender"
        //static let DOB                  = "Select DOB"
        static let Age                  = "Enter Age"
        static let Height               = "Select Height"
        static let DesiredWeight        = "Desired Weight"
        static let TimeToloseWeight     = "Enter Lose Weight Time"
        static let ScaleMACAddress      = "Enter your scale Id"
        static let WeightManagementGoal = "Select Weight Management Goal"
        static let UserSelection        = "Select Your Input Choice"
    }
    
    struct ErrorConstant{
        
        //static let ErrorKeySignUpEmptyFullName              = "Please enter your name"
        static let ErrorKeySignUpEmptyFirstName              = "Please enter your first name"
        static let ErrorKeySignUpEmptyMiddleName              = "Please enter your middle name"
        static let ErrorKeySignUpEmptyLastName              = "Please enter your last name"
        static let ErrorKeySignUpEmptyEmail                 = "Please enter your email"
        static let ErrorKeySignUpValidationEmail            = "Please enter a valid email address"
        static let ErrorKeySignUpEmptyPassword              = "Please enter your password"
        static let ErrorKeySignUpValidationPassword         = "Password must be more than 8 characters"
        static let ErrorKeySignUpEmptyPhoneNumber           = "Please enter your phone number"
        static let ErrorKeySignUpEmptyScaleId               = "Please enter your scale Id"
        static let ErrorKeySignUpEmptyWeightManagementGoal              = "Please select any option"
        static let ErrorKeySignUpEmptyUserChoice              = "Please select any option"
        static let ErrorKeySignUpValidationScaleId          = "Scale id must be contains 10 degit numeric number"
        static let ErrorKeySignUpEmptyPreffredUnits         = "Please select your Preffered Units"
        static let ErrorKeySignUpEmptyGender                = "Please select any gender type."
        static let ErrorKeySignUpValidationPhone            = "Please enter a valid phone number"
        static let ErrorKeySignUpEmptyDOB                   = "Please select your DOB"
        static let ErrorKeySignUpEmptyHeight                = "Please enter your height"
        static let ErrorKeySignUpEmptyDesiredWeight         = "Please enter your desired weight"
        static let ErrorKeySignUpEmptyTimeToLoseWeight      = "Please select your time to lose weight"
        static let ErrorKeySignUpNotSelectTermsAndCondition      = "Please accept Terms & Condition"
        static let ErrorKeySignUpSelectZeroWeightLoseTime                 = "Please select any proper weight lose time"
    }

}
