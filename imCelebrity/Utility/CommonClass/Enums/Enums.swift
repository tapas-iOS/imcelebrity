//
//  Enums.swift
//  DemoApp
//
//  Created by cn02 on 24/08/18.
//  Copyright © 2018 Tapas. All rights reserved.
//

import Foundation

struct Enums{
    enum CellButtonName: String{
        case Signup = "Signup"
        case Login = "Login"
    }
    
    
    enum DeviceType: String {
        case iOS = "IOS"
        case Android = "2"
    }
    
    struct OTPVerifiction {
        enum ButtonTitle: String{
            case Verify =  "VERIFY"
            case ResendOTP =  "RESEND"
        }
    }
    
    enum DashboardCellType: Int {
        case Video = 1
        case Snap
    }
    enum FooterPopupType: Int {
        case Dashboard = 1
        case Connects
    }
    enum FilterPopupType: Int {
        case Connects = 1
        case Fans
        case Notifications
    }
    
}
