//
//  ApiCalling+Extension.swift
//  Phylam
//
//  Created by CN138 on 05/02/18.
//  Copyright © 2018 Phylam. All rights reserved.
//

import Foundation
import Alamofire

extension ApiCalling {
    
    
    class func postResponce(urlString: String, parameter: [String : Any], completion: @escaping (DataResponse<String>) -> Void) {
        Alamofire.request(urlString, method: .post, parameters: parameter).responseString { (response) in
           completion(response)
        }
    }

    // MARK: Login API request.
    class func userLoginCheck(emailId : String, userPassword : String , completion: @escaping ( _ result: Any?, _ error : Error?) -> Void) ->Void{
        
        let paraDic = [APIConstants.RequestParameters.Login.Email: emailId ,
                       APIConstants.RequestParameters.Login.Password: userPassword,
                       APIConstants.RequestParameters.Login.AppType: Enums.DeviceType.iOS.rawValue,
                       APIConstants.RequestParameters.Login.CountryCode: ""] as [String : Any]
        
        let urlString : String = APIConstants.APIServicePath.Login
        self.printAPIDetails(strAPIFuncName: "User Login Check", strAPI: urlString, parameters: paraDic, headers: nil)
        
        self.fetchJson(urlString: urlString, methodType:.post, param: paraDic, headerInfo : nil) {
            (data, er) in completion(data,er)
        }
        
//        self.postResponce(urlString: urlString, parameter: paraDic) { (response) in
//            guard let data = response.data else { return }
//            do {
//                let decoder = JSONDecoder()
//                let loginResponce = try decoder.decode(LoginModel.Login.self, from: data)
//                completion(loginResponce)
//            } catch let error {
//                print(error)
//                completion(nil)
//            }
//        }
    }
    

    class func userSocialSignupCheck(name : String, email : String, imageUrl: String, socialID: String, sex: String, socialType: String , completion: @escaping ( LoginModel.Login?) -> Void) {
        
        let paraDic = [APIConstants.RequestParameters.SocialLogin.Name: name ,
                       APIConstants.RequestParameters.SocialLogin.Email: email,
                       APIConstants.RequestParameters.SocialLogin.ImageURL: imageUrl,
                       APIConstants.RequestParameters.SocialLogin.socialID: socialID,
                       APIConstants.RequestParameters.SocialLogin.sex: sex,
                       APIConstants.RequestParameters.SocialLogin.socialType: socialType,
                       APIConstants.RequestParameters.SocialLogin.UserType: "Social",
                       APIConstants.RequestParameters.SocialLogin.AppType: Enums.DeviceType.iOS.rawValue,
                       APIConstants.RequestParameters.SocialLogin.DeviceToken: CommonClass.getDeviceToken()
            ] as [String : Any]
        
        let urlString : String = APIConstants.APIServicePath.SocialLogin
        self.printAPIDetails(strAPIFuncName: "User Social Signup Check", strAPI: urlString, parameters: paraDic, headers: nil)
        
        self.postResponce(urlString: urlString, parameter: paraDic) { (response) in
            guard let data = response.data else { return }
            do {
                let decoder = JSONDecoder()
                let loginResponce = try decoder.decode(LoginModel.Login.self, from: data)
                completion(loginResponce)
            } catch let error {
                print(error)
                completion(nil)
            }
        }
    }
    
    class func userEmailSignupCheck(name : String, mobile : String, countryCode: String, appType: String, DoB: String, password: String , completion: @escaping ( _ result: Any?, _ error : Error?) -> Void) ->Void {

        let paraDic = [APIConstants.RequestParameters.EmailSignup.Name: name ,
                       APIConstants.RequestParameters.EmailSignup.Mobile: mobile,
                       APIConstants.RequestParameters.EmailSignup.CountryCode: countryCode,
                       APIConstants.RequestParameters.EmailSignup.AppType: appType,
                       APIConstants.RequestParameters.EmailSignup.DoB: DoB,
                       APIConstants.RequestParameters.EmailSignup.Password: password
            ] as [String : Any]
        
        let urlString : String = APIConstants.APIServicePath.EmailSignup
        self.printAPIDetails(strAPIFuncName: "User Email Signup Check", strAPI: urlString, parameters: paraDic, headers: nil)
        
        self.fetchJson(urlString: urlString, methodType:.post, param: paraDic, headerInfo : nil) {
            (data, er) in completion(data,er)
        }
        
//        self.postResponce(urlString: urlString, parameter: paraDic) { (response) in
//            guard let data = response.data else { return }
//            do {
//                let decoder = JSONDecoder()
//                let loginResponce = try decoder.decode(LoginModel.Login.self, from: data)
//                completion(loginResponce)
//            } catch let error {
//                print(error)
//                completion(nil)
//            }
//        }
    }
    
    
    
    class func userOTPCheck(strOTP : String, completion: @escaping ( _ result: Any?, _ error : Error?) -> Void) ->Void {
        
        let strUserID: String = "5c8bd980ee4b8974694c2485" //String(CommonClass.userDefaultValueGet(Constant.UserDefaultKeyName.UserID))
        
        let paraDic = [APIConstants.RequestParameters.VerifyOTP.UserId: strUserID ,
                       APIConstants.RequestParameters.VerifyOTP.VerificationCode: strOTP,
                       APIConstants.RequestParameters.VerifyOTP.AppType: Enums.DeviceType.iOS.rawValue,
                       APIConstants.RequestParameters.VerifyOTP.DeviceToken: "",
                       ] as [String : Any]
        
        let urlString : String = APIConstants.APIServicePath.VerifyOTP
        
        self.printAPIDetails(strAPIFuncName: "User OTP Validation", strAPI: urlString, parameters: paraDic, headers: CommonClass.headerDetails())
        
        self.fetchJson(urlString: urlString, methodType:.post, param: paraDic, headerInfo : CommonClass.headerDetails()) {
            (data, er) in completion(data,er)
        }
    }
    
    class func resendOTP(phoneNumber : String, countryCode: String, completion: @escaping ( _ result: Any?, _ error : Error?) -> Void) ->Void {

        let paraDic = [APIConstants.RequestParameters.ResendOTP.countryCode: countryCode ,
                       APIConstants.RequestParameters.ResendOTP.mobileNumber: phoneNumber,
                       APIConstants.RequestParameters.ResendOTP.SendSms : "RESEND"
            ] as [String : Any]
        
        let urlString : String = APIConstants.APIServicePath.RsendOTP
        
        self.printAPIDetails(strAPIFuncName: "User OTP Validation", strAPI: urlString, parameters: paraDic, headers: CommonClass.headerDetails())
        
        self.fetchJson(urlString: urlString, methodType:.post, param: paraDic, headerInfo : CommonClass.headerDetails()) {
            (data, er) in completion(data,er)
        }
    }
    
    
    class func printAPIDetails(strAPIFuncName: String, strAPI: String, parameters: [String : Any]?, headers: HTTPHeaders?){
        print("***************************************************************************")
        print("*************************************************************************** \n \n")
        
        print("API Calling from Method::::   \(strAPIFuncName)\n")
        print("API Name::::   \(strAPI)\n")
        print("Headers Details::::   \(headers ?? [:]) \n")
        print("Parameter Details::::   \(parameters ?? [:])\n")
        
        print("***************************************************************************")
        print("*************************************************************************** \n \n")
        
    }
    
}


//    class func signupWithEmail(userFirstName firstName : String, userMiddleName middleName : String, userLastName lastName : String, userEmailId emailId : String , userPassword password : String, userPhoneNumber phoneNumber : String, userWeightManagementGoal weightManagementGoal : String, userSelectedChoice selectedChoice : String, userPrefferedUnit prefferedUnit: String, scaleMacAddress scaleMacId: String, userGender gender : String, userDOB dob: String,userCurrentHeight currentHeight: String, userTargetWeight targetWeight: String, userRequredTime requredTime : String, userImageData imageData: Data ,completion: @escaping ( _ result: Any?, _ error : Error?) -> Void) ->Void {
//        
//        
//        let paraDic = [APIConstants.RequestParameters.EmailSignup.FirstName: firstName ,
//                       APIConstants.RequestParameters.EmailSignup.MiddleName: middleName ,
//                       APIConstants.RequestParameters.EmailSignup.LastName: lastName ,
//                        APIConstants.RequestParameters.EmailSignup.EmailId: emailId,
//                        APIConstants.RequestParameters.EmailSignup.Password: password,
//                        APIConstants.RequestParameters.EmailSignup.PhoneNumber: phoneNumber,
//                        APIConstants.RequestParameters.EmailSignup.PrefferedUnits: prefferedUnit,
//                        APIConstants.RequestParameters.EmailSignup.ScaleMacId: scaleMacId,
//                        APIConstants.RequestParameters.EmailSignup.Gender: gender,
//                        APIConstants.RequestParameters.EmailSignup.DOB: dob,
//                        APIConstants.RequestParameters.EmailSignup.Height: currentHeight,
//                        APIConstants.RequestParameters.EmailSignup.DesiredWeight: targetWeight,
//                        APIConstants.RequestParameters.EmailSignup.TimeToloseWeight: requredTime,
//                        APIConstants.RequestParameters.EmailSignup.WeightManagementGoal: weightManagementGoal,
//                        APIConstants.RequestParameters.EmailSignup.UserChoice: selectedChoice,
//                        APIConstants.RequestParameters.EmailSignup.DeviceType: Enums.DeviceType.iOS.rawValue,
//                        APIConstants.RequestParameters.EmailSignup.DeviceToken: CommonClass.getDeviceToken()
//            ] as [String : Any]
//        
//        let urlString : String = APIConstants.APIServicePath.Signup
//        
//        self.printAPIDetails(strAPIFuncName: "Signup With Email", strAPI: urlString, parameters: paraDic, headers: nil)
//        
//        let imageFileName = "SureFiz" + "_iOS_user_image.jpeg"
//
//        self.uploadFile(urlString: urlString, _data: imageData, _filePath: APIConstants.RequestParameters.EmailSignup.UserImage, _fName: imageFileName, _fType: "image/*", _method: .post, _param: paraDic, _header: nil) { (data, er) in
//            completion(data,er)
//        }
//    }


