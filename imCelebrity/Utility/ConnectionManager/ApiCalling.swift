import UIKit
import Alamofire

class ApiCalling: NSObject {
    private override init() {
        
    }
    
    var strDeviceTypeId = "iOS"
    
    class func fetchJson(urlString : String?, methodType : HTTPMethod? , param : Dictionary<String,Any>? , headerInfo :  Dictionary<String,String>? , completion: @escaping (_ result: Any?, _ error : Error?) -> Void)
    {
        guard self.validateURL(urlString: urlString) else {
            completion(nil, NSError(domain: "Invalid url String", code: 0, userInfo: nil))
            return
        }
        // Add further url verification codes here
        let _ =  self.fetchDataRequest(urlString : urlString!, methodType:methodType , parameters:param,headerInfo : headerInfo,shouldStartImmediately:true){ (data, er) in
            completion(data,er)
        }
    }
    
    //MARK:- FILE_UPLOAD_TASK
    class func uploadFile(urlString : String?,_data: Data,_filePath:String?,_fName: String?,_fType: String?,_method: HTTPMethod?, _param:Dictionary<String,Any>?, _header:HTTPHeaders?,completion: @escaping (_ result: Any?, _ error : Error?) -> Void){
        
        guard self.validateURL(urlString: urlString) else {
            completion(nil, NSError(domain: "Invalid url String", code: 0, userInfo: nil))
            
            return
        }
        
        self.getUploadTask(url: urlString!, data: _data, filePath: _filePath, fName: _fName, fType: _fType, method: _method, param: _param, header: _header){ (data, er) in
            
            completion(data,er)
        }
        
    }
    
    //MARK:- JSON_DATA_TASK url
    class func fetchDataRequest(urlString : String,
                                methodType : HTTPMethod?,
                                parameters:Dictionary<String,Any>?,
                                headerInfo:HTTPHeaders?,
                                shouldStartImmediately : Bool,
                                completion: @escaping (_ result: Any?, _ error : Error?) -> Void) -> DataRequest?
    {
        let mType:HTTPMethod = methodType!
        //.validate(contentType: ["application/json"])
        let dataReq : DataRequest
        switch mType {
        case HTTPMethod.get :
            dataReq = Alamofire.request(urlString,
                                        method: mType,
                                        parameters: parameters,
                                        encoding: URLEncoding.httpBody,//JSONEncoding.default,
                                        headers: headerInfo)
                //            .validate(statusCode: 200..<300)
                //.validate(contentType: ["application/json"])
                .responseString(completionHandler: { responseString in
                    print(responseString)
                })
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        if let JSON = response.result.value {
                            
                            let jsonResult : NSDictionary = JSON as! NSDictionary
                            completion(jsonResult, nil)
                        }
                    case .failure(let error):
                        
                        
                        completion(nil, error)
                    }
            }
            if(shouldStartImmediately)
            {
                dataReq.resume()
            }
            return dataReq
        case HTTPMethod.post :
            dataReq = Alamofire.request(urlString,
                                        method: mType,
                                        parameters: parameters,
                                        encoding: URLEncoding.httpBody,//JSONEncoding.default,
                                        headers: headerInfo)
                //            .validate(statusCode: 200..<300)
                //.validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        if let JSON = response.result.value {
                            
                            let jsonResult : NSDictionary = JSON as! NSDictionary
                            completion(jsonResult, nil)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
                    
                    
            }
                .responseString { response in
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value{
                            print(data)
                        }
                        
                        completion(nil, nil)
                        
                    case .failure(_):
                        completion(nil, nil)
                        break
                    }
            }
            
            
            
            if(shouldStartImmediately)
            {
                dataReq.resume()
            }
            return dataReq
            
        default:
            return nil
        }
    }
    
    // MARK: Image upload
    class func getUploadTask(url : String ,
                             data: Data,
                             filePath: String?,
                             fName: String?,
                             fType: String?,
                             method : HTTPMethod?,
                             param:Dictionary<String,Any>?,
                             header:HTTPHeaders?,
                             completion: @escaping ( _ result: Any?,  _ error : Error?) -> Void)
    {
        let mType:HTTPMethod = method ?? .post
        /*
        let encodingMemoryThreshold: UInt64 = SessionManager.multipartFormDataEncodingMemoryThreshold
        
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                multipartFormData.append(data, withName: filePath!, fileName: fName!, mimeType: fType!)
                for (key, value) in param! {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        },
            
            usingThreshold: encodingMemoryThreshold,
            to: url,
            method: mType,
            headers: header,
            encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted * 100)")
                    })
                    
                    upload.responseJSON { response in
                        
                        // result of response serialization
                        
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            completion(JSON, nil)
                        }else{
                            completion(nil, NSError(domain: "Return format is not a JSON Fromat", code: 0, userInfo: nil))
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    completion(nil, encodingError)
                }
        }
        )
         */
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data, withName: filePath!, fileName: fName!, mimeType: fType!)
            for (key, value) in param! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:url,
           method: mType,
           headers: header)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted * 100)")
                })
                
                upload.responseJSON { response in
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        completion(JSON, nil)
                    }else{
                        completion(nil, NSError(domain: "Return format is not a JSON Fromat", code: 0, userInfo: nil))
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                completion(nil, encodingError)
            }
            
        }
    }
    
    // MARK:- SESSIONS
    internal class func session()->URLSession{
        return Alamofire.SessionManager.default.session
    }
    
    class func initialiseSession()->Void{
        Alamofire.SessionManager.default.startRequestsImmediately = false
    }
    
    //MARK:- Extras
    class func validateURL(urlString : String?) -> Bool
    {
        // Add further url verification codes here
        guard urlString != nil && (urlString?.checkURL())! else {
            return false
        }
        return true
    }
}

