//
//  FilterPopup.swift
//  imCelebrity
//
//  Created by Paramita  on 01/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol FilterPopupDelegate: class {
    func filterCancelBtnAction()
    func filterApplyBtnAction()
    func didSelectClkEvent(index: Int)
}



class FilterPopup: UIView {

    let SCREENRECT = UIScreen.main.bounds
    
    @IBOutlet weak var headerTxtLbl: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var leadingConstraintOutlate = NSLayoutConstraint()
    let sexPicker = UIPickerView()
    let dobPicker = UIDatePicker()
    var accessaryView = UIView()
    var nibView = UIView()
    let textCellId = "CellTextFieldModel"
    let cellCheckBox = "CheckBoxCell"
    let sexArray = ["MALE", "FEMALE", "OTHER"]
    weak var delegate: FilterPopupDelegate?
    var currentFilterTypeIndex = Int()
    
    var textInputAttributesArr = Array<Any>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        var nibObjects = Bundle.main.loadNibNamed("FilterPopup", owner: self, options: nil)
        nibView = nibObjects?[0] as! UIView
        nibView.frame = frame
        self.addSubview(nibView)
        self.setupUI()
        self.loadTableCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureFilter(typeIndex: Enums.FilterPopupType) {
        switch typeIndex {
        case .Connects:
            self.configureConnectsTextInput()
            break
        case .Fans:
            self.configureFansTextInput()
            break
        case .Notifications:
            self.configureNotificationTextInput()
            break
        }
        self.loadTableCell()
    }
    
    func setupUI() {
        bgView.addShadow(offset: CGSize(width: -1, height: 1), color: UIColor.darkGray, opacity: 0.5, radius: 20)
        self.setupAccessoryView()
    }
    func setupAccessoryView() {
        accessaryView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:44)
        accessaryView.backgroundColor = .red
        
        let doneButton : UIButton = UIButton()
        doneButton.frame = CGRect(x:SCREENRECT.width-60, y:0, width:60, height:44)
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonClicked), for: .touchUpInside)
        doneButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(doneButton)
        
        let cancelButton : UIButton = UIButton()
        cancelButton.frame = CGRect(x:10, y:0, width:60, height:44)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked(sender:)), for: .touchUpInside)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(cancelButton)
        
        dobPicker.frame = CGRect(x:0, y:0, width:SCREENRECT.width, height:216)
        dobPicker.datePickerMode = .date
        dobPicker.maximumDate = Date()
        dobPicker.backgroundColor = .white
        
        sexPicker.frame = CGRect(x:0, y:0, width:SCREENRECT.width, height:216)
        sexPicker.delegate = self
        sexPicker.dataSource = self
        sexPicker.backgroundColor = .white
    }
    func loadTableCell(){
        self.tableView.register(UINib(nibName: textCellId, bundle: nil), forCellReuseIdentifier: textCellId)
        self.tableView.register(UINib(nibName: cellCheckBox, bundle: nil), forCellReuseIdentifier: cellCheckBox)
        
    }
    func configureConnectsTextInput() {
        self.headerTxtLbl.text = "Filter Connects"
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Age", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Age should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "Profession", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Profession should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), CheckBoxAttributes.init(text: "Celebs", status: false), CheckBoxAttributes.init(text: "Blocked Profile", status: false)]
    }
    func configureFansTextInput() {
        self.headerTxtLbl.text = "Filter Fans"
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Age", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Age should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "Profession", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Profession should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "Blocked", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Blocked should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), CheckBoxAttributes.init(text: "Celebs", status: false)]
    }
    
    func configureNotificationTextInput() {
        self.headerTxtLbl.text = "Filter Notifications"
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Select app", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Select app should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "Date Range", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Date range should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: true, isShowCountryCodes: false, isShowDropdownArrow: false), TextInputAttributes.init(placeholderText: "Bookmarked", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Bookmarked should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "Read", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Read should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true)]
    }
    
    @IBAction func applyBtnAction(_ sender: Any) {
        self.delegate?.filterApplyBtnAction()
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.delegate?.filterCancelBtnAction()
    }
    
    @objc func doneButtonClicked(sender:UIButton){
        self.resignFirstResponder()
        self.endEditing(true)
        
        let type: Enums.FilterPopupType  = Enums.FilterPopupType(rawValue: currentFilterTypeIndex)!
        switch type {
            
        case .Connects:
            self.setValueForSex()
            break
        case .Fans:
            self.setValueForSex()
            break
        case .Notifications:
            self.setValueForDOB()
            break
        }
        
    }
    
    func setValueForSex() {
        let indexpath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = "\(sexArray[sexPicker.selectedRow(inComponent: 0)])"
        
        let attText = textInputAttributesArr[indexpath.row]
        if var att = attText as? TextInputAttributes {
            att.textFieldText = cell?.txtInput.text ?? ""
            textInputAttributesArr[indexpath.row] = att
        }
    }
    func setValueForDOB() {
        let indexpath = IndexPath(row: 1, section: 0)
        let cell = tableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = changeDateTimeFormat(inputDateTime: dobPicker.date, outPutTimeZone: NSTimeZone.local, outPutFormat: "dd MMMM yyyy")
        
        let attText = textInputAttributesArr[indexpath.row]
        if var att = attText as? TextInputAttributes {
            att.textFieldText = cell?.txtInput.text ?? ""
            textInputAttributesArr[indexpath.row] = att
        }
    }
    
    @objc func cancelButtonClicked(sender:UIButton){
        self.endEditing(true)
    }
    
    func DidSelectActivityForConnects(index: Int) {
        let attText = textInputAttributesArr[index]
        if let att = attText as? TextInputAttributes {
            if index == 1 || index == 2 {
                self.delegate?.didSelectClkEvent(index: index)
            }
        }else {
            if var attCheck = attText as? CheckBoxAttributes {
                if attCheck.status {
                    attCheck.status = false
                }else {
                    attCheck.status = true
                }
                textInputAttributesArr[index] = attCheck
                self.tableView.reloadData()
            }
        }
    }
    func DidSelectActivityForNotification(index: Int) {
        if index == 1 {
            
        }else {
            self.delegate?.didSelectClkEvent(index: index)
        }
    }
}

extension FilterPopup: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let type: Enums.FilterPopupType  = Enums.FilterPopupType(rawValue: currentFilterTypeIndex)!
        switch type {
        case .Connects:
            textField.inputView = sexPicker
            textField.inputAccessoryView = accessaryView
            break
        case .Fans:
            textField.inputView = sexPicker
            textField.inputAccessoryView = accessaryView
            break
        case .Notifications:
            textField.inputView = dobPicker
            textField.inputAccessoryView = accessaryView
            break
        }

    }
}

extension FilterPopup: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textInputAttributesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let attText = textInputAttributesArr[indexPath.row]
        if let att = attText as? TextInputAttributes {
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath) as! CellTextFieldModel
            cell.populateAllData(attributes: att)
            cell.txtInput.tag = indexPath.row
            cell.txtInput.delegate = self
            
            let type: Enums.FilterPopupType  = Enums.FilterPopupType(rawValue: currentFilterTypeIndex)!
            switch type {
            case .Connects:
                if indexPath.row == 0 {
                    cell.txtInput.isUserInteractionEnabled = true
                }else {
                    cell.txtInput.isUserInteractionEnabled = false
                }
                break
            case .Fans:
                if indexPath.row == 0 {
                    cell.txtInput.isUserInteractionEnabled = true
                }else {
                    cell.txtInput.isUserInteractionEnabled = false
                }
                break
            case .Notifications:
                if indexPath.row == 1 {
                    cell.txtInput.isUserInteractionEnabled = true
                }else {
                    cell.txtInput.isUserInteractionEnabled = false
                }
                break
            }
            
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellCheckBox, for: indexPath) as! CheckBoxCell
            if let attCheck = attText as? CheckBoxAttributes {
                cell.populateAllData(obj: attCheck)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let attText = textInputAttributesArr[indexPath.row]
        if let att = attText as? TextInputAttributes {
            return 60
        }else {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type: Enums.FilterPopupType  = Enums.FilterPopupType(rawValue: currentFilterTypeIndex)!
        switch type {
        case .Connects:
            self.DidSelectActivityForConnects(index: indexPath.row)
            break
        case .Fans:
            self.DidSelectActivityForConnects(index: indexPath.row)
            break
        case .Notifications:
            self.DidSelectActivityForNotification(index: indexPath.row)
            break
        }
        
    }
}
extension FilterPopup : UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sexArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sexArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let indexpath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = sexArray[row]
    }
}

