//
//  FooterCustomView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 17/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol FooterCustomViewDelegate: class {
    func middleBtnClkEvent()
    func profileBtnClkEvent()
    func searchBtnClkEvent()
    func messageBtnClkEvent()
    func callBtnClkEvent()
}


class FooterCustomView: UIView {
    
    var shadowViewBgColor : UIColor? {
        didSet {
            self.bgShadowView.backgroundColor = shadowViewBgColor
        }
    }
    
    var leftFirstImg : UIImage?  {
        didSet {
            self.leftFirstBtnOutlate.setImage(leftFirstImg, for: .normal)
            self.leftFirstBtnOutlate.setImage(leftFirstImg?.withRenderingMode(.alwaysTemplate), for: .selected)
            self.leftFirstBtnOutlate.tintColor = UIColor.red
        }
    }
    var leftSecondImg : UIImage?  {
        didSet {
            self.leftSecondBtnOutlate.setImage(leftSecondImg, for: .normal)
            self.leftSecondBtnOutlate.setImage(leftSecondImg?.withRenderingMode(.alwaysTemplate), for: .selected)
            self.leftSecondBtnOutlate.tintColor = UIColor.red
        }
    }
    var rightFirstImg : UIImage?  {
        didSet {
            self.rightFirstBtnOutlate.setImage(rightFirstImg, for: .normal)
            self.rightFirstBtnOutlate.setImage(rightFirstImg?.withRenderingMode(.alwaysTemplate), for: .selected)
            self.rightFirstBtnOutlate.tintColor = UIColor.red
        }
    }
    var rightSecondImg : UIImage?  {
        didSet {
            self.rightSecondBtnOutlate.setImage(rightSecondImg, for: .normal)
            self.rightSecondBtnOutlate.setImage(rightSecondImg?.withRenderingMode(.alwaysTemplate), for: .selected)
            self.rightSecondBtnOutlate.tintColor = UIColor.red
        }
    }
    var middleImg : UIImage?  {
        didSet {
            self.middleBtnOutlate.setImage(middleImg, for: .normal)
        }
    }
    var middleBgImg : UIImage?  {
        didSet {
            self.middleBtnOutlate.setBackgroundImage(middleBgImg, for: .normal)
        }
    }
    
    var nibView = UIView()
    weak var delegate: FooterCustomViewDelegate?
    
    @IBOutlet weak var middleBtnOutlate: UIButton!
    @IBOutlet weak var rightFirstBtnOutlate: UIButton!
    @IBOutlet weak var rightSecondBtnOutlate: UIButton!
    @IBOutlet weak var leftSecondBtnOutlate: UIButton!
    @IBOutlet weak var leftFirstBtnOutlate: UIButton!
    @IBOutlet weak var bgShadowView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        var nibObjects = Bundle.main.loadNibNamed("FooterCustomView", owner: self, options: nil)
        nibView = nibObjects?[0] as! UIView
        nibView.frame = frame
        self.addSubview(nibView)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupUI() {
        self.bgShadowView.addShadow(offset: CGSize(width: -1, height: 1), color: UIColor.darkGray, opacity: 0.5, radius: 20)
        
    }
    
    
    
    @IBAction func leftSecondBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        }else {
            sender.isSelected = true
            leftFirstBtnOutlate.isSelected = false
            rightFirstBtnOutlate.isSelected = false
            rightSecondBtnOutlate.isSelected = false
        }
        self.delegate?.searchBtnClkEvent()
    }
    @IBAction func leftFirstBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        }else {
            sender.isSelected = true
            leftSecondBtnOutlate.isSelected = false
            rightFirstBtnOutlate.isSelected = false
            rightSecondBtnOutlate.isSelected = false
        }
        self.delegate?.profileBtnClkEvent()
    }
    @IBAction func rightSecondBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        }else {
            sender.isSelected = true
            leftSecondBtnOutlate.isSelected = false
            rightFirstBtnOutlate.isSelected = false
            leftFirstBtnOutlate.isSelected = false
        }
        self.delegate?.messageBtnClkEvent()
    }
    @IBAction func rightFirstBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        }else {
            sender.isSelected = true
            leftSecondBtnOutlate.isSelected = false
            leftFirstBtnOutlate.isSelected = false
            rightSecondBtnOutlate.isSelected = false
        }
        self.delegate?.callBtnClkEvent()
    }
    @IBAction func middleBtnAction(_ sender: Any) {
        self.delegate?.middleBtnClkEvent()
    }
    
}
