//
//  CustomLoaderClass.swift
//  VIPTrainer
//
//  Created by s on 13/09/17.
//  Copyright © 2017 CapitalNumbers. All rights reserved.
//

import UIKit

class CustomLoaderClass: UIView {

    @IBOutlet weak var customViewImageOutlet: UIImageView!
    var ninView = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var nibObjects = Bundle.main.loadNibNamed("CustomLoaderClass", owner: self, options: nil)
        ninView = nibObjects?[0] as! UIView
        ninView.frame = frame
        self.addSubview(ninView)
        
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }

}
