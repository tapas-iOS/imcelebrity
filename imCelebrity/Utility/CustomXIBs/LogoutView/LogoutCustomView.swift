//
//  LogoutCustomView.swift
//  imCelebrity
//
//  Created by Paramita  on 19/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol LogoutCustomViewDelegate: class {
    func noBtnClkEvent()
    func yesBtnClkEvent()
}

class LogoutCustomView: UIView {
    
    var nibView = UIView()
    @IBOutlet weak var topBgView: UIView!
    weak var delegate: LogoutCustomViewDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        var nibObjects = Bundle.main.loadNibNamed("LogoutCustomView", owner: self, options: nil)
        nibView = nibObjects?[0] as! UIView
        nibView.frame = frame
        self.addSubview(nibView)
        self.setupUI()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    func setupUI() {
        self.topBgView.addCornerRadious(radious: 10)
        self.topBgView.addShadow(offset: CGSize(width: -1, height: 1), color: UIColor.lightGray, opacity: 0.5, radius: 20)
    }

    @IBAction func noBtnAction(_ sender: Any) {
        self.delegate?.noBtnClkEvent()
    }
    @IBAction func yesBtnAction(_ sender: Any) {
        self.delegate?.yesBtnClkEvent()
    }
}
