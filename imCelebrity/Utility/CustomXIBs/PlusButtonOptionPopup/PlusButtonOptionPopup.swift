//
//  PlusButtonOptionPopup.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 24/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol PlusButtonOptionPopupDelegate: class {
    func firstButtonSelectionEvent()
    func secondButtonSelectionEvent()
    func crossButtonSelectionEvent()
}

class PlusButtonOptionPopup: UIView {

    @IBOutlet weak var connectsFooterPlusBgView: UIView!
    @IBOutlet weak var backGroundImageView: UIImageView!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBOutlet weak var buttonOutletSecond: UIButton!
    @IBOutlet weak var buttonOutletFirst: UIButton!
    @IBOutlet weak var lableHeaderDescription: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    var heightConstraintOutlate = NSLayoutConstraint()
    
    var nibView = UIView()
    weak var delegate: PlusButtonOptionPopupDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        var nibObjects = Bundle.main.loadNibNamed("PlusButtonOptionPopup", owner: self, options: nil)
        nibView = nibObjects?[0] as! UIView
        nibView.frame = frame
        self.addSubview(nibView)
        self.setupUI()
    }
    
    func ConfigureViewAsPerViewOpen(openPopup openVC: Enums.FooterPopupType) {
        switch  openVC{
        case .Dashboard:
            self.lableHeaderDescription.text = "What you want to upload?"
            self.connectsFooterPlusBgView.isHidden = true
            break
        case .Connects:
            self.lableHeaderDescription.text = "Send Invitation"
            self.connectsFooterPlusBgView.isHidden = false
            break
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    func setupUI() {
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func secondButtonSelection(_ sender: UIButton) {
        self.delegate?.secondButtonSelectionEvent()
    }
    
    @IBAction func FirstButtonSelection(_ sender: UIButton) {
        self.delegate?.firstButtonSelectionEvent()
    }
    
    @IBAction func crossButtonSelection(_ sender: UIButton) {
        self.delegate?.crossButtonSelectionEvent()
    }
}
