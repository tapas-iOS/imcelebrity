//
//  SuggestionPopupCustomView.swift
//  imCelebrity
//
//  Created by Paramita  on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class SuggestionPopupCustomView: UIView {
    
    var nibView = UIView()

    @IBOutlet weak var secondLblOutlate: UILabel!
    @IBOutlet weak var firstLblOutlate: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        var nibObjects = Bundle.main.loadNibNamed("SuggestionPopupCustomView", owner: self, options: nil)
        nibView = nibObjects?[0] as! UIView
        nibView.frame = frame
        self.addSubview(nibView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupConnectsText() {
        firstLblOutlate.text = "Please select at least 5 popular users to start seeing their posts."
        secondLblOutlate.text = "More friends you invite, more connections you will make and more famous you will be!"
    }
    func setupFansText() {
        firstLblOutlate.text = "Please share or upload good content that you can earn fans and invite contacts to make more fans!"
        secondLblOutlate.text = ""
    }
    
    @IBAction func crossBtnAction(_ sender: Any) {
        self.isHidden = true
    }

}
