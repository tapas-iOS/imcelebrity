
import Foundation
import UIKit

extension String {
    
    
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    var westernArabicNumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .compactMap { pattern ~= $0 ? Character($0) : nil })
    }
    
    func trimWhiteSpace() -> String{
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func getFullyTrimmedStringLength()->Int{
        return self.trimWhiteSpace().count
    }
    
    func checkURL() -> Bool {
        guard (self.getFullyTrimmedStringLength()) > 0 else{
            return  false
        }
        
        let urlRegEx = "^http(?:s)?://(?:w{3}\\.)?(?!w{3}\\.)(?:[\\p{L}a-zA-Z0-9\\-]+\\.){1,}(?:[\\p{L}a-zA-Z]{2,})/(?:\\S*)?$"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        
        return urlTest.evaluate(with: self)
    }
    
    func replaceOccuranceOfString()->String{
        return self.replacingOccurrences(of: "&quot;", with: "\"", options: .literal, range: nil)
    }
    
    func  convertToBase64() -> String {
        let data = (self).data(using: String.Encoding.utf8)
        let base64String = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return base64String
    }
    
    func stringLength() -> Int {
        return self.count
    }

    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
    
    func capitalizingFirstLetter() -> String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    //MARK: - Validation Checking Of Email id....
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    //MARK: - Validation Checking of Password Formate requirement.....
    func passwordChecking() -> Bool  {
        var lengthCheckResult = Bool()
        if((self.count) > 7) {
            lengthCheckResult = true
        }else {
            lengthCheckResult = false
        }
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: self)
        print("\(capitalresult)")
        
        let smallLetterRegEx  = ".*[a-z]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
        let smallresult = texttest1.evaluate(with: self)
        print("\(smallresult)")
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest2.evaluate(with: self)
        print("\(numberresult)")
        
        if(capitalresult == true && smallresult == true && numberresult == true && lengthCheckResult == true) {
            return true
        }else {
            return false
        }
    }
    
    func changeWhitePlaceHolderTextColour() -> NSAttributedString{
        return NSAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
    }
    
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    func isValidatePhoneNumber() -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        return result
    }

}


extension String {
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes { _ = CC_SHA1($0, CC_LONG(data.count), &digest) }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}
