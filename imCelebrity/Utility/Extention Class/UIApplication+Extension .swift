//
//  UIApplication+Extension .swift
//  FollowUp
//
//  Created by SumitavaDatta on 06/02/18.
//  Copyright © 2018 CapitalNumbers. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
