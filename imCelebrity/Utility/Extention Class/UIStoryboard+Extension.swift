//
//  UIStoryboard+Extension.swift
//  FollowUp
//
//  Created by SumitavaDatta on 05/02/18.
//  Copyright © 2018 CapitalNumbers. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    //MARK: SIDE_MENU
    class func musicStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Music")
    }
    class func videoStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Video")
    }
    class func connectsStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Connects")
    }
    class func fansStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Fans")
    }
    class func SnapStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Snap")
    }
    class func favouriteStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Favourite")
    }
    class func settingsStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Settings")
    }
    class func searchStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Search")
    }
    class func ProfileStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Profile")
    }
    class func chatStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Chat")
    }
    class func NotificationsStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Notification")
    }
    class func otherStoryBoard()->UIStoryboard {
        return self.getStoryBoard(name: "Main")
    }
    class func getStoryBoard(name : String)->UIStoryboard {
        return UIStoryboard.init(name: name, bundle: Bundle.main)
    }
}
