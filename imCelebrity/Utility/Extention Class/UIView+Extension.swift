
import Foundation
import UIKit

enum VerticalLocation: String {
    case bottom
    case top
}

extension UIView {
    
    func tableViewBtmConstraintChange(btmConstraint: NSLayoutConstraint, value : CGFloat) {
        btmConstraint.constant = value
        UIView.animate(withDuration: 0.25, delay: 0, options: .beginFromCurrentState, animations: {() -> Void in
            self.layoutIfNeeded()
            self.setNeedsUpdateConstraints()
        }, completion: {(_ finished: Bool) -> Void in
            if finished {
            }
        })
    }
    
    func addCornerRadious(radious: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radious
    }
    
    func addBorder(borderWidth: CGFloat, color: UIColor) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
    }
    
        func addShadow(location: VerticalLocation, color: UIColor = .lightGray, opacity: Float = 0.5, radius: CGFloat = 5.0) {
            switch location {
            case .bottom:
                addShadow(offset: CGSize(width: 0, height: 10), color: color, opacity: opacity, radius: radius)
            case .top:
                addShadow(offset: CGSize(width: 0, height: -10), color: color, opacity: opacity, radius: radius)
            }
        }
        
        func addShadow(offset: CGSize, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
            self.layer.masksToBounds = false
            self.layer.shadowColor = color.cgColor
            self.layer.shadowOffset = offset
            self.layer.shadowOpacity = opacity
            self.layer.shadowRadius = radius
        }
    
    
    func loadFooterCustomView(footerView: FooterCustomView) {
        self.configureView(view: footerView)
    }
    func loadFooterPlusPopupCustomView(footerPlusPopupView: PlusButtonOptionPopup) {
        self.configureView(view: footerPlusPopupView)
    }
    func loadSuggestionCustomView(suggestionView: SuggestionPopupCustomView) {
        self.configureFullView(view: suggestionView)
    }
    func loadCustomConnectsFilterView(filterView: FilterPopup) {
        self.configureFullView(view: filterView)
    }
    
    func configureView(view: UIView) {
        view.frame = CGRect(x: 0, y: 0, width: Int(self.frame.size.width), height: 100)
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            view.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        }
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        view.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
    func configureFullView(view: UIView) {
        view.frame = CGRect(x: 0, y: 0, width: Int(self.frame.size.width), height: Int(self.frame.size.height))
        self.addSubview(view)
        view.isHidden = true  //true
        
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0))
        if #available(iOS 11.0, *) {
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self.safeAreaLayoutGuide, attribute: .top, multiplier: 1.0, constant: 0))
        } else {
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0))
        }
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0))
    }
    func constrainToEdges(_ subview: UIView) {
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        let topContraint = NSLayoutConstraint(
            item: subview,
            attribute: .top,
            relatedBy: .equal,
            toItem: self,
            attribute: .top,
            multiplier: 1.0,
            constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(
            item: subview,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: self,
            attribute: .bottom,
            multiplier: 1.0,
            constant: 0)
        
        let leadingContraint = NSLayoutConstraint(
            item: subview,
            attribute: .leading,
            relatedBy: .equal,
            toItem: self,
            attribute: .leading,
            multiplier: 1.0,
            constant: 0)
        
        let trailingContraint = NSLayoutConstraint(
            item: subview,
            attribute: .trailing,
            relatedBy: .equal,
            toItem: self,
            attribute: .trailing,
            multiplier: 1.0,
            constant: 0)
        
        addConstraints([
            topContraint,
            bottomConstraint,
            leadingContraint,
            trailingContraint])
    }

}
