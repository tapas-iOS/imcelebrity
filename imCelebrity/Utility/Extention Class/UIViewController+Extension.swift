//
//  UIViewController+Extension.swift
//  FollowUp
//
//  Created by SumitavaDatta on 05/02/18.
//  Copyright © 2018 CapitalNumbers. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices

extension UIViewController {
    
//    var delApp : AppDelegate {
//        return AppDelegate.delApp
//    }
    
//    class func getSideMenuViewController()->SideMenuViewController{
//        if AppDelegate.delApp.sideMenu == nil{
//            AppDelegate.delApp.sideMenu = UIStoryboard.sideMenuStoryBoard().instantiateInitialViewController() as? SideMenuViewController
//        }
//
//        return AppDelegate.delApp.sideMenu!
//    }
    
    func commonAlertMessage(title : String = Constant.AppName ,message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAutoCancelAlertWithMessage(message: String){
        let action = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) in
        })
        let alertController = self.showSuccessfullAlert(alertTitle: Constant.AppName, alertMessage: message, actionsArray: [action])
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
            alertController.dismiss(animated: true, completion: {
            })
        })
    }
    
    func showSuccessfullAlert(alertTitle:String,alertMessage:String,actionsArray:[UIAlertAction]?)->UIAlertController{
        
        let alert = UIAlertController(title: alertTitle,
                                      message: alertMessage,
                                      preferredStyle: UIAlertController.Style.alert)
        
        if actionsArray != nil{
            for item in actionsArray!{
                alert.addAction(item)
            }
        }else{
            let cancelAction = UIAlertAction(title: "OK",
                                             style: .cancel, handler: nil)
            
            alert.addAction(cancelAction)
        }
        
        DispatchQueue.main.async {
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
        return alert
    }

    
    func showLogoutAlert(with message: String){
        let action = UIAlertAction.init(title: AlertConstants.SessionExpireAlert.AlertButtonTitleOk, style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) in
           // AppDelegate.delApp.setDisplayViewController(displayIndex: Enums.DisplaySectionIndex(rawValue: Enums.DisplaySectionIndex.Logout.rawValue  )!)
        })
        
        let alertController = self.showSuccessfullAlert(alertTitle: Constant.AppName, alertMessage: message, actionsArray: [action])
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.5, execute: {

            alertController.dismiss(animated: true, completion: {
               // AppDelegate.delApp.setDisplayViewController(displayIndex: Enums.DisplaySectionIndex(rawValue: Enums.DisplaySectionIndex.Logout.rawValue)!)
            })
        })
    }
   
}


extension UIViewController {
    func showActionSheetForShowingPhotoPickerType(_ completion: @escaping(_ photpPicketType: PhotoPickerType?) -> Void){
        let action = UIAlertController(title: "Choose photo from", message: nil, preferredStyle: .actionSheet)
        action.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            completion(.camera)
        }))
        action.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action) in
            completion(.library)
        }))
        action.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            completion(nil)
        }))
        self.present(action, animated: true, completion: nil)
    }
    func takePhotoFromCamera(imagePicker: UIImagePickerController) {
        if UIImagePickerController.availableCaptureModes(for: .front) != nil {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.cameraDevice = .front
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker,animated: true,completion: nil)
        }else if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.cameraDevice = .rear
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker,animated: true,completion: nil)
        }  else {
            self.noCamera()
        }
    }
    func takePhotoFromGallery(imagePicker: UIImagePickerController) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePicker.allowsEditing = true
            imagePicker.mediaTypes = [kUTTypeImage as String]
            self.present(imagePicker,animated: true, completion: nil)
        }
    }
    func noCamera() {
        self.commonAlertMessage(message: "Sorry, this device has no camera")
    }
}
