//
//  UIWindow+Extension.swift
//  imCelebrity
//
//  Created by Paramita  on 19/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension UIWindow {
    func loadCustomLogoutView(logoutView: LogoutCustomView) {
        self.configureWindowView(view: logoutView)
    }
    func configureWindowView(view: UIView) {
        self.addSubview(view)
        self.bringSubviewToFront(view)
        view.isHidden = true  //true
        
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0))
    }
}
