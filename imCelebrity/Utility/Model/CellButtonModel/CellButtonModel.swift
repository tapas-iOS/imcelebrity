//
//  CellButtonModel.swift
//  imCelebrity
//
//  Created by Paramita  on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol CellButtonModelDelegate: class {
    func saveButtonAction()
}

class CellButtonModel: UITableViewCell {
    
    weak var delegate: CellButtonModelDelegate?
    @IBOutlet weak var cellButtonOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.addCornorRediusOnButton()
    }
    
   private func addCornorRediusOnButton(){
        self.cellButtonOutlet.addCornerRadious(radious: 10)
    }
    
    func setButtonName(buttonTitle title:String){
        //Enums.CellButtonName.Signup
        self.cellButtonOutlet.setTitle(title, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension CellButtonModel{
    @IBAction func cellButtonAction(_ sender: UIButton) {
        delegate?.saveButtonAction()
    }
}
