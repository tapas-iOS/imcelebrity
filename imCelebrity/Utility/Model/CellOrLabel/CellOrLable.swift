//
//  CellOrLable.swift
//  imCelebrity
//
//  Created by Paramita  on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class CellOrLable: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
