//
//  SnapListingCollectionCell.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol SnapListingCellDelegate: class {
    func snapButtonClkAction(tagIndex: Int)
}

class SnapListingCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewPostedUserName: UIImageView!
    @IBOutlet weak var lablePostedUserName: UILabel!
    @IBOutlet weak var imageViewLoginUserIcon: UIImageView!
    @IBOutlet weak var snapButtonOutlate: UIButton!
    @IBOutlet weak var snapThumbImageView: UIImageView!
    weak var delegate: SnapListingCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func confuguration(index: Int) {
        //self.layer.cornerRadius = 5
        //self.layer.masksToBounds = true
        self.snapButtonOutlate.tag = index
    }
    
    @IBAction func SnapButtonAction(_ sender: Any) {
        self.delegate?.snapButtonClkAction(tagIndex: (sender as AnyObject).tag)
    }
}
