//
//  CellSocialLoginModel.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
protocol CellSocialLoginModelDelegate: class {
    func facebookBtnClkEvent()
    func googlePlusBtnClkEvent()
}


class CellSocialLoginModel: UITableViewCell {
    weak var delegate: CellSocialLoginModelDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @IBAction func googlePlusBtnAction(_ sender: Any) {
        self.delegate?.googlePlusBtnClkEvent()
    }
    @IBAction func facebookBtnAction(_ sender: Any) {
        self.delegate?.facebookBtnClkEvent()
    }

}
