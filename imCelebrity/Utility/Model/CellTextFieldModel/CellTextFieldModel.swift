//
//  CellTextFieldModel.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol CellTextFieldModelDelegate: class {
    func rightButtonClkAction(index: Int)
    func leftButtonClkAction()
}

class CellTextFieldModel: UITableViewCell {
    
    @IBOutlet weak var txtInput: SkyFloatingLabelTextField!
    weak var delegate: CellTextFieldModelDelegate?
    
    var rightView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        view.isUserInteractionEnabled = true
        return view
    }()
    
    let rightBtn : UIButton = {
        let btn = UIButton()
        return btn
    }()
    
    var leftView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.frame = CGRect(x: 0, y: 0, width: 60, height: 40)
        view.isUserInteractionEnabled = true
        return view
    }()
    
    var leftSeparateLine : UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.lightGray
        return lbl
    }()
    
    let leftBtn : UIButton = {
        let btn = UIButton()
        btn.setTitleColor(UIColor.lightGray, for: .normal)
        btn.contentVerticalAlignment = .bottom
        btn.titleLabel?.font = UIFont(name: "OpenSans", size: 14.0)
        return btn
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func populateAllData(attributes: TextInputAttributes) {
        rightBtn.frame = rightView.bounds
        rightBtn.addTarget(self, action: #selector(rightButtonAction(_:)), for: .touchUpInside)
        self.rightView.addSubview(rightBtn)
        
        leftBtn.frame = leftView.bounds
        leftSeparateLine.frame = CGRect(x: 55, y: 20, width: 1, height: 20)
        leftBtn.addTarget(self, action: #selector(leftButtonAction(_:)), for: .touchUpInside)
        self.leftView.addSubview(leftBtn)
        self.leftView.addSubview(leftSeparateLine)
        
        self.txtInput.placeholder = attributes.placeholderText
        self.txtInput.text = attributes.textFieldText
        self.txtInput.keyboardType = attributes.keyboardType
        self.txtInput.isSecureTextEntry = attributes.isSecureTextEntry
        self.txtInput.returnKeyType = attributes.returnKey
        
        self.txtInput.leftView = nil
        self.txtInput.rightView = nil
        
        
        if attributes.isViewPasswordButton {
            rightBtn.setImage(#imageLiteral(resourceName: "PasswordHide"), for: .normal)
            self.txtInput.rightViewMode = .always
            self.txtInput.rightView = rightView
            self.txtInput.rightView?.isUserInteractionEnabled = true
        } else if attributes.isShowCalenderButton {
            rightBtn.setImage(#imageLiteral(resourceName: "CalenderIcon"), for: .normal)
            self.txtInput.rightViewMode = .always
            self.txtInput.rightView = rightView
        }
        if attributes.isShowCountryCodes {
            leftBtn.setTitle("+91", for: .normal)
            self.txtInput.leftViewMode = .always
            self.txtInput.leftView = leftView
        }
        if attributes.isShowDropdownArrow {
            rightBtn.setImage(#imageLiteral(resourceName: "DropdownArrow"), for: .normal)
            //rightBtn.setTitle(">", for: .normal)
            rightBtn.setTitleColor(UIColor.black, for: .normal)
            self.txtInput.rightViewMode = .always
            self.txtInput.rightView = rightView
            self.txtInput.rightView?.isUserInteractionEnabled = true
        }
    }
    
    
    @objc func rightButtonAction(_ sender: Any) {
        self.delegate?.rightButtonClkAction(index: (sender as AnyObject).tag)
    }
    @objc func leftButtonAction(_ sender: Any) {
        self.delegate?.leftButtonClkAction()
    }
    
}



