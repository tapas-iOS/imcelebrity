//
//  CheckBoxCell.swift
//  imCelebrity
//
//  Created by Paramita  on 01/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class CheckBoxCell: UITableViewCell {

    @IBOutlet weak var checkedImgView: UIImageView!
    @IBOutlet weak var txtLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func populateAllData(obj: CheckBoxAttributes) {
        self.txtLabel.text = obj.text
        if obj.status {
            self.checkedImgView.image = #imageLiteral(resourceName: "CheckBoxSelected")
        }else {
            self.checkedImgView.image = #imageLiteral(resourceName: "CheckBOxDelect")
        }
    }
    
}
