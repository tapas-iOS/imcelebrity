//
//  CellForgotLabelModel.swift
//  imCelebrity
//
//  Created by Paramita  on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol CellForgotLabelModelDelegate: class {
    func forgotPasswordBtnClkEvent()
}

class CellForgotLabelModel: UITableViewCell {
    
    weak var delegate: CellForgotLabelModelDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        self.delegate?.forgotPasswordBtnClkEvent()
    }
    
}
