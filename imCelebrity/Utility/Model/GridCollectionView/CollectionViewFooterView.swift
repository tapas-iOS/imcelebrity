//
//  CollectionViewFooterView.swift
//  imCelebrity
//
//  Created by Paramita  on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class CollectionViewFooterView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        // Customize here
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
}
