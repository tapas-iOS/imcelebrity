//
//  GridCollectionViewCell.swift
//  imCelebrity
//
//  Created by Paramita  on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol GridCollectionViewCellDelegate: class {
    func videoButtonClkAction(tagIndex: Int)
}

class GridCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBOutlet weak var videoCaptionLbl: UILabel!
    @IBOutlet weak var videoButtonOutlate: UIButton!
    @IBOutlet weak var videoThumbImageView: UIImageView!
    weak var delegate: GridCollectionViewCellDelegate?
    
    
    func confuguration(index: Int) {
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.videoButtonOutlate.tag = index
    }
    
    
    
    
    
    @IBAction func videoButtonAction(_ sender: Any) {
        self.delegate?.videoButtonClkAction(tagIndex: (sender as AnyObject).tag)
    }
}
