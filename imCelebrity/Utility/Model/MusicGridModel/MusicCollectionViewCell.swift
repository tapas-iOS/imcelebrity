//
//  MusicCollectionViewCell.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 07/06/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class MusicCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var musicCaptionLbl: UILabel!
    @IBOutlet weak var musicBtnOutlate: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var musicThumbImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func musicBtnAction(_ sender: Any) {
    }
}
