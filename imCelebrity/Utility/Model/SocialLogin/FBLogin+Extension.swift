//
//  FBLogin+Extension.swift
//  SocialLogin
//
//  Created by Weaver web 5 on 31/01/19.
//  Copyright © 2019 SocialLogin. All rights reserved.
//

import Foundation
import UIKit
import FacebookLogin
import FBSDKLoginKit

extension UIViewController {
    
    //function is fetching the user data
    func getFBSDKAccessToken( completionHandler:@escaping(_ success: Bool, _ fbSDKTokenString: String?,_ resultDict: NSDictionary, _ error: String?)-> Void){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict: NSDictionary = result as! NSDictionary
                    if let accessToken = FBSDKAccessToken.current(){
                        print("\(accessToken)")
                       completionHandler(true, "\(accessToken)" , dict, error as? String)
                    }
                }
            })
        }
    }
}

