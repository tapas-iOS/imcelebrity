//
//  GoogleLogin+Extension.swift
//  SocialLogin
//
//  Created by Weaver web 5 on 31/01/19.
//  Copyright © 2019 SocialLogin. All rights reserved.
//

import Foundation
import  UIKit
import FirebaseAuth
import GoogleSignIn

extension UIViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    func configureGoogleSignInDelegate() {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            print("\(error)")
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print("\(error)")
                return
            }
            if ((self as? LoginVC) != nil) {
                let vc = self as? LoginVC
                vc?.getGoogleLoginDetails(userDetails: user!)
            }
        }
        
    }
    private func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("\(String(describing: error))")
    }
     
}
