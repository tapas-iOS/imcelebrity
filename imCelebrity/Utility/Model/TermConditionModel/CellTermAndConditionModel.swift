//
//  CellTermAndConditionModel.swift
//  imCelebrity
//
//  Created by Paramita  on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class CellTermAndConditionModel: UITableViewCell {
    
    @IBOutlet weak var textViewTermsConditionText: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTermsConditionText()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupTermsConditionText(){
        let normalText = "By clicking Sign Up you are accepting our "
        let linkText = "Terms & Conditions"
        
        let fullText = normalText + linkText
        
        let attributedString = NSMutableAttributedString(string: fullText)
        let url = URL(string: APIConstants.StaticURL.TermsAndCondition)!
        
        // Set the 'click here' substring to be the link
        attributedString.setAttributes([.link: url], range: NSMakeRange(normalText.count, linkText.count))
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray , range: NSMakeRange(0, normalText.count))
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: NSMakeRange(normalText.count, linkText.count))

        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"OpenSans", size: 15.0)!, range: NSMakeRange(0, normalText.count+linkText.count))
        
        self.textViewTermsConditionText.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]
        self.textViewTermsConditionText.attributedText = attributedString
        self.textViewTermsConditionText.isUserInteractionEnabled = true
    }
    
}
