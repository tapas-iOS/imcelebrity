//
//  ChatOnlineUserCell.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

//225,109,222

class ChatOnlineUserCell: UICollectionViewCell {
    
    @IBOutlet weak var userProfessionLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    
    func setupUI() {
        self.userImgView.layer.borderWidth = 1
        self.userImgView.layer.borderColor = UIColor(red: 225/225, green: 109/225, blue: 222/225, alpha: 1.0).cgColor
        self.userImgView.addCornerRadious(radious: 35)
    }
}
