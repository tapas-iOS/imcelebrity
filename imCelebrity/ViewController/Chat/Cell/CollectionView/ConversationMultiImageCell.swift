//
//  ConversationMultiImageCell.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ConversationMultiImageCell: UICollectionViewCell {
    
    @IBOutlet weak var uploadedImgView: UIImageView!
    
    func setupUI() {
        uploadedImgView.addCornerRadious(radious: 5)
    }
}
