//
//  ConversationMultiImageCountCell.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ConversationMultiImageCountCell: UICollectionViewCell {
    
    @IBOutlet weak var UploadedImageCount: UILabel!
    
    func setupUI() {
        UploadedImageCount.addCornerRadious(radious: 5)
        UploadedImageCount.layer.borderColor = UIColor.darkGray.cgColor
        UploadedImageCount.layer.borderWidth = 1
    }
}
