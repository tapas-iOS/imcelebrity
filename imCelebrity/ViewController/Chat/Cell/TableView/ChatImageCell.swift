//
//  ChatImageCell.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ChatImageCell: UITableViewCell {
    @IBOutlet weak var unreadCountBtnOutlate: UIButton!
    @IBOutlet weak var msgtimeLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var onlineImgView: UIImageView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ChatImageCell :  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConversationMultiImageCountCell", for: indexPath as IndexPath) as? ConversationMultiImageCountCell
            cell?.setupUI()
            return cell!
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConversationMultiImageCell", for: indexPath as IndexPath) as? ConversationMultiImageCell
            cell?.setupUI()
            return cell!
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (self.collectionView.frame.width-30)/4
        return CGSize(width: width, height: 40)
    }
}
