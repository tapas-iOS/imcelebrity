//
//  ChatTextCell.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ChatTextCell: UITableViewCell {

    @IBOutlet weak var unreadCountBtnOutlate: UIButton!
    @IBOutlet weak var msgtimeLbl: UILabel!
    @IBOutlet weak var userLastMsgLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var onlineImgView: UIImageView!
    @IBOutlet weak var userImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
