//
//  ChatVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ChatVC {
    @IBAction func homeBtnACtion(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func profileBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func notificationBtnAction(_ sender: Any) {
        let notificationVC = UIStoryboard.NotificationsStoryBoard().instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
}


extension ChatVC : ChatConversasionVCDelegate {
   func conversasionDidSelectClkEvent(index: Int) {
//        let chatDetailsVC = UIStoryboard.chatStoryBoard().instantiateViewController(withIdentifier: "ChatDetailsViewController") as! ChatDetailsViewController
//        self.navigationController?.pushViewController(chatDetailsVC, animated: true)
    
}
}
