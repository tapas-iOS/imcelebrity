//
//  ChatVC+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
import Parchment

extension ChatVC {
    func loadVC() {
        let storyboard = UIStoryboard.chatStoryBoard()
        conversationList = storyboard.instantiateViewController(withIdentifier: "ChatConversasionVC") as! ChatConversasionVC
        callList = storyboard.instantiateViewController(withIdentifier: "ChatCallVC") as! ChatCallVC
        
        conversationList.delegate = self
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [conversationList, callList])
        pagingViewController.indicatorColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        pagingViewController.backgroundColor = UIColor.white
        pagingViewController.selectedBackgroundColor = UIColor(red: 253/255, green: 253/255, blue: 253/255, alpha: 1.0)
        pagingViewController.textColor = UIColor.black
        pagingViewController.selectedTextColor = UIColor.black
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        pagingViewController.selectedFont = UIFont.boldSystemFont(ofSize: 15)
        
        // Make sure you add the PagingViewController as a child view
        // controller and constrain it to the edges of the view.
        addChild(pagingViewController)
        bgView.addSubview(pagingViewController.view)
        bgView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
    }
}
