//
//  ChatVC.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var bgView: UIView!
    var footerView = FooterCustomView()
    
    var conversationList = ChatConversasionVC()
    var callList = ChatCallVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVC()
        self.loadCustomFooterView()
    }

  

}
