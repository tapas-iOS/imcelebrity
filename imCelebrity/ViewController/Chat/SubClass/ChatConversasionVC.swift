//
//  ChatConversasionVC.swift
//  imCelebrity
//
//  Created by Paramita  on 18/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol ChatConversasionVCDelegate: class {
    func conversasionDidSelectClkEvent(index: Int)
}

class ChatConversasionVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    weak var delegate: ChatConversasionVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    

}
extension ChatConversasionVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row%2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTextCell", for: indexPath) as? ChatTextCell
            return cell!
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageCell", for: indexPath) as? ChatImageCell
            return cell!
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if indexPath.row%2 == 0 {
            return 100
         }else {
            return 115
        }
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 80))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.conversasionDidSelectClkEvent(index: indexPath.row)
    }
    
}
