//
//  ChatOutgoingCell.swift
//  All-In-One
//
//  Created by CN138 on 25/08/17.
//  Copyright © 2017 Capital Numbers. All rights reserved.
//

import UIKit

class ChatOutgoingCell: UITableViewCell {

    @IBOutlet weak var headerViewHeightConstraintOutlate: NSLayoutConstraint!
    @IBOutlet weak var testLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var msgOutgoingTimeLbl: UILabel!
    @IBOutlet weak var chatTextWidthConstraintOutlate: NSLayoutConstraint!
    @IBOutlet weak var bgCircleView: UIView!
    @IBOutlet weak var chatTextLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgCircleView.addCornerRadious(radious: 5)
        bgCircleView.addShadow(offset: CGSize(width: -1, height: 1), color: UIColor.lightGray, opacity: 0.5, radius: 0.5)
        self.testLbl.clipsToBounds = true
        self.testLbl.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func cellDetails(obj: ChatOBJ, type: String) {
        self.chatTextLbl?.sizeToFit()
        self.chatTextLbl?.adjustsFontSizeToFitWidth = true
        self.chatTextLbl?.layer.masksToBounds = true
        //cell.ChatTextLabel?.layer.cornerRadius = 10;
        self.chatTextLbl.text = obj.chatMessage
        self.msgOutgoingTimeLbl.text = obj.time
        if(type == "group") {
            self.nameLbl.isHidden = false
            self.nameLbl.text = "you"
        }else {
            self.nameLbl.isHidden = true
        }
    }

}
