//
//  ChatDetailsViewController.swift
//  Phylam
//
//  Created by CN138 on 09/04/18.
//  Copyright © 2018 Phylam. All rights reserved.
//

import UIKit
import AssetsLibrary
import MobileCoreServices
import AVFoundation
import AVKit

class ChatDetailsViewController: UIViewController {
    
    @IBOutlet weak var viewLeadingConstantOutlate: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var chatDetailsTblView: UITableView!
    @IBOutlet weak var chatTxtView: FloatLabelTextView!
    @IBOutlet weak var textViewHeigntConstraintOutlate: NSLayoutConstraint!
    @IBOutlet weak var commentedViewBottomConstraintOutlate: NSLayoutConstraint!
    var ChatTextArray = Array<ChatOBJ>()
    var strEndtime = NSNumber()
    var oldMessageAllLoad = Bool()
    var channelName = String()
    let imagePicker = UIImagePickerController()
    var isTypingStatus = Bool()
    var typingUserName = String()
    var isUploadPhoto = Bool()
    var chatType = String()
    var headerSectionCount = Int()
    var getDate = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupTextView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.chatTxtView.removeObserver(self, forKeyPath: "contentSize")
    }
    func configure() {
        oldMessageAllLoad = false
        isTypingStatus = false
        self.headerSectionCount = 1
        chatDetailsTblView.estimatedRowHeight = 50
        chatDetailsTblView.rowHeight = UITableView.automaticDimension
        self.footerView.addShadow(offset: CGSize(width: -1, height: 1), color: UIColor.lightGray, opacity: 0.5, radius: 0.5)
//        self.userImgView.sd_setImage(with: NSURL(string: objFollowerModel.picBig) as URL?, placeholderImage:UIImage(named:"HistobookBannerNoImg"))
    }
    
    
    
    
    @IBAction func sendBtnAction(_ sender: Any) {
        
    }
    @IBAction func attchedBtnAction(_ sender: Any) {
        
    }
    
}
extension ChatDetailsViewController {
    //MARK: - KEYBOARD HIDE/UNHIDE
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.tableViewBtmConstraintChange(btmConstraint: self.commentedViewBottomConstraintOutlate, value: keyboardSize.height)
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.tableViewBtmConstraintChange(btmConstraint: self.commentedViewBottomConstraintOutlate, value: 0)
    }
}
extension ChatDetailsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row%2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatOutgoingCell", for: indexPath as IndexPath) as! ChatOutgoingCell
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatIncomingCell", for: indexPath as IndexPath) as! ChatIncomingCell
            return cell
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
extension ChatDetailsViewController {
    //MARK: - Text view dynamic height.......
    func setupTextView() {
        if let txtView = chatTxtView {
            txtView.scrollsToTop = false;
            txtView.backgroundColor = UIColor.clear;
            txtView.isScrollEnabled = true;
            
            txtView.addObserver(self, forKeyPath: "contentSize", options:[ NSKeyValueObservingOptions.old , NSKeyValueObservingOptions.new], context: nil)
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if(chatTxtView.text.isEmpty) {
            chatTxtView.hint = "Type your message..."
            self.viewLeadingConstantOutlate.constant = 40
        }else {
            chatTxtView.hint = ""
            self.viewLeadingConstantOutlate.constant = 0
        }
        let whitespaceSet = CharacterSet.whitespacesAndNewlines
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let changeDict = change, let constraint = textViewHeigntConstraintOutlate, let view = self.chatTxtView {
            
            if object as? NSObject == self.chatTxtView && keyPath == "contentSize" {
                if let oldContentSize = (changeDict[NSKeyValueChangeKey.oldKey] as AnyObject).cgSizeValue,
                    let newContentSize = (changeDict[NSKeyValueChangeKey.newKey] as AnyObject).cgSizeValue {
                    
                    if(newContentSize.height < 120) {
                        constraint.constant = newContentSize.height
                        if(newContentSize.height < 120) {
                            self.view.layoutIfNeeded()
                            let contentOffsetToShowLastLine = CGPoint(x: 0.0, y: view.contentSize.height - view.bounds.height)
                            view.contentOffset = contentOffsetToShowLastLine
                        }else {
                            constraint.constant = 119
                            self.view.layoutIfNeeded()
                            let contentOffsetToShowLastLine = CGPoint(x: 0.0, y: view.contentSize.height - view.bounds.height)
                            view.contentOffset = contentOffsetToShowLastLine
                        }
                    }else {
                        constraint.constant = 119
                        self.view.layoutIfNeeded()
                        let contentOffsetToShowLastLine = CGPoint(x: 0.0, y: view.contentSize.height - view.bounds.height)
                        view.contentOffset = contentOffsetToShowLastLine
                    }
                }
            }
        }
    }
}

