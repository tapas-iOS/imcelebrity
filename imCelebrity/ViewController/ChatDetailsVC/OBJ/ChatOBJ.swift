//
//  ChatOBJ.swift
//  Phylam
//
//  Created by CN138 on 10/04/18.
//  Copyright © 2018 Phylam. All rights reserved.
//

import UIKit

class ChatOBJ: NSObject {
    var chatUserName = String()
    var chatMessage = String()
    var chatUUID = String()
    var clientidentifier = String()
    var time = String()
    var uploadtype = String()
    var imageUrl = String()
    var videoUrl = String()
    var videoThumbPath = String()
    var headerCount = Int()
    var OriginalDate =  String()
    
}
