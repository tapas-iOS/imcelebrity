//
//  Connects+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ConnectsVC {
    @IBAction func homeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func profileBtnAction(_ sender: Any) {
    }
    @IBAction func notificationBtnAction(_ sender: Any) {
        let notificationVC = UIStoryboard.NotificationsStoryBoard().instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func filterBtnAction(_ sender: Any) {
        self.showFilterView()
    }
    @IBAction func exploreBtnAction(_ sender: Any) {
    }
}
