//
//  Connects+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ConnectsVC {
    func loadCustomView() {
        self.loadPlusButtonPopupView()
        self.loadCustomFooterView()
        self.loadSuggestionPopupView()
        self.loadCustomFilterView()
    }
    func loadCustomFilterView() {
        filterPopupView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(filterPopupView)
        
        filterPopupView.translatesAutoresizingMaskIntoConstraints = false
        filterPopupView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        filterPopupView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        if #available(iOS 11.0, *) {
            filterPopupView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            filterPopupView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        }
        filterPopupView.leadingConstraintOutlate = filterPopupView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: self.view.frame.width)
        filterPopupView.leadingConstraintOutlate.isActive = true
        
        self.filterPopupView.currentFilterTypeIndex = 1
        self.filterPopupView.configureFilter(typeIndex: .Connects)
        filterPopupView.delegate = self 
    }
    func loadSuggestionPopupView() {
        self.view.loadSuggestionCustomView(suggestionView: suggestioncustomView)
        self.suggestioncustomView.isHidden = false
        self.suggestioncustomView.setupConnectsText()
    }
    func loadPlusButtonPopupView() {
        self.loadPlusButttonCustomView(popupView: plusButtonPopup)
        self.plusButtonPopup.ConfigureViewAsPerViewOpen(openPopup: .Connects)
    }
    func loadPlusButttonCustomView(popupView: PlusButtonOptionPopup) {
        plusButtonPopup.frame = CGRect(x: 0, y: 0, width: Int(plusButtonPopup.frame.size.width), height: 100)
        self.view.addSubview(plusButtonPopup)
        
        plusButtonPopup.translatesAutoresizingMaskIntoConstraints = false
        plusButtonPopup.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -80).isActive = true
        plusButtonPopup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        plusButtonPopup.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        plusButtonPopup.heightConstraintOutlate = plusButtonPopup.heightAnchor.constraint(equalToConstant: 0)
        plusButtonPopup.heightConstraintOutlate.isActive = true
        self.plusButtonPopup.alpha = 0.0
        
        plusButtonPopup.delegate = self 
    }
    func showFooterPlusView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.plusButtonPopup.heightConstraintOutlate.constant = 200
            self.plusButtonPopup.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideFooterPlusView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.plusButtonPopup.heightConstraintOutlate.constant = 0
            self.plusButtonPopup.alpha = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func showFilterView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.filterPopupView.leadingConstraintOutlate.constant = 0
            self.filterPopupView.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    func hideFilterView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.filterPopupView.leadingConstraintOutlate.constant = self.view.frame.width
            self.filterPopupView.alpha = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
}
