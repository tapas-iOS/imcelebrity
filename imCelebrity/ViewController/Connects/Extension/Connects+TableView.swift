//
//  Connects+TableView.swift
//  imCelebrity
//
//  Created by Paramita  on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ConnectsVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectsListCell", for: indexPath) as? ConnectsListCell
        
        if indexPath.row == currentDeleteIndex {
            cell?.backgroundColor = UIColor.white
        }else {
            cell?.backgroundColor = UIColor.clear
        }
       
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 80))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        if indexPath?.row == self.currentDeleteIndex {
            self.currentDeleteIndex = -1
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        self.currentDeleteIndex = indexPath.row
        self.tableView.reloadData()
        
        let delete = UITableViewRowAction(style: .default, title: "") { (action:UITableViewRowAction, indexPath:IndexPath) in
            print("delete at:\(indexPath)")
        }
        
        let indexPath = IndexPath(row: indexPath.row, section: 0)
        let cell: ConnectsListCell = self.tableView.cellForRow(at: indexPath) as! ConnectsListCell
        
        let backView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: cell.bounds.size.height))
        backView.backgroundColor = UIColor.red
        
        let myImage = UIImageView(frame: CGRect(x: 30, y: backView.frame.size.height/2-20, width: 20, height: 20))
        myImage.image = #imageLiteral(resourceName: "ConnectsListBlock")
        backView.addSubview(myImage)
        
        let lbl = UILabel(frame: CGRect(x: 0, y: myImage.frame.origin.y+myImage.frame.height+5, width: 80, height: 17))
        lbl.text = "Block"
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "OpenSans", size: 15)
        backView.addSubview(lbl)
        
        let imgSize: CGSize = tableView.frame.size
        UIGraphicsBeginImageContextWithOptions(imgSize, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        backView.layer.render(in: context!)
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        delete.backgroundColor = UIColor(patternImage: newImage)
        
        return [delete]
    }

}
extension ConnectsVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.hideFooterPlusView()
    }
}

