//
//  ConnectsVC.swift
//  imCelebrity
//
//  Created by Paramita  on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ConnectsVC: UIViewController {
    
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    var footerView = FooterCustomView()
    var plusButtonPopup = PlusButtonOptionPopup()
    var suggestioncustomView = SuggestionPopupCustomView()
    var filterPopupView = FilterPopup()
    var currentDeleteIndex : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCustomView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    

}
