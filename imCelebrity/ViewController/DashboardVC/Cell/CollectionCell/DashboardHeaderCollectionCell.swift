//
//  DashboardHeaderCollectionCell.swift
//  ImCelebrity
//
//  Created by Paramita  on 21/03/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

class DashboardHeaderCollectionCell: UICollectionViewCell {
    @IBOutlet weak var headerImgView: UIImageView!
    @IBOutlet weak var headerTextLbl: UILabel!
    
    
    func populateAllData(dict: Dictionary<String, Any>) {
        self.headerImgView.image = dict["image"] as? UIImage
        self.headerTextLbl.text = dict["name"] as? String
    }
}
