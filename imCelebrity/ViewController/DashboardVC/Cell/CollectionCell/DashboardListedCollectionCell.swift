//
//  DashboardListedCollectionCell.swift
//  ImCelebrity
//
//  Created by Paramita  on 21/03/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit


class DashboardListedCollectionCell: UICollectionViewCell {
    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var videoThumbImgView: UIImageView!
    @IBOutlet weak var cameraImgView: UIImageView!
    
    func populateAllData(obj: ListDetailsObjectClass, type: Int){
        self.userNameLbl.text = obj.uploadUserName
        
        if Enums.DashboardCellType.Video.rawValue == type {
            self.videoThumbImgView.isHidden = false
            self.cameraImgView.isHidden = true
        }else {
            self.videoThumbImgView.isHidden = true
            self.cameraImgView.isHidden = false
        }
    }
    
}
