//
//  DashboardHeaderCell.swift
//  ImCelebrity
//
//  Created by Paramita  on 21/03/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

protocol DashboardHeaderCellDelegate: class {
    func didSelectClkEvent(index: Int)
}

class DashboardHeaderCell: UITableViewCell {

    
    @IBOutlet weak var headerCollectionView: UICollectionView!
    var headerArr = Array<Dictionary<String, Any>>()
    weak var delegate: DashboardHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func populateAllData(arr: Array<Dictionary<String, Any>>) {
        headerArr = arr
        self.headerCollectionView.reloadData()
    }

}
extension DashboardHeaderCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return headerArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardHeaderCollectionCell", for: indexPath) as? DashboardHeaderCollectionCell
        let dict = headerArr[indexPath.row]
        cell?.populateAllData(dict: dict)
        return cell!
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        let width = (self.frame.size.width - 60)/4
//        return CGSize(width: width, height: width)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/4, height: collectionViewSize/4)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelectClkEvent(index: indexPath.row)
    }
    
}
