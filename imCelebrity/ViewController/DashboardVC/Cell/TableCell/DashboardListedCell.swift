//
//  DashboardListedCell.swift
//  ImCelebrity
//
//  Created by Paramita  on 21/03/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

class DashboardListedCell: UITableViewCell {
    
    @IBOutlet weak var listedCollectionView: UICollectionView!
    var listesArr = Array<ListDetailsObjectClass>()
    var typeStatus = Int()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func populateAllData(arr: Array<ListDetailsObjectClass>, type: Int) {
        listesArr = arr
        typeStatus = type
        self.listedCollectionView.reloadData()
    }

}

extension DashboardListedCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if listesArr.count>0 {
            return listesArr.count
        }else {
            return 10
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardListedCollectionCell", for: indexPath) as? DashboardListedCollectionCell
        if listesArr.count>0 {
            let obj = listesArr[indexPath.row]
            cell?.populateAllData(obj: obj, type: typeStatus) 
        }
        return cell!
    }
    
    

}
