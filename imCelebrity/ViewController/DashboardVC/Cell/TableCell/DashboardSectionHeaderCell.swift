//
//  DashboardSectionHeaderCell.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 18/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class DashboardSectionHeaderCell: UITableViewCell {
    @IBOutlet weak var headertitleLbl: UILabel!
    
    @IBOutlet weak var videoTypeBtnOutlate: UIButton!
    @IBOutlet weak var allLbl: UILabel!
    @IBOutlet weak var buttonOutletAllButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func seuupUI() {
        videoTypeBtnOutlate.layer.borderWidth = 2
        videoTypeBtnOutlate.layer.borderColor = UIColor.red.cgColor
        videoTypeBtnOutlate.addCornerRadious(radious: 13)
    }

}
