//
//  DashboardUnregisteredUserCell.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol DashboardUnregisteredUserCellDelegate: class {
    func RegisterButtonClickEvent()
    func ShowVideoButtonClickEvent()
    func ShowSnapButtonClickEvent()
}

class DashboardUnregisteredUserCell: UITableViewCell {
    weak var delegate: DashboardUnregisteredUserCellDelegate?
    @IBOutlet weak var buttonOutletShowSnap: UIButton!
    @IBOutlet weak var buttonOutletShowVideo: UIButton!
    @IBOutlet weak var imageViewUserIcon: UIImageView!
    @IBOutlet weak var registerButtonOutlet: UIButton!
    @IBOutlet weak var labelHeaderText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerButtonOutlet.addCornerRadious(radious: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func videoButtonAction(_ sender: UIButton) {
    }
    
    @IBAction func registerButtonAction(_ sender: Any) {
        delegate?.RegisterButtonClickEvent()
    }
    
    @IBAction func snapButtonAction(_ sender: Any) {
    }

}
