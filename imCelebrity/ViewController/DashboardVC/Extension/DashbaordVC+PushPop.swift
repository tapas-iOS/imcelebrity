//
//  DashbaordVC+PushPop.swift
//  imCelebrity
//
//  Created by Paramita  on 20/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension DashboardVC {
    
    func moveToLoginVC(){
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    func moveToRegistrationVC(){
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    func moveToVideoListingVC(){
        let videoListingVC = UIStoryboard.videoStoryBoard().instantiateViewController(withIdentifier: "VideoListingVC") as! VideoListingVC
        self.navigationController?.pushViewController(videoListingVC, animated: true)
    }
    func moveToConnectsVC(){
        let connectsVC = UIStoryboard.connectsStoryBoard().instantiateViewController(withIdentifier: "ConnectsVC") as! ConnectsVC
        self.navigationController?.pushViewController(connectsVC, animated: true)
    }
    func moveToFansVC(){
        let fansVC = UIStoryboard.fansStoryBoard().instantiateViewController(withIdentifier: "FansVC") as! FansVC
        self.navigationController?.pushViewController(fansVC, animated: true)
    }
    
    func moveToSnapVC(){
        let snapVC = UIStoryboard.SnapStoryBoard().instantiateViewController(withIdentifier: "SnapListingVC") as! SnapListingVC
        self.navigationController?.pushViewController(snapVC, animated: true)
    }
    func moveToFavouritesVC(){
        let favouritesVC = UIStoryboard.favouriteStoryBoard().instantiateViewController(withIdentifier: "FavouriteListingVC") as! FavouriteListingVC
        self.navigationController?.pushViewController(favouritesVC, animated: true)
    }
    func moveToSettingsVC(){
        let settingsVC = UIStoryboard.settingsStoryBoard().instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    func moveToSearchVC(){
        let searchVC = UIStoryboard.searchStoryBoard().instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    func moveToChatVC(){
        let chatVC = UIStoryboard.chatStoryBoard().instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    func moveToMusicVC(){
        let musicVC = UIStoryboard.musicStoryBoard().instantiateViewController(withIdentifier: "MusicListingVC") as! MusicListingVC
        self.navigationController?.pushViewController(musicVC, animated: true)
    }
    
}
