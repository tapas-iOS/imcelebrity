//
//  DashbaordVC+TableCellDelegate.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation

extension DashboardVC: DashboardUnregisteredUserCellDelegate{
    func RegisterButtonClickEvent() {
        self.moveToRegistrationVC()
    }
    
    func ShowVideoButtonClickEvent() {
        
    }
    
    func ShowSnapButtonClickEvent() {
        
    }
    
    
}
extension DashboardVC : LogoutCustomViewDelegate {
    func noBtnClkEvent() {
        appDelegate.customLogoutView.isHidden = true
        self.removeAnimate(popupView: appDelegate.customLogoutView)
    }
    
    func yesBtnClkEvent() {
        appDelegate.customLogoutView.isHidden = true
        UserDefaults.standard.set(nil, forKey: Constant.UserDefaultKeyName.AccessToken)
        self.removeAnimate(popupView: appDelegate.customLogoutView)
        self.dashboardTableView.reloadData()
    }
    
    
}
extension DashboardVC : FooterCustomViewDelegate {
    func profileBtnClkEvent() {
        
    }
    
    func searchBtnClkEvent() {
        self.moveToSearchVC()
    }
    
    func messageBtnClkEvent() {
        
    }
    
    func callBtnClkEvent() {
        
    }
    
    func middleBtnClkEvent() {
        self.plusButtonPopup.ConfigureViewAsPerViewOpen(openPopup: .Dashboard)
        self.showFooterPlusView()
    }
    
    
}
extension DashboardVC : DashboardHeaderCellDelegate {
    func didSelectClkEvent(index: Int) {
        switch index {
        case 0:
            self.moveToVideoListingVC()
            break
        case 1:
            self.moveToSnapVC()
            break
        case 2:
            self.moveToChatVC()
            break
        case 3:
            self.moveToMusicVC()
            break
        case 4:
            self.moveToConnectsVC()
            break
        case 5:
            self.moveToFansVC()
            break
        case 6:
            self.moveToFavouritesVC()
            break
        case 7:
            self.moveToSettingsVC()
            break
        default:
            break
        }
    }
    
    
}

