//
//  DashboardVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 20/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension DashboardVC {
    @IBAction func ProfileBtnAction(_ sender: Any) {
        let profileVC = UIStoryboard.ProfileStoryBoard().instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func notificationButtonAction(_ sender: UIButton) {
        let notificationVC = UIStoryboard.NotificationsStoryBoard().instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @objc func showAllButtonAction(sender : UIButton){
        if(sender.tag == 0){
//            self.moveToDetailsVC()
        }else{
            self.commonAlertMessage(message: "You are selected wrong index. Please select first Index")
        }
    }
}
