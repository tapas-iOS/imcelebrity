//
//  DashboardVC+Configuration.swift
//  ImCelebrity
//
//  Created by Paramita  on 21/03/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

extension DashboardVC {
    /*
     1. Popular Videos
     2. Popular Snaps
     3. Favourite Videos
     4. Favourite Snaps
     5. Fans Videos
     6. Fans Snaps
     7. Connects Videos
     8. Connects Snaps
     9. Celebs Videos
     10. Celebs Snaps
     */
    func createDummyArr() {
        listedArr = [
            [
                "name"      : "Video",
                "image": UIImage(named: "Video")!,
                "headerText"  : "Viral Videos"
            ],
            [
                "name"      : "Snap",
                "image": UIImage(named: "Snap")!,
                "headerText"  : "Viral Snap"
            ],
            [
                "name"      : "Chat",
                "image": UIImage(named: "Chat")!,
                "headerText"  : "Viral Chat"
            ],
            [
                "name"      : "Call",
                "image": UIImage(named: "Call")!,
                "headerText"  : "Viral Call"
            ],
            [
                "name"      : "Connects",
                "image": UIImage(named: "Connects")!,
                "headerText"  : "Viral Connects"
            ],
            [
                "name"      : "Fans",
                "image": UIImage(named: "Fan")!,
                "headerText"  : "Viral Fans"
            ],
            [
                "name"      : "Favorite",
                "image": UIImage(named: "Favorite")!,
                "headerText"  : "Viral Favorite"
            ],
            [
                "name"      : "Settings",
                "image": UIImage(named: "Settings")!,
                "headerText"  : "Viral Settings"
            ]
        ]
    }
    
    func addingTheShadowOnView(){
        if(CommonClass.isALoginUser()){
            self.viewNavigationHeader.addShadow(location: VerticalLocation.bottom)
        }
    }
    func showFooterPlusView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.plusButtonPopup.heightConstraintOutlate.constant = 200
            self.plusButtonPopup.alpha = 1.0
            //            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideFooterPlusView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.plusButtonPopup.heightConstraintOutlate.constant = 0
            self.plusButtonPopup.alpha = 0.0
            //            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func loadCustomView() {
        self.view.loadFooterCustomView(footerView: footerView)
        self.footerView.delegate = self
        self.setupUIFooterView()
    }
    
    func setupUIFooterView() {
        self.footerView.shadowViewBgColor = UIColor.white
        self.footerView.middleImg = #imageLiteral(resourceName: "PlusIcon")
        self.footerView.middleBgImg = #imageLiteral(resourceName: "PlusBgIcon")
        self.footerView.leftFirstImg = #imageLiteral(resourceName: "FooterProfileIcon")
        self.footerView.leftSecondImg = #imageLiteral(resourceName: "FooterSearchIcon")
        self.footerView.rightFirstImg = #imageLiteral(resourceName: "FooterCallIcon")
        self.footerView.rightSecondImg = #imageLiteral(resourceName: "FooterMsgIcon")
    }
    
    func loadPlusButtonPopupView() {
        self.loadPlusButttonCustomView(popupView: plusButtonPopup)
        
    }
    
    func showAnimate(popupView:UIView)
    {
        popupView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        popupView.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            popupView.alpha = 1.0
            popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate(popupView:UIView)
    {
        UIView.animate(withDuration: 0.25, animations: {
            popupView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            popupView.alpha = 0.0
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                //                    popupView.removeFromSuperview()
            }
        });
    }
    func fetchJSON(){
        if let path = Bundle.main.path(forResource: "Dashboard", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print("JSON Result is: \(jsonResult)")
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    let resultArray = jsonResult["result"] as! Array<[String: Any]>
                    self.arrayDashbaordDetails = DashbaordDetailsObjectClass.fetchDashboardDetails(dashbaordDetailsArray: resultArray) as! [DashbaordDetailsObjectClass]
                    print("Details: \(self.arrayDashbaordDetails[0].name)")
                }
            } catch {
                // handle error
            }
        }
    }
}
