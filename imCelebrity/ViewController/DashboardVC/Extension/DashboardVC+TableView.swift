//
//  DashboardVC+TableView.swift
//  ImCelebrity
//
//  Created by Paramita  on 21/03/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

extension DashboardVC: UITableViewDelegate, UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int  {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayDashbaordDetails.count*2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row%2 == 0) {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "DashboardSectionHeaderCell", for: indexPath) as! DashboardSectionHeaderCell
            headerCell.backgroundColor = UIColor.white
            headerCell.tag = indexPath.row/2
            headerCell.buttonOutletAllButton.addTarget(self, action: #selector(self.showAllButtonAction(sender:)), for: .touchUpInside)
            let obj = arrayDashbaordDetails[indexPath.row/2]
            headerCell.headertitleLbl.text = obj.name
            return headerCell
        }else {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "DashboardListedCell", for: indexPath) as? DashboardListedCell
            let obj = arrayDashbaordDetails[indexPath.row/2]
            cell?.populateAllData(arr: obj.listDetails, type: obj.type)
            cell?.selectionStyle = .none
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if(indexPath.row%2 == 0) {
            return 40
         }else {
            return 170
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(CommonClass.isKeyPresentInUserDefaults(key: Constant.UserDefaultKeyName.AccessToken)){
            let cell =  tableView.dequeueReusableCell(withIdentifier: "DashboardHeaderCell") as? DashboardHeaderCell
            cell?.selectionStyle = .none
            cell?.delegate = self
            cell?.populateAllData(arr: listedArr)
            return cell!
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "DashboardUnregisteredUserCell") as? DashboardUnregisteredUserCell
            cell!.delegate = self
            cell?.selectionStyle = .none
            return cell!
        }

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(CommonClass.isKeyPresentInUserDefaults(key: Constant.UserDefaultKeyName.AccessToken)){
            let NumbrOfCell  = listedArr.count
            let numbrOfHorizontalCell : Int = Int(round(Double(NumbrOfCell)/4))
            let width : Int = Int((self.view.frame.size.width - 60)/4)
            print("\(numbrOfHorizontalCell)")
            print("\(width)")
            let height = (numbrOfHorizontalCell*width)+(numbrOfHorizontalCell*10)+40
            return CGFloat(height)
        }else{
            return 220
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 80))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }
}


extension DashboardVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.hideFooterPlusView()
    }
}
