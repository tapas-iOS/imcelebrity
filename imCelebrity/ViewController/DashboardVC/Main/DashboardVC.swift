//
//  DashboardVC.swift
//  ImCelebrity
//
//  Created by Paramita  on 21/03/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {

    @IBOutlet weak var viewNavigationHeader: UIView!
    @IBOutlet weak var viewUserIcon: UIView!
    @IBOutlet weak var notificationButtonOutlet: UIButton!
    @IBOutlet weak var dashboardTableView: UITableView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var listedArr = Array<Dictionary<String, Any>>()
    var arrayDashbaordDetails = Array<DashbaordDetailsObjectClass>()
    var footerView = FooterCustomView()
    var plusButtonPopup = PlusButtonOptionPopup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchJSON()
        self.loadPlusButtonPopupView()
        self.loadCustomView()
        self.createDummyArr()
        self.addingTheShadowOnView()
        if(CommonClass.isALoginUser()){
            self.viewUserIcon.isHidden = false
            self.notificationButtonOutlet.isHidden = false
        }else{
            self.viewUserIcon.isHidden = true
            self.notificationButtonOutlet.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
}
