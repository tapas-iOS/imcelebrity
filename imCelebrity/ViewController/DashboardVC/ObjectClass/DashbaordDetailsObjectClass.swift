//
//  DashbaordDetailsObjectClass.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class DashbaordDetailsObjectClass: NSObject {
    
    var listDetails : [ListDetailsObjectClass]!
    var name : String!
    var type : Int!
    var typeString : String!

    class func fetchDashboardDetails(dashbaordDetailsArray: Array<[String:Any]>?) -> Array<Any>? {
        guard (dashbaordDetailsArray != nil) else { return nil }
        guard dashbaordDetailsArray?.count != 0 else { return nil }
        var arrList: NSMutableArray?
        for dic in dashbaordDetailsArray!{
            let detailsObj : DashbaordDetailsObjectClass = DashbaordDetailsObjectClass.init(dictionary: dic)
            if arrList == nil {
                arrList = NSMutableArray.init()
            }
            arrList?.add(detailsObj)
        }

        let listArray = arrList! as NSArray as? [DashbaordDetailsObjectClass]
        return listArray
    }
    
    internal convenience init(dictionary : Dictionary<String, Any>) {
        self.init()
        name = dictionary["name"] as? String
        type = dictionary["type"] as? Int
        typeString = dictionary["typeString"] as? String
        listDetails = [ListDetailsObjectClass]()
        if let detailsArray = dictionary["details"] as? [[String:Any]]{
            for dic in detailsArray{
                let value = ListDetailsObjectClass(dictionary: dic)
                listDetails.append(value)
            }
        }
    }
    
}
