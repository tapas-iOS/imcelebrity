//
//  ListDetailsObjectClass.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ListDetailsObjectClass: NSObject {
    
    var backgroundImageUrl : String!
    var backgroundVideoUrl : String!
    var descriptionField : String!
    var liked : Int!
    var thumnailImage : String!
    var uploadedUserId : Int!
    var uploadedUserImageUrl : String!
    var uploadUserName : String!

    class func fetchListDetails(dashbaordListDic: Dictionary<String, Any>?) -> Array<Any>? {
        guard (dashbaordListDic != nil) else { return nil }
        guard dashbaordListDic?.count != 0 else { return nil }
        var arrList: NSMutableArray?
        let detailsObj : ListDetailsObjectClass = ListDetailsObjectClass.init(dictionary: (dashbaordListDic)!)
        if arrList == nil {
            arrList = NSMutableArray.init()
        }
        arrList?.add(detailsObj)
        let listArray = arrList! as NSArray as? [ListDetailsObjectClass]
        return listArray
    }
    
    internal convenience init(dictionary : Dictionary<String, Any>) {
        self.init()
        backgroundImageUrl = dictionary["backgroundImageUrl"] as? String
        backgroundVideoUrl = dictionary["backgroundVideoUrl"] as? String
        descriptionField = dictionary["description"] as? String
        liked = dictionary["liked"] as? Int
        thumnailImage = dictionary["thumnailImage"] as? String
        uploadedUserId = dictionary["UploadedUserId"] as? Int
        uploadedUserImageUrl = dictionary["UploadedUserImageUrl"] as? String
        uploadUserName = dictionary["uploadUserName"] as? String
    }

}
