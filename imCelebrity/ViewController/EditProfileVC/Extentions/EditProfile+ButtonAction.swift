//
//  EditProfile+ButtonAction.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension EditProfileVC{
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notificationBtnAction(_ sender: Any) {
        let notificationVC = UIStoryboard.NotificationsStoryBoard().instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    @IBAction func profileBtnAction(_ sender: Any) {
    }
    @IBAction func uploadPhotoBtnAction(_ sender: Any) {
        self.openActionSheet()
    }
    
    
    @objc func doneButtonClicked(sender: UIButton){
        print(accessaryView.tag)
        if accessaryView.tag == 1 {
            self.resignFirstResponder()
            self.view.endEditing(true)
            let indexpath = IndexPath(row: 1, section: 0)
            let cell = self.editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.text = changeDateTimeFormat(inputDateTime: dobPicker.date, outPutTimeZone: NSTimeZone.local, outPutFormat: "dd MMMM yyyy")
            self.textInputAttributesArr[1].textFieldText = cell?.txtInput.text ?? ""
            
            let indexpath1 = IndexPath(row: 2, section: 0)
            let cell1 = self.editProfileTableView.cellForRow(at: indexpath1) as? CellTextFieldModel
            cell1?.txtInput.becomeFirstResponder()
        }else if(self.accessaryView.tag == 2){
            let indexpath1 = IndexPath(row: 3, section: 0)
            let cell1 = self.editProfileTableView.cellForRow(at: indexpath1) as? CellTextFieldModel
            cell1?.txtInput.becomeFirstResponder()
        }else if(self.accessaryView.tag == 5){
            let indexpath = IndexPath(row: 5, section: 0)
            let cell = self.editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.text = "\(sexArray[sexPicker.selectedRow(inComponent: 0)])"
            self.textInputAttributesArr[1].textFieldText = cell?.txtInput.text ?? ""
            
            let indexpath1 = IndexPath(row: 6, section: 0)
            let cell1 = self.editProfileTableView.cellForRow(at: indexpath1) as? CellTextFieldModel
            cell1?.txtInput.becomeFirstResponder()
        }
    }
    
    @objc func cancelButtonClicked(sender:UIButton){
        self.view.endEditing(true)
    }
}

extension EditProfileVC : CellButtonModelDelegate {
    func saveButtonAction() {
        print("IDIIDIDIDIDIDIDIDIDID")
    }
}
