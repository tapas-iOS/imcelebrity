//
//  EditProfile+ImagePicker.swift
//  imCelebrity
//
//  Created by Paramita  on 31/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
import CropViewController

extension EditProfileVC {
    func openActionSheet() {
        self.showActionSheetForShowingPhotoPickerType({ (pickerType) in
            guard let pickerType = pickerType else {return}
            if pickerType == .camera{
                self.openCamera()
            } else {
                self.openGallery()
            }
        })
    }
    func openGallery() {
        self.imagePicker.delegate = self
        self.takePhotoFromGallery(imagePicker: imagePicker)
    }
    
    func openCamera() {
        self.imagePicker.delegate = self
        self.takePhotoFromCamera(imagePicker: imagePicker)
    }
}
extension EditProfileVC : CropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: image)
            cropController.delegate = self
            cropController.cropView.aspectRatioLockEnabled = true
            cropController.aspectRatioPickerButtonHidden = true
            cropController.resetAspectRatioEnabled = false
            cropController.aspectRatioPreset = .presetSquare
            
            picker.dismiss(animated: true, completion: {
                self.navigationController?.present(cropController, animated: true, completion: nil)
            })
        }
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        cropViewController.dismiss(animated: true) {
            
            DispatchQueue.global(qos: .default).async(execute: {
                DispatchQueue.main.async(execute: {
                    self.profileBigUserIcon.image = image
                })
            })
        }
    }
}


