//
//  EditProfileVC+Configuration.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension EditProfileVC{
    func addingTheShadowOnView(){
        self.viewNavigationHeader.addShadow(location: VerticalLocation.bottom)
        self.profileBigUserIcon.addCornerRadious(radious: self.profileBigUserIcon.frame.width/2)
        self.profileBigUserIcon.addBorder(borderWidth: 2, color: UIColor.white)
    }
    
    func registerNib() {
        self.editProfileTableView.separatorStyle = .none
        self.editProfileTableView.register(UINib(nibName: textCellId, bundle: nil), forCellReuseIdentifier: textCellId)
        self.editProfileTableView.register(UINib(nibName: buttonCellId, bundle: nil), forCellReuseIdentifier: buttonCellId)
    }
    
    func configureTextInput() {
        textInputAttributesArr = [
            TextInputAttributes.init(placeholderText: "Name", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Name should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false,isShowCountryCodes: false, isShowDropdownArrow: false),
            TextInputAttributes.init(placeholderText: "Date of Birth", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Date of Birth should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: true,isShowCountryCodes: false, isShowDropdownArrow: false),
            TextInputAttributes.init(placeholderText: "Mobile Number", textFieldText: "", keyboardType: .numberPad, isSecureTextEntry: false, errorMessage: "Mobile Number should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false,isShowCountryCodes: true, isShowDropdownArrow: false),
            TextInputAttributes.init(placeholderText: "Email ID", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Email should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false),
            TextInputAttributes.init(placeholderText: "Profession", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Please Select Any Profession", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true),
            TextInputAttributes.init(placeholderText: "Gender", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Please Select Any Gender", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true),
            TextInputAttributes.init(placeholderText: "Role Models", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Role Models should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false),
            TextInputAttributes.init(placeholderText: "Skills", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Please Select Any Skills", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true),
            TextInputAttributes.init(placeholderText: "Address", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Address should not be empty", returnKey: .done, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false)]
    }
    func setTextFieldAccessoryViews(){
        accessaryView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:44)
        accessaryView.backgroundColor = .red
        
        let doneButton : UIButton = UIButton()
        doneButton.frame = CGRect(x:SCREENRECT.width-60, y:0, width:60, height:44)
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonClicked(sender:)), for: .touchUpInside)
        doneButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(doneButton)
        
        let cancelButton : UIButton = UIButton()
        cancelButton.frame = CGRect(x:10, y:0, width:60, height:44)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked(sender:)), for: .touchUpInside)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(cancelButton)
        
        dobPicker.frame = CGRect(x:0, y:0, width:SCREENRECT.width, height:216)
        dobPicker.datePickerMode = .date
        dobPicker.maximumDate = Date()
        dobPicker.backgroundColor = .white
        
        sexPicker.frame = CGRect(x:0, y:0, width:SCREENRECT.width, height:216)
        sexPicker.delegate = self
        sexPicker.dataSource = self
        sexPicker.backgroundColor = .white
    }
    func getCounrtyPhoneCode(){
        let currentLocale = NSLocale.current
        guard let countryCode = currentLocale.regionCode else {return}
        self.counrtyPhoneCode = getCountryPhonceCode(countryCode) ?? ""
        self.countryCode = getCountryCode(countryCode) ?? ""
        
        let indexpath = IndexPath(row: 3, section: 0)
        let cell = editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.leftBtn.setTitle(counrtyPhoneCode, for: .normal)
    }
    
    func getCountryPhonceCode(_ countryCode : String) -> String?{
        let countries = CommonClass.getCountries()
        guard let index = countries.firstIndex(where: {$0.code == countryCode}) else {return nil}
        guard let phoneCode = countries[index].phoneCode else {return nil}
        return phoneCode
    }
    func getCountryCode(_ countryCode : String) -> String?{
        let countries = CommonClass.getCountries()
        guard let index = countries.firstIndex(where: {$0.code == countryCode}) else {return nil}
        guard let code = countries[index].code else {return nil}
        return code
    }
    
}

extension EditProfileVC : CellTextFieldModelDelegate {
    func rightButtonClkAction(index: Int) {
        if index == 1 {
            let indexpath = IndexPath(row: 2, section: 0)
            let cell = self.editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.inputView = dobPicker
            cell?.txtInput.inputAccessoryView = accessaryView
            cell?.txtInput.becomeFirstResponder()
        }else if index == 3 {
            let indexpath: IndexPath = NSIndexPath(row: index+1, section: 0) as IndexPath
            let cell = self.editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            if isShowPassword {
                isShowPassword = false
                cell?.txtInput.isSecureTextEntry = true
                cell?.rightBtn.setImage(#imageLiteral(resourceName: "PasswordHide"), for: .normal)
            }else {
                isShowPassword = true
                cell?.txtInput.isSecureTextEntry = false
                cell?.rightBtn.setImage(#imageLiteral(resourceName: "PasswordShow"), for: .normal)
            }
        }
    }
    
    func leftButtonClkAction() {
        let searchCountryCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchCountryCodeVC") as! SearchCountryCodeVC
        searchCountryCodeVC.delegate = self
        searchCountryCodeVC.isShowCountryDetails = true
        
        self.navigationController?.pushViewController(searchCountryCodeVC, animated: true)
    }
    
}
extension EditProfileVC : SearchCountryCodeVCDelegate {
    func didSelect(country: Country) {
        guard let phoneCode = country.phoneCode else {return}
        let indexpath = IndexPath(row: 3, section: 0)
        let cell = self.editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.leftBtn.setTitle(phoneCode, for: .normal)
        
        self.counrtyPhoneCode = phoneCode
        self.countryCode = country.code ?? ""
    }
}
