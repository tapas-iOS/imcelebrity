//
//  EditProfileVC+TableView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension EditProfileVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.textInputAttributesArr.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == self.textInputAttributesArr.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: buttonCellId, for: indexPath) as!
            CellButtonModel
            cell.cellButtonOutlet.setTitle("Save", for: .normal)
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath) as! CellTextFieldModel
            if(indexPath.row == 4 || indexPath.row == 7) {
                cell.txtInput.isUserInteractionEnabled = false
            }else {
                cell.txtInput.isUserInteractionEnabled = true
            }
            let attText = textInputAttributesArr[indexPath.row]
            cell.populateAllData(attributes: attText)
            cell.rightBtn.tag = indexPath.row
            cell.txtInput.delegate = self 
            cell.txtInput.tag = indexPath.row
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4 || indexPath.row == 7 {
            let searchCountryCodeVC = UIStoryboard.otherStoryBoard().instantiateViewController(withIdentifier: "SearchCountryCodeVC") as! SearchCountryCodeVC
            searchCountryCodeVC.currentIndex = indexPath.row
            searchCountryCodeVC.delegate1 = self
            searchCountryCodeVC.isShowCountryDetails = false
            self.navigationController?.pushViewController(searchCountryCodeVC, animated: true)
        }
    }
    
}
