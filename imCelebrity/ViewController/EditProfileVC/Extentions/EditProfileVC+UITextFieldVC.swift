//
//  EditProfileVC+UITextFieldVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import  UIKit


extension EditProfileVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            accessaryView.tag = textField.tag
            textField.inputView = dobPicker
            textField.inputAccessoryView = accessaryView
        }else if textField.tag == 2 {
            accessaryView.tag = textField.tag
            textField.inputAccessoryView = accessaryView
        }else if(textField.tag == 5){
            accessaryView.tag = textField.tag
            textField.inputView = sexPicker
            textField.inputAccessoryView = accessaryView
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag >= 0 && textField.tag <= 0 {
            let indexpath = IndexPath(row: textField.tag+2, section: 0)
            let cell = self.editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.becomeFirstResponder()
            return false
        }else {
            self.view.endEditing(true)
            return true
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let txtAfterUpdate = textField.text! as NSString
        let updateText: String = txtAfterUpdate.replacingCharacters(in: range, with: string) as String
        
        textInputAttributesArr[textField.tag].textFieldText = updateText
        return true
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            print("KEYBAORD HEIGHT:  \(keyboardHeight)")
            self.keybaordHeight = keyboardHeight
            self.editProfileTableButtonConstraint.constant = keyboardHeight - 10
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.editProfileTableButtonConstraint.constant = 0
    }
    
}

extension EditProfileVC : SearchCountryCodeVCListDelegate {
    func didSelect(str: String, index: Int) {
        let indexpath = IndexPath(row: index, section: 0)
        let cell = self.editProfileTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = str
        
        let attText = self.textInputAttributesArr[indexpath.row]
        if var att = attText as? TextInputAttributes {
            att.textFieldText = cell?.txtInput.text ?? ""
            self.textInputAttributesArr[index] = att
        }
    }
}
