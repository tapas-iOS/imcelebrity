//
//  EditProfileVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {
    
    let textCellId = "CellTextFieldModel"
    let buttonCellId = "CellButtonModel"
    
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var profileBigUserIcon: UIImageView!
    @IBOutlet weak var editProfileTableButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewNavigationHeader: UIView!
    @IBOutlet weak var editProfileTableView: UITableView!{
        didSet{
            self.editProfileTableView.delegate = self
            self.editProfileTableView.dataSource = self
        }
    }
    
    var textInputAttributesArr = Array<TextInputAttributes>()
    let dobPicker = UIDatePicker()
    let isdPicker = UIPickerView()
     let sexPicker = UIPickerView()
    var accessaryView = UIView()
    let countryAccessaryView = UIView()
    let SCREENRECT = UIScreen.main.bounds
    var isShowPassword = false
    lazy var counrtyPhoneCode = ""
    lazy var countryCode = ""
    let sexArray = ["MALE", "FEMALE", "OTHER"]
    var isTableViewUp:Bool = false
    var keybaordHeight              =  CGFloat()
    
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.addingTheShadowOnView()
        self.registerNib()
        self.configureTextInput()
        self.setTextFieldAccessoryViews()
        self.getCounrtyPhoneCode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
