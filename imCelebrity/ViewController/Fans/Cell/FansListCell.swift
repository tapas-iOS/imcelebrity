//
//  FansListCell.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class FansListCell: UITableViewCell {
    
    @IBOutlet weak var fansBtnOutlate: UIButton!
    @IBOutlet weak var connectBtnOutlate: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func fansBtnAction(_ sender: Any) {
    }
    @IBAction func connectBtnAction(_ sender: Any) {
    }
}
