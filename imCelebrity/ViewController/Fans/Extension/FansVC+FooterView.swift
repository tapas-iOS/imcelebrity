//
//  FansVC+FooterView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension FansVC: FooterCustomViewDelegate {
    func profileBtnClkEvent() {
        
    }
    
    func searchBtnClkEvent() {
        let searchVC = UIStoryboard.searchStoryBoard().instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func messageBtnClkEvent() {
        
    }
    
    func callBtnClkEvent() {
        
    }
    
    func loadCustomFooterView() {
        self.view.loadFooterCustomView(footerView: footerView)
        self.footerView.delegate = self
        self.setupUIFooterView()
    }
    func setupUIFooterView() {
        self.footerView.shadowViewBgColor = UIColor.white
        self.footerView.middleImg = #imageLiteral(resourceName: "PlusIcon")
        self.footerView.middleBgImg = #imageLiteral(resourceName: "FansFooterBgIcon")
        self.footerView.leftFirstImg = #imageLiteral(resourceName: "FooterProfileIcon")
        self.footerView.leftSecondImg = #imageLiteral(resourceName: "FooterSearchIcon")
        self.footerView.rightFirstImg = #imageLiteral(resourceName: "FooterCallIcon")
        self.footerView.rightSecondImg = #imageLiteral(resourceName: "FooterMsgIcon")
    }
    
    func middleBtnClkEvent() {
        self.showFooterPlusView()
    }
}

