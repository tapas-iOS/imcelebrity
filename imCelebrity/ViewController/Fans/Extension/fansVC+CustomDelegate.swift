//
//  fansVC+CustomDelegate.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension FansVC: PlusButtonOptionPopupDelegate{
    func firstButtonSelectionEvent() {
        
    }
    
    func secondButtonSelectionEvent() {
        
    }
    
    func crossButtonSelectionEvent() {
        self.hideFooterPlusView()
    }
    
}

extension FansVC: FilterPopupDelegate {
    func filterCancelBtnAction() {
        self.hideFilterView()
    }
    func filterApplyBtnAction() {
        self.hideFilterView()
    }
    
    func didSelectClkEvent(index: Int) {
        let searchCountryCodeVC = UIStoryboard.otherStoryBoard().instantiateViewController(withIdentifier: "SearchCountryCodeVC") as! SearchCountryCodeVC
        searchCountryCodeVC.currentIndex = index
        searchCountryCodeVC.delegate1 = self
        searchCountryCodeVC.isShowCountryDetails = false
        self.navigationController?.pushViewController(searchCountryCodeVC, animated: true)
    }
}
extension FansVC : SearchCountryCodeVCListDelegate {
    func didSelect(str: String, index: Int) {
        let indexpath = IndexPath(row: index, section: 0)
        let cell = self.filterPopupView.tableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = str
        
        let attText = self.filterPopupView.textInputAttributesArr[indexpath.row]
        if var att = attText as? TextInputAttributes {
            att.textFieldText = cell?.txtInput.text ?? ""
            self.filterPopupView.textInputAttributesArr[index] = att
        }
    }
    
}
