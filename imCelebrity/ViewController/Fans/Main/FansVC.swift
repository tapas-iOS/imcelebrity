//
//  FansVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class FansVC: UIViewController {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var bgView: UIView!
    var footerView = FooterCustomView()
    var plusButtonPopup = PlusButtonOptionPopup()
    var suggestioncustomView = SuggestionPopupCustomView()
    var filterPopupView = FilterPopup()
    
    var fansList = FansListingVC()
    var celebsList = FansCelebsListingVC()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVC()
        self.loadCustomView() 
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }

}
