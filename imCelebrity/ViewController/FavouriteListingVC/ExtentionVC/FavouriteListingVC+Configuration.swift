//
//  VideoListingVC+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 26/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import Parchment

extension FavouriteListingVC{
    
    func loadVC() {
        let storyboard = UIStoryboard(name: "Favourite", bundle: nil)
        favouriteSnapVC = storyboard.instantiateViewController(withIdentifier: "FavouriteSnapListingVC") as! FavouriteSnapListingVC
         favouriteVideoVC = storyboard.instantiateViewController(withIdentifier: "FavouriteVideoListingVC") as! FavouriteVideoListingVC
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [ favouriteVideoVC,favouriteSnapVC])
        pagingViewController.indicatorColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        pagingViewController.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.selectedBackgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.textColor = UIColor.black
        pagingViewController.selectedTextColor = UIColor.black
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        pagingViewController.selectedFont = UIFont.boldSystemFont(ofSize: 15)
        
        // Make sure you add the PagingViewController as a child view
        // controller and constrain it to the edges of the view.
        addChild(pagingViewController)
        bgView.addSubview(pagingViewController.view)
        bgView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
    }
    
    func loadPlusButtonPopupView() {
        self.loadPlusButttonCustomView(popupView: plusButtonPopup)
        
    }

    func showFooterPlusView() {
        self.plusButtonPopup.heightConstraintOutlate.constant = 200
        self.plusButtonPopup.isHidden = false
    }
    
    func hideFooterPlusView() {
        self.plusButtonPopup.heightConstraintOutlate.constant = 0
        self.plusButtonPopup.isHidden = true
    }

}
