//
//  VideoListingVC+FooterView.swift
//  imCelebrity
//
//  Created by Paramita  on 26/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension FavouriteListingVC : FooterCustomViewDelegate {
    func profileBtnClkEvent() {
        
    }
    
    func searchBtnClkEvent() {
        let searchVC = UIStoryboard.searchStoryBoard().instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func messageBtnClkEvent() {
        
    }
    
    func callBtnClkEvent() {
        
    }
    
    func middleBtnClkEvent() {
        self.showFooterPlusView()
    }
    
    
    func loadCustomView() {
        self.view.loadFooterCustomView(footerView: footerView)
        self.footerView.delegate = self
    }
    
    
}
