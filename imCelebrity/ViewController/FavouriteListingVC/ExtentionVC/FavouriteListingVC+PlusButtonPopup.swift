//
//  VideoListingVC+PlusButtonPopup.swift
//  imCelebrity
//
//  Created by Paramita  on 26/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension FavouriteListingVC{
    
    func loadPlusButttonCustomView(popupView: PlusButtonOptionPopup) {
        plusButtonPopup.frame = CGRect(x: 0, y: 0, width: Int(plusButtonPopup.frame.size.width), height: 100)
        self.view.addSubview(plusButtonPopup)
        
        plusButtonPopup.translatesAutoresizingMaskIntoConstraints = false
        plusButtonPopup.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -80).isActive = true
        plusButtonPopup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        plusButtonPopup.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        plusButtonPopup.heightConstraintOutlate = plusButtonPopup.heightAnchor.constraint(equalToConstant: 300)
        plusButtonPopup.heightConstraintOutlate.isActive = true
        
        plusButtonPopup.delegate = self
        self.hideFooterPlusView()
    }
    
}

extension FavouriteListingVC: PlusButtonOptionPopupDelegate{
    func firstButtonSelectionEvent() {
        
    }
    
    func secondButtonSelectionEvent() {
        
    }
    
    func crossButtonSelectionEvent() {
        self.hideFooterPlusView()
    }
    
    
}
