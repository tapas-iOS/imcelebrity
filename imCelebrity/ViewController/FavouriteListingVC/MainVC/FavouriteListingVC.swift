//
//  FavouriteListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class FavouriteListingVC: UIViewController {
    
    @IBOutlet weak var buttonOuletNotification: UIButton!
    @IBOutlet weak var buttonOutletHome: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    var footerView = FooterCustomView()
    var plusButtonPopup = PlusButtonOptionPopup()
    
    var favouriteSnapVC = FavouriteSnapListingVC()
    var favouriteVideoVC = FavouriteVideoListingVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVC()
    }
}
