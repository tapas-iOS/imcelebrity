//
//  VideoListing+TableView.swift
//  ImCelebrity
//
//  Created by Weaver web 5 on 03/04/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

extension FavouriteSnapListingVC : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath as IndexPath) as? SnapListingCollectionCell
        cell?.confuguration(index: indexPath.row)
        cell?.delegate = self
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (self.view.frame.size.width - 40)/3
        return CGSize(width: width, height: width)
    }
    
}

