//
//  FavouriteSnapListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class FavouriteSnapListingVC: UIViewController {
    var cellId = "SnapListingCollectionCell"
    @IBOutlet weak var favouriteSnapsCollectionView: UICollectionView!{
        didSet{
            favouriteSnapsCollectionView.delegate = self
            favouriteSnapsCollectionView.dataSource = self
        }
    }
    
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 3

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCustionCollectionCell()
    }
    
    func loadCustionCollectionCell(){
        self.favouriteSnapsCollectionView.register(UINib(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
    }
}
