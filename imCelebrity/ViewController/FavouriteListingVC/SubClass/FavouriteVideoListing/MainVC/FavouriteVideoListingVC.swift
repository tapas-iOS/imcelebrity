//
//  FavouriteVideoListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class FavouriteVideoListingVC: UIViewController {
    
    var cellId = "GridCollectionViewCell"

    @IBOutlet weak var favouriteVideoCollectionView: UICollectionView!{
        didSet{
            favouriteVideoCollectionView.delegate = self
            favouriteVideoCollectionView.dataSource = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCustionCollectionCell()
    }
    
    func loadCustionCollectionCell(){
        self.favouriteVideoCollectionView.register(UINib(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
    }
    
}
