//
//  ForgetPassword+TextFeild.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension ForgotPasswordVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
            return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let txtAfterUpdate = textField.text! as NSString
        let updateText: String = txtAfterUpdate.replacingCharacters(in: range, with: string) as String
        
        self.textInputAttributesArr[textField.tag].textFieldText = updateText
        
        return true
    }
    
}
