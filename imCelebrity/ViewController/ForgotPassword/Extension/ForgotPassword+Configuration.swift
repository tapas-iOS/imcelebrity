//
//  ForgotPassword+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ForgotPasswordVC {
    func registerNib() {
        self.forgotPasswordTableView.separatorStyle = .none
        self.forgotPasswordTableView.register(UINib(nibName: "CellLogoCell", bundle: nil), forCellReuseIdentifier: logoCellId)
        self.forgotPasswordTableView.register(UINib(nibName: "CellTextFieldModel", bundle: nil), forCellReuseIdentifier: textCellId)
    }
    func configureTextInput() {
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Email/Mobile Number", textFieldText: "", keyboardType: .emailAddress, isSecureTextEntry: false, errorMessage: "Email should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false)]
    }
    
}
