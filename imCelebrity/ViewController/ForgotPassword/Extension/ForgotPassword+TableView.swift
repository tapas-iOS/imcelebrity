//
//  ForgotPassword+TableView.swift
//  imCelebrity
//
//  Created by Paramita  on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ForgotPasswordVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textInputAttributesArr.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: logoCellId, for: indexPath) as! CellLogoCell
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath) as! CellTextFieldModel
            let attText = textInputAttributesArr[indexPath.row-1]
            cell.txtInput.delegate = self
            cell.populateAllData(attributes: attText)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return (self.view.frame.height/2) - 50
        }else {
            return 80
        }
    }
    
    
}

