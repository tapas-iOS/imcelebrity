//
//  ForgotPasswordVC.swift
//  imCelebrity
//
//  Created by Paramita  on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    @IBOutlet weak var forgotPasswordTableView: UITableView!
    @IBOutlet weak var buttonOutletCross: UIButton!
    @IBOutlet weak var viewFooter: UIView!
    
    let logoCellId = "CellLogoCell"
    let textCellId = "CellTextFieldModel"
    let buttonCellId = "CellButtonModel"
    var textInputAttributesArr = Array<TextInputAttributes>()

    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addingTheShadowOnView()
        self.registerNib()
        self.configureTextInput()
    }
    
    func addingTheShadowOnView(){
        self.viewFooter.addShadow(location: VerticalLocation.top)
    }


}
