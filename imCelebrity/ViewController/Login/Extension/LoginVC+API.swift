//
//  LoginVC+API.swift
//  imCelebrity
//
//  Created by Paramita  on 10/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//



import Foundation
import UIKit

extension LoginVC {
    func callLoginWebserviceMethod(){
        print("UserName: \(self.textInputAttributesArr[0].textFieldText) ,, Password:\(self.textInputAttributesArr[1].textFieldText)")
        self.callLogingApi(_userEmail: self.textInputAttributesArr[0].textFieldText ,_userPassword: self.textInputAttributesArr[1].textFieldText.sha1() ) 
    }
    
    func callLogingApi( _userEmail: String ,_userPassword: String ) {
        if CommonClass.isConnectedToNetwork() == true {
            CommonClass.addLoading(view: self.view)
            ApiCalling.userLoginCheck(emailId:_userEmail, userPassword:_userPassword, completion: {
                (result : Any?, error :Error? ) in
                CommonClass.removeLoading(view: self.view)
                
                if (result != nil){
                print("Result:------- \(result!)")
                if let _response = result as? NSDictionary {
                let status = _response["response_code"] as! Int
                if(status == 2000){
                if let responseData = _response["response_data"] as? NSDictionary{
                if let userProfileDetailsDic =  responseData["profile_details"] as? Dictionary<String, Any>{
                let objUserDetailsClass = UserDetailsObject()
                objUserDetailsClass.getUserDetails(userInfoDict: userProfileDetailsDic )
                    
                     self.moveToDashbaordVC()
                    
                }
                }
                }else  if(status == 4001){
                let message = _response["response_message"] as! String
                self.showAutoCancelAlertWithMessage(message: message)
                }else{
                self.showAutoCancelAlertWithMessage(message: "Server Error. Retry after sometime.")
                }
                }else{
                
                self.showAutoCancelAlertWithMessage(message: "Server Error. Retry after sometime.")
                }
                }
            })}else {
            self.showAutoCancelAlertWithMessage(message: Constant.NoInternentMessgae)
        }
    }
    
    func callSocialLogingApi() {
        if CommonClass.isConnectedToNetwork() == true {
            CommonClass.addLoading(view: self.view)
            ApiCalling.userSocialSignupCheck(name : self.social_name, email : self.social_email, imageUrl: self.social_image_url, socialID: self.social_id, sex: self.social_sex, socialType: self.social_type) { (loginModel) in
                CommonClass.removeLoading(view: self.view)
            }}else {
            self.showAutoCancelAlertWithMessage(message: Constant.NoInternentMessgae)
        }
    }

    func fetchResponseData(Dic: NSDictionary){
        
    }
}

