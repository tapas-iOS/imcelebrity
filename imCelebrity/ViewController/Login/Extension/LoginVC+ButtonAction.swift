//
//  LoginVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit


//MARK: -Direct Button Action.......
extension LoginVC {
    @IBAction func registrationButtonAction(_ sender: Any) {
        //self.moveToSignupVC()
        self.popToPreviousVC()
    }
    
    
    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


//MARK: -Custom Delegate Button Action.......
extension LoginVC : CellForgotLabelModelDelegate, CellButtonModelDelegate, CellSocialLoginModelDelegate {
    func forgotPasswordBtnClkEvent() {
        let forgotPasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
    }
    
    func facebookBtnClkEvent() {
      self.getFacebookLoginDetails()
    }
    
    func googlePlusBtnClkEvent() {
        self.configureGoogleSignInDelegate()
    }
    func saveButtonAction() {
        //self.validateUserDetailsForEmailLogin()
        self.callLoginWebserviceMethod()
        
    }
}


extension LoginVC : CellTextFieldModelDelegate {
    func leftButtonClkAction() {
        //Not necessary
    }
    
    func rightButtonClkAction(index: Int) {
        let indexpath: IndexPath = NSIndexPath(row: 2, section: 0) as IndexPath
        let cell = loginTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        if isShowPassword {
            isShowPassword = false
            cell?.txtInput.isSecureTextEntry = true
            cell?.rightBtn.setImage(#imageLiteral(resourceName: "PasswordHide"), for: .normal)
        }else {
            isShowPassword = true
            cell?.txtInput.isSecureTextEntry = false
            cell?.rightBtn.setImage(#imageLiteral(resourceName: "PasswordShow"), for: .normal)
        }
    }
}
