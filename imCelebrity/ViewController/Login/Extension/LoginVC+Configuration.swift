//
//  LoginVC+Configuration.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

extension LoginVC {
    func registerNib() {
        self.loginTableView.separatorStyle = .none
        self.loginTableView.register(UINib(nibName: logoCellId, bundle: nil), forCellReuseIdentifier: logoCellId)
        self.loginTableView.register(UINib(nibName: textCellId, bundle: nil), forCellReuseIdentifier: textCellId)
        self.loginTableView.register(UINib(nibName: forgotCellId, bundle: nil), forCellReuseIdentifier: forgotCellId)
        self.loginTableView.register(UINib(nibName: buttonCellId, bundle: nil), forCellReuseIdentifier: buttonCellId)
        self.loginTableView.register(UINib(nibName: orLblCellId, bundle: nil), forCellReuseIdentifier: orLblCellId)
        self.loginTableView.register(UINib(nibName: socialCellId, bundle: nil), forCellReuseIdentifier: socialCellId)
        
    }
    func configureTextInput() {
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Email/Mobile Number", textFieldText: "", keyboardType: .emailAddress, isSecureTextEntry: false, errorMessage: "Email should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false), TextInputAttributes.init(placeholderText: "Password", textFieldText: "", keyboardType: .default, isSecureTextEntry: true, errorMessage: "Password should not be empty", returnKey: .next, isViewPasswordButton: true, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false)]  
    }
    func configureGoogleSignIn() {
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signOut()
    }
    
}
