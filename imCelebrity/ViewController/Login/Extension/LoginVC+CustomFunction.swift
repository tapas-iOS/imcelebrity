//
//  LoginVC+CustomFunction.swift
//  imCelebrity
//
//  Created by Paramita  on 10/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
import GoogleSignIn
import FirebaseAuth
import FacebookLogin
import FBSDKLoginKit

extension LoginVC{
    func validateUserDetailsForEmailLogin(){
        if(!(self.textInputAttributesArr[0].textFieldText.isEmpty)){
            if((self.textInputAttributesArr[0].textFieldText.isValidEmail())){
                if(!(self.textInputAttributesArr[1].textFieldText.isEmpty)){
                    if(!((self.textInputAttributesArr[1].textFieldText.count) < 4)){
                        print("User Email:: \(self.textInputAttributesArr[0].textFieldText)  |||| User Password:: \(self.textInputAttributesArr[1].textFieldText.isEmpty)")
                        self.callLoginWebserviceMethod()
//                        self.moveToDashbaordVC()
                    }else{
                        self.commonAlertMessage(message: "Please Enter a valid Password")
                    }
                }else{
                    self.commonAlertMessage(message: "Please Enter a Password")
                }
            }else{
                self.commonAlertMessage(message: "Please Enter a valid email id")
            }
        }else{
            self.commonAlertMessage(message: "Please Enter a email ")
        }
    }
    
    func toogleTableViewPostion(changeConstantHeight: CGFloat , isViewUp: Bool) {
        self.view.layoutIfNeeded()
        if isViewUp {
            isTableViewUp = true
            self.buttomConstantviewTableBackground.constant = self.keybaordHeight
        }else{
            isTableViewUp = false
            self.buttomConstantviewTableBackground.constant = 0
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            print("KEYBAORD HEIGHT:  \(keyboardHeight)")
            self.keybaordHeight = keyboardHeight
            self.buttomConstantviewTableBackground.constant = keyboardHeight + 5
        }
    }
    func getFacebookLoginDetails(){
        if let accessToken = FBSDKAccessToken.current(){
            print("\(accessToken)")
            self.getFBSDKAccessToken { (success, accessToken, resultDict, error) in
                //"fields": "id, name, first_name, last_name, picture.type(large), email"
            }
        }else {
            let loginManager = LoginManager()
            loginManager.logIn(readPermissions: [.email], viewController: self){ loginResult in
                switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success:
                    self.getFBSDKAccessToken { (success, accessToken, resultDict, error) in
                        print(resultDict)
                        //Api call...
                        self.social_name = resultDict["name"]! as! String
                        self.social_email = ""//resultDict["email"]! as! String
                        self.social_image_url = ((resultDict["picture"]! as! NSDictionary)["data"] as! NSDictionary)["url"] as! String
                        self.social_id = resultDict["id"]! as! String
                        self.social_type = "FACEBOOK"
                        
                        self.callSocialLogingApi()
                    }
                }
            }
        }
    }
    
    func getGoogleLoginDetails(userDetails: GIDGoogleUser) {
        self.social_name = userDetails.profile.name
        self.social_email = ""//userDetails.profile.email
        self.social_image_url = userDetails.profile.imageURL(withDimension: 0).absoluteString
        self.social_id = userDetails.userID
        self.social_type = "GOOGLE"
        
        self.callSocialLogingApi()
    }
    
}
