//
//  LoginVC+PushPop.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
extension LoginVC {
    func moveToSignupVC(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
        
    func moveToDashbaordVC() {
        
       UserDefaults.standard.set("kjkj8y8880hmhhjhj", forKey: Constant.UserDefaultKeyName.AccessToken)
        
        let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.navigationController?.pushViewController(dashboardVC, animated: true)
    }
    
    func popToPreviousVC(){
        self.navigationController?.popViewController(animated: true)
    }
}
