//
//  LoginVC+TableView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension LoginVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textInputAttributesArr.count + 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: logoCellId, for: indexPath) as! CellLogoCell
            return cell
        }else if(indexPath.row == 1 || indexPath.row == 2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath) as! CellTextFieldModel
            let attText = textInputAttributesArr[indexPath.row-1]
            cell.populateAllData(attributes: attText)
            cell.txtInput.tag = indexPath.row-1
            cell.txtInput.delegate = self
            cell.delegate = self
            return cell
        }else if (indexPath.row == 3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: forgotCellId, for: indexPath) as! CellForgotLabelModel
            cell.delegate = self
            return cell
        }else if (indexPath.row == 4) {
            let cell = tableView.dequeueReusableCell(withIdentifier: buttonCellId, for: indexPath) as! CellButtonModel
            cell.cellButtonOutlet.setTitle("LOGIN", for: .normal)
            cell.delegate = self
            return cell
        }else if (indexPath.row == 5) {
            let cell = tableView.dequeueReusableCell(withIdentifier: orLblCellId, for: indexPath) as! CellOrLable
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: socialCellId, for: indexPath) as! CellSocialLoginModel
            cell.delegate = self 
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return (self.view.frame.height/2) - (self.view.frame.height/5)
        }else {
            return 60//self.view.frame.width * 0.5
        }
    }
}
