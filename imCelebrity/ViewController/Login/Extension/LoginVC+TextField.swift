//
//  LoginVC+TextField.swift
//  imCelebrity
//
//  Created by Paramita  on 10/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag >= 0 && textField.tag <= 0 {
            let indexpath = IndexPath(row: textField.tag+2, section: 0)
            let cell = loginTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.becomeFirstResponder()
            return false
        }else {
            self.view.endEditing(true)
            return true
        }
        
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let txtAfterUpdate = textField.text! as NSString
        let updateText: String = txtAfterUpdate.replacingCharacters(in: range, with: string) as String
        
        textInputAttributesArr[textField.tag].textFieldText = updateText
        
        return true
    }

}
