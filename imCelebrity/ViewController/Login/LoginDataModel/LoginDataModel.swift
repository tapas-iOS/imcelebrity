//
//  LoginDataModel.swift
//  imCelebrity
//
//  Created by Paramita  on 22/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//



import Foundation

struct LoginModel: Codable {
    struct Login: Codable {
        var response_code : Int
        var response_data: ResponseData
        var response_message : String
    }
    
    struct ResponseData: Codable {
        var profile_details : ProfileDetails
    }
    struct ProfileDetails: Codable {
//        var chatToken: String
        var authToken : String
        var profile_type : String
        var city: String
        var country: String
        var country_code: String
        var dob: Int
        var email: String
        var mobile: String
        var name: String
        var profile_pic: String
        var quickblox_status: String
        var sex: String
        var user_id: String
        var verification_status: String
    }
}
