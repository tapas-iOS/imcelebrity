//
//  LoginVCViewController.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var buttonOutletCross: UIButton!
    @IBOutlet weak var loginTableView: UITableView!
    @IBOutlet var buttomConstantviewTableBackground: NSLayoutConstraint!
    
    let logoCellId = "CellLogoCell"
    let socialCellId = "CellSocialLoginModel"
    let textCellId = "CellTextFieldModel"
    let orLblCellId = "CellOrLable"
    let buttonCellId = "CellButtonModel"
    let forgotCellId = "CellForgotLabelModel"
    var textInputAttributesArr = Array<TextInputAttributes>()
    var isTableViewUp               = Bool()
    var keybaordHeight              = CGFloat()
    var isShowPassword = false
    var obj = LoginModel()
    
    //Social..
    var social_name : String = ""
    var social_email : String = ""
    var social_id : String = ""
    var social_sex : String = ""
    var social_type : String = ""
    var social_image_url : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerNib()
        self.configureTextInput()
        self.configureGoogleSignIn()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }

}
