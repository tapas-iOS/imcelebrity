//
//  MusicListingVC+Configuration.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/06/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
import Parchment

extension MusicListingVC {
    func loadVC() {
        let storyboard = UIStoryboard.musicStoryBoard()
        celebMusicVC = storyboard.instantiateViewController(withIdentifier: "MusicCelebsVC") as! MusicCelebsVC
        popularMusicVC = storyboard.instantiateViewController(withIdentifier: "MusicPopularVC") as! MusicPopularVC
        fansMusicVC = storyboard.instantiateViewController(withIdentifier: "MusicFansVC") as! MusicFansVC
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [celebMusicVC, popularMusicVC, fansMusicVC])
        pagingViewController.indicatorColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        pagingViewController.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.selectedBackgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.textColor = UIColor.black
        pagingViewController.selectedTextColor = UIColor.black
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        pagingViewController.selectedFont = UIFont.boldSystemFont(ofSize: 15)
        
        let count = CGFloat(pagingViewController.viewControllers.count)
        let width = self.view.frame.width/count
        pagingViewController.menuItemSize = .fixed(width: width, height: 40)
        
        // Make sure you add the PagingViewController as a child view
        // controller and constrain it to the edges of the view.
        addChild(pagingViewController)
        bgView.addSubview(pagingViewController.view)
        bgView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
    }
}
