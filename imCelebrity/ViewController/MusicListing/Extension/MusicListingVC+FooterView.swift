//
//  MusicListingVC+FooterView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/06/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension MusicListingVC : FooterCustomViewDelegate {
    func profileBtnClkEvent() {
        
    }
    
    func searchBtnClkEvent() {
        let searchVC = UIStoryboard.searchStoryBoard().instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func messageBtnClkEvent() {
        
    }
    
    func callBtnClkEvent() {
        
    }
    
    
    func loadCustomView() {
        self.view.loadFooterCustomView(footerView: footerView)
        self.footerView.delegate = self
        self.setupUIFooterView()
    }
    func setupUIFooterView() {
        self.footerView.shadowViewBgColor = UIColor.black
        self.footerView.middleImg = #imageLiteral(resourceName: "PlusIcon")
        self.footerView.middleBgImg = #imageLiteral(resourceName: "SnapFooterBgIcon")
        self.footerView.leftFirstImg = #imageLiteral(resourceName: "VideoConnectsIcon")
        self.footerView.leftSecondImg = #imageLiteral(resourceName: "VideoSearchIcon")
        self.footerView.rightFirstImg = #imageLiteral(resourceName: "VideoPhoneIcon")
        self.footerView.rightSecondImg = #imageLiteral(resourceName: "ViideoMessageIcon")
    }
    
    func middleBtnClkEvent() {
        //        self.showFooterPlusView()
    }
}

