//
//  MusicListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/06/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class MusicListingVC: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
    
    var footerView = FooterCustomView()
    var celebMusicVC = MusicCelebsVC()
    var popularMusicVC = MusicPopularVC()
    var fansMusicVC = MusicFansVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVC()
        self.loadCustomView()

    }
    

    

}
