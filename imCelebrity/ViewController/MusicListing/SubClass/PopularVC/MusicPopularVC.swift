//
//  MusicPopularVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 08/06/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class MusicPopularVC: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var footerIdentifier = "FooterCellIdentifire"
    var cellId = "MusicCollectionViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureCollectionView()
        
    }


}
extension MusicPopularVC {
    func configureCollectionView() {
        collectionView.register(CollectionViewFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerIdentifier)
        self.collectionView.register(UINib(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
    }
}
extension MusicPopularVC : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath as IndexPath) as? MusicCollectionViewCell
//        cell?.confuguration(index: indexPath.row)
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (self.view.frame.size.width - 30)/2
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width:collectionView.frame.size.width, height:80)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerIdentifier, for: indexPath)
            
            footerView.backgroundColor = UIColor.clear
            return footerView
            
        default:
            let view = UIView()
            return view as! UICollectionReusableView
            //assert(false, "Unexpected element kind")
        }
    }
    
}
