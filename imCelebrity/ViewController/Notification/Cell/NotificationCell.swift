//
//  NotificationCell.swift
//  imCelebrity
//
//  Created by Paramita  on 11/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol NotificationCellDelegate: class {
    func bookmarkBtnClkEvent(index: Int)
    func deleteBtnClkEvent(index: Int)
}

class NotificationCell: UITableViewCell {

    @IBOutlet weak var deleteBtnOutlate: UIButton!
    @IBOutlet weak var bookmarkBtnOutlate: UIButton!
    @IBOutlet weak var notificationTimeLbl: UILabel!
    @IBOutlet weak var NotificationDescriptionLbl: UILabel!
    @IBOutlet weak var notificationNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var cellTypeImgView: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var topBookmarkBtnOutlate: UIButton!
    weak var delegate: NotificationCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backView.addCornerRadious(radious: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func populatrAllData(obj : NotificationOBJ) {
        self.notificationNameLbl.text = obj.title
        self.notificationTimeLbl.text = obj.notificationDate
        
        if obj.isExpand {
            self.NotificationDescriptionLbl.text = obj.longDescription
            self.bookmarkBtnOutlate.isHidden = false
            self.deleteBtnOutlate.isHidden = false
            self.topBookmarkBtnOutlate.isHidden = true
        }else {
            self.NotificationDescriptionLbl.text = obj.shortDescription
            self.bookmarkBtnOutlate.isHidden = true
            self.deleteBtnOutlate.isHidden = true
            if obj.isBookmark {
                self.topBookmarkBtnOutlate.isHidden = false
            }else {
                self.topBookmarkBtnOutlate.isHidden = true
            }
            
        }
        
        if obj.isBookmark {
            self.bookmarkBtnOutlate.setImage(#imageLiteral(resourceName: "NotificationSelectBookmark"), for: .normal)
        }else {
             self.bookmarkBtnOutlate.setImage(#imageLiteral(resourceName: "NotificationDeselectBookmark"), for: .normal)
        }
        
        let type : NotificationType = NotificationType(rawValue: obj.notificationType+1)!
        switch type {
        case .Video:
            self.cellTypeImgView.image = #imageLiteral(resourceName: "NotificationVideoIcon")
            break
        case .Snap:
            self.cellTypeImgView.image = #imageLiteral(resourceName: "NotificationSnapIcon")
            break
        case .Call:
            self.cellTypeImgView.image = #imageLiteral(resourceName: "NotificationCallIcon")
            break
        case .Chat:
            self.cellTypeImgView.image = #imageLiteral(resourceName: "NotificationChatIcon")
            break
        case .Connection:
            self.cellTypeImgView.image = #imageLiteral(resourceName: "NotificationConnectIcon")
            break
        case .Fan:
            self.cellTypeImgView.image = #imageLiteral(resourceName: "NotificationFanIcon")
            break
        case .Favorite:
            self.cellTypeImgView.image = #imageLiteral(resourceName: "NotificationFavoriteIcon")
            break
        
        }
        
    }
    
    
    @IBAction func deleteBtnACtion(_ sender: Any) {
        self.delegate?.deleteBtnClkEvent(index: (sender as AnyObject).tag)
    }
    @IBAction func bookmarkBtnAction(_ sender: Any) {
        self.delegate?.bookmarkBtnClkEvent(index: (sender as AnyObject).tag)
    }
    
}
