//
//  NotificationVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 10/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension NotificationVC{
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notificationBtnAction(_ sender: Any) {
    }
    @IBAction func settingsBtnAction(_ sender: Any) {
        let notificationSettingsVC = UIStoryboard.settingsStoryBoard().instantiateViewController(withIdentifier: "NotificationSettingsVC") as! NotificationSettingsVC
        self.navigationController?.pushViewController(notificationSettingsVC, animated: true)
    }
    @IBAction func filterBtnAction(_ sender: Any) {
        self.showFilterView()
    }
    @IBAction func profileBtnAction(_ sender: Any) {
    }
}
