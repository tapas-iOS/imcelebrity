//
//  NotificationVC+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 10/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension NotificationVC {
    func configureDemoArr() {
        for i in 0..<7 {
            let obj = NotificationOBJ()
            obj.fetchData(index: i)
            self.notificationArr.append(obj)
        }
        self.tableView.reloadData()
    }
    func loadCustomView() {
        self.loadCustomFilterView()
    }
    func loadCustomFilterView() {
        filterPopupView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(filterPopupView)
        
        filterPopupView.translatesAutoresizingMaskIntoConstraints = false
        filterPopupView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        filterPopupView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        if #available(iOS 11.0, *) {
            filterPopupView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            filterPopupView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        }
        filterPopupView.leadingConstraintOutlate = filterPopupView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: self.view.frame.width)
        filterPopupView.leadingConstraintOutlate.isActive = true
        
        self.filterPopupView.currentFilterTypeIndex = 3
        self.filterPopupView.configureFilter(typeIndex: .Notifications)
        filterPopupView.delegate = self
    }
    func showFilterView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.filterPopupView.leadingConstraintOutlate.constant = 0
            self.filterPopupView.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    func hideFilterView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.filterPopupView.leadingConstraintOutlate.constant = self.view.frame.width
            self.filterPopupView.alpha = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
