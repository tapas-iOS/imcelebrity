//
//  NotificationVC+CustomDelegate.swift
//  imCelebrity
//
//  Created by Paramita  on 10/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension NotificationVC: NotificationCellDelegate {
    func bookmarkBtnClkEvent(index: Int) {
        let obj = notificationArr[index]
        if obj.isBookmark {
            obj.isBookmark = false
        }else {
            obj.isBookmark = true
        }
        let indexPath = IndexPath(item: index, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
    func deleteBtnClkEvent(index: Int) {
        self.notificationArr.remove(at: index)
        let indexPath = IndexPath(item: index, section: 0)
        self.tableView.deleteRows(at: [indexPath], with: .left)
    }
    
}
extension NotificationVC: FilterPopupDelegate {
    func filterCancelBtnAction() {
        self.hideFilterView()
    }
    
    func filterApplyBtnAction() {
        self.hideFilterView()
    }
    
    func didSelectClkEvent(index: Int) {
        let searchCountryCodeVC = UIStoryboard.otherStoryBoard().instantiateViewController(withIdentifier: "SearchCountryCodeVC") as! SearchCountryCodeVC
        searchCountryCodeVC.currentIndex = index
        searchCountryCodeVC.delegate1 = self
        searchCountryCodeVC.isShowCountryDetails = false
        self.navigationController?.pushViewController(searchCountryCodeVC, animated: true)
    }
}
extension NotificationVC : SearchCountryCodeVCListDelegate {
    func didSelect(str: String, index: Int) {
        let indexpath = IndexPath(row: index, section: 0)
        let cell = self.filterPopupView.tableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = str
        
        let attText = self.filterPopupView.textInputAttributesArr[indexpath.row]
        if var att = attText as? TextInputAttributes {
            att.textFieldText = cell?.txtInput.text ?? ""
            self.filterPopupView.textInputAttributesArr[index] = att
        }
    }
    
}
