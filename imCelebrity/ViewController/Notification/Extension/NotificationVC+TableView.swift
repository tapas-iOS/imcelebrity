//
//  NotificationVC+TableView.swift
//  imCelebrity
//
//  Created by Paramita  on 10/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension NotificationVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell
        cell?.backView.addShadow(offset: CGSize(width: -1, height: 1), color: UIColor.darkGray, opacity: 0.5, radius: 5)
        cell?.bookmarkBtnOutlate.tag = indexPath.row
        cell?.deleteBtnOutlate.tag = indexPath.row
        cell?.delegate = self
        if notificationArr.count>0 {
            let obj = notificationArr[indexPath.row]
            cell?.populatrAllData(obj: obj)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = notificationArr[indexPath.row]
        if obj.isExpand {
            obj.isExpand = false
        }else {
            obj.isExpand = true
        }
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
}
