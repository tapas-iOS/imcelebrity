//
//  NotificationVC.swift
//  imCelebrity
//
//  Created by Paramita  on 10/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var userProfileImgView: UIImageView!
    var filterPopupView = FilterPopup()
    var notificationArr = Array<NotificationOBJ>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureDemoArr()
        self.loadCustomView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
}
