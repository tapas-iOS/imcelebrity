//
//  NotificationOBJ.swift
//  imCelebrity
//
//  Created by Paramita  on 11/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

enum NotificationType: Int {
    case Video = 1
    case Snap
    case Call
    case Chat
    case Connection
    case Fan
    case Favorite
}


class NotificationOBJ: NSObject {
    var title = String()
    var shortDescription = String()
    var longDescription = String()
    var isExpand = Bool()
    var notificationDate = String()
    var isBookmark = Bool()
    var notificationType = Int()
    
    func fetchData(index: Int) {
        self.notificationType = index 
        self.isExpand = false
        self.notificationDate = "20-10-2018"
        self.isBookmark = false
        self.longDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
        
        switch index {
        case 0:
            self.title = "Anjali uploads a video just now!"
            self.shortDescription = "Please check your contents"
        case 1:
            self.title = "Anjali uploads a snap just now!"
            self.shortDescription = "Please check your contents"
        case 2:
            self.title = "Anjali is Called you Just now!"
            self.shortDescription = "Please check your contents"
        case 3:
            self.title = "Anjali wants to chat with you!"
            self.shortDescription = "Please add me in your chat list"
        case 4:
            self.title = "Anjali wants to cnnect with you!"
            self.shortDescription = "Please add me in your connect list"
        case 5:
            self.title = "Anjali is waiting to be your fan!"
            self.shortDescription = "Please add me in your fan list"
        case 6:
            self.title = "Anjali is waiting to be your favorite!"
            self.shortDescription = "Please add me in your favorite list"
        default:
            break
        }
        
    }
}
