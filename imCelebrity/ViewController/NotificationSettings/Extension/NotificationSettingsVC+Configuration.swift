//
//  NotificationSettingsVC+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 16/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

struct NotificationSettingsTextAttribute {
    var notificationName: String
    var notificationStatus: Bool
}

extension NotificationSettingsVC {
    func addingTheShadowOnView(){
        self.viewFooter.addShadow(location: VerticalLocation.top)
        self.viewNavigationHeader.addShadow(location: VerticalLocation.bottom)
    }
    func createNotificationSettingsAttribute() {
        notificationArr = [NotificationSettingsTextAttribute.init(notificationName: "Notification", notificationStatus: true), NotificationSettingsTextAttribute.init(notificationName: "Fans Notifications", notificationStatus: false)]
    }
}
