//
//  NotificationSettingsVC+TableView.swift
//  imCelebrity
//
//  Created by Paramita  on 16/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import  UIKit

extension NotificationSettingsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchCell", for: indexPath) as! SettingsSwitchCell
        cell.selectionStyle = .none
        let object = self.notificationArr[indexPath.row]
        cell.delegate = self
        cell.configureNotificationSettingsCell(indexRow: indexPath.row, obj: object)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
extension NotificationSettingsVC: SettingsSwitchCellDelegate{
    func switchToogleButtonAction(tagIndex: Int, status: Bool){
        self.notificationArr[tagIndex].notificationStatus = status
    }
}

