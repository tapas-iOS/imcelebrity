//
//  NotificationSettingsVC.swift
//  imCelebrity
//
//  Created by Paramita  on 16/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class NotificationSettingsVC: UIViewController {

    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var viewNavigationHeader: UIView!
    @IBOutlet weak var tableView: UITableView!
    var notificationArr = Array<NotificationSettingsTextAttribute>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addingTheShadowOnView()
        self.createNotificationSettingsAttribute()
    }
    
    
    

}
