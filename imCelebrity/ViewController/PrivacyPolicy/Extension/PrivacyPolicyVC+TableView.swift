//
//  PrivacyPolicyVC+TableView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import  UIKit

extension PrivacyPolicyVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.privacyArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchCell", for: indexPath) as! SettingsSwitchCell
        cell.selectionStyle = .none
        let object = self.privacyArr[indexPath.row]
        cell.delegate = self
        cell.configurePrivacyCell(indexRow: indexPath.row, obj: object)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
extension PrivacyPolicyVC: SettingsSwitchCellDelegate{
    func switchToogleButtonAction(tagIndex: Int, status: Bool){
        self.privacyArr[tagIndex].privacyStatus = status
    }
}
