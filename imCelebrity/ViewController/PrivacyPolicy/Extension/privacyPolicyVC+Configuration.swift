//
//  privacyPolicyVC+Configuration.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

struct PrivacyPolicyTextAttribute {
    var privacyName: String
    var privacyStatus: Bool
}

extension PrivacyPolicyVC {
    func addingTheShadowOnView(){
        self.viewFooter.addShadow(location: VerticalLocation.top)
        self.viewNavigationHeader.addShadow(location: VerticalLocation.bottom)
    }
    func createPrivacyAttribute() {
        privacyArr = [PrivacyPolicyTextAttribute.init(privacyName: "Profession", privacyStatus: true), PrivacyPolicyTextAttribute.init(privacyName: "Date of Birth", privacyStatus: false), PrivacyPolicyTextAttribute.init(privacyName: "Gender", privacyStatus: true), PrivacyPolicyTextAttribute.init(privacyName: "Email", privacyStatus: false), PrivacyPolicyTextAttribute.init(privacyName: "Phone No", privacyStatus: true), PrivacyPolicyTextAttribute.init(privacyName: "Address", privacyStatus: false), PrivacyPolicyTextAttribute.init(privacyName: "Skills", privacyStatus: true), PrivacyPolicyTextAttribute.init(privacyName: "Role Model", privacyStatus: false), PrivacyPolicyTextAttribute.init(privacyName: "About Me", privacyStatus: true)]
    }
}
