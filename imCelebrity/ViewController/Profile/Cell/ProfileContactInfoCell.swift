//
//  ProfileContactInfoCell.swift
//  imCelebrity
//
//  Created by Paramita  on 30/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ProfileContactInfoCell: UITableViewCell {

    @IBOutlet weak var lblLocationAddress: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblPhoneNo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func populateAllData(obj: ProfileObjectClass) {
        self.lblPhoneNo.text = obj.phoneNo
        self.lblEmailAddress.text = obj.email
        self.lblLocationAddress.text = obj.location
    }

}
