//
//  ProfileOtherInfoCell.swift
//  imCelebrity
//
//  Created by Paramita  on 30/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ProfileOtherInfoCell: UITableViewCell {

    @IBOutlet weak var lblAboutMe: UILabel!
    @IBOutlet weak var lblSkill: UILabel!
    @IBOutlet weak var lblRoleModel: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func populateAllData(obj: ProfileObjectClass) {
        self.lblDob.text = "Date of Birth:    " + obj.dob
        self.lblRoleModel.text = "Role Model: \n" + obj.roleModel
        self.lblSkill.text = "Skill: \n" + obj.skill
        self.lblAboutMe.text = "About me: \n" + obj.aboutMe
        
        self.changeSubStringTextColor(mainString: self.lblDob.text!, subString: "Date of Birth:", lbl: self.lblDob)
        self.changeSubStringTextColor(mainString: self.lblRoleModel.text!, subString: "Role Model:", lbl: self.lblRoleModel)
        self.changeSubStringTextColor(mainString: self.lblSkill.text!, subString: "Skill:", lbl: self.lblSkill)
        self.changeSubStringTextColor(mainString: self.lblAboutMe.text!, subString: "About me:", lbl: self.lblAboutMe)
    }
    
    func changeSubStringTextColor(mainString: String, subString: String, lbl: UILabel) {
        let range = (mainString as NSString).range(of: subString)
        
        let attribute = NSMutableAttributedString.init(string: mainString)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black , range: range)
        attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "OpenSans-Bold", size: 15)!, range: range)
        lbl.attributedText = attribute
    }

}
