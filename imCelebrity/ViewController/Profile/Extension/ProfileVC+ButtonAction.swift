//
//  ProfileVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ProfileVC {
    @IBAction func editProfileBtnActio(_ sender: Any) {
        let profileVC = UIStoryboard.ProfileStoryBoard().instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    @IBAction func homeBtnACtion(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notificationBtnACtion(_ sender: Any) {
        let notificationVC = UIStoryboard.NotificationsStoryBoard().instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    @IBAction func profileBtnACtion(_ sender: Any) {
    }
    @IBAction func connectBtnAction(_ sender: Any) {
        let connectsVC = UIStoryboard.connectsStoryBoard().instantiateViewController(withIdentifier: "ConnectsVC") as! ConnectsVC
        self.navigationController?.pushViewController(connectsVC, animated: true)
    }
    @IBAction func fansBtnAction(_ sender: Any) {
        let fansVC = UIStoryboard.fansStoryBoard().instantiateViewController(withIdentifier: "FansVC") as! FansVC
        self.navigationController?.pushViewController(fansVC, animated: true)
    }
    @IBAction func videoBtnAction(_ sender: Any) {
        let videoListingVC = UIStoryboard.videoStoryBoard().instantiateViewController(withIdentifier: "VideoListingVC") as! VideoListingVC
        self.navigationController?.pushViewController(videoListingVC, animated: true)
    }
    @IBAction func snapBtnAction(_ sender: Any) {
        let snapVC = UIStoryboard.SnapStoryBoard().instantiateViewController(withIdentifier: "SnapListingVC") as! SnapListingVC
        self.navigationController?.pushViewController(snapVC, animated: true)
    }
}
