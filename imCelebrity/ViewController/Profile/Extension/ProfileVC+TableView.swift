//
//  ProfileVC+TableView.swift
//  imCelebrity
//
//  Created by Paramita  on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ProfileVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileContactInfoCell", for: indexPath) as? ProfileContactInfoCell
            cell?.populateAllData(obj: obj)
            return cell!
        }else {
             let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileOtherInfoCell", for: indexPath) as? ProfileOtherInfoCell
            cell?.populateAllData(obj: obj)
            return cell!
        }
    }
    
    
}
