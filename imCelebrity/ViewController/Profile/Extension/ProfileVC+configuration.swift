//
//  ProfileVC+configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ProfileVC {
    func addingTheShadowOnView(){
        self.viewNavigationHeader.addShadow(location: VerticalLocation.bottom)
    }
    
    func configureTableView() {
        self.tableView.estimatedRowHeight = 150
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    func fetchData() {
        self.obj.phoneNo = "+91-78248364823"
        self.obj.email = "tapas@gmail.com"
        self.obj.location = "32/A lorem Ipsum Dolar sit Country, State, City, pincode, Zip"
        self.obj.dob = "20/01/1987"
        self.obj.roleModel = "role"
        self.obj.skill = "skill"
        self.obj.aboutMe = "This is a testing message. This is a testing message. This is a testing message. This is a testing message. This is a testing message. This is a testing message. This is a testing message."
        self.obj.connectsCount = "760"
        self.obj.fansCount = "1.2k"
        self.obj.videoCount = "430"
        self.obj.snapCount = "7.6k"
        
    }
    func loadData() {
        self.lblConnectsCount.text = obj.connectsCount
        self.lblFansCount.text = obj.fansCount
        self.lblVideoCount.text = obj.videoCount
        self.lblSnapCount.text = obj.snapCount
    }
    
}
