//
//  ProfileVC.swift
//  imCelebrity
//
//  Created by Paramita  on 29/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var genderImgView: UIImageView!
    @IBOutlet weak var onlineImgView: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblSupport: UILabel!
    @IBOutlet weak var userBigImgView: UIImageView!
    @IBOutlet weak var lblSnapCount: UILabel!
    @IBOutlet weak var lblVideoCount: UILabel!
    @IBOutlet weak var lblConnectsCount: UILabel!
    @IBOutlet weak var lblFansCount: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewNavigationHeader: UIView!
    @IBOutlet weak var userImgView: UIImageView!
    var obj = ProfileObjectClass()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addingTheShadowOnView()
        self.configureTableView()
        self.fetchData()
        self.loadData()
        
    }
    
    
}
