//
//  ProfileObjectClass.swift
//  imCelebrity
//
//  Created by Paramita  on 30/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ProfileObjectClass: NSObject {
    var email = String()
    var phoneNo = String()
    var location = String()
    var dob = String()
    var roleModel = String()
    var skill = String()
    var aboutMe = String()
    var connectsCount = String()
    var fansCount = String()
    var videoCount = String()
    var snapCount = String()

}
