//
//  PhoneListCell.swift
//  imCelebrity
//
//  Created by Paramita  on 22/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol PhoneListCellDelegate: class {
    func checkBtnClkEvent(index: Int)
}

class PhoneListCell: UITableViewCell {

    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var inviteBtnOutlate: UIButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var checkedBtnOutlate: UIButton!
    @IBOutlet weak var checkedImgView: UIImageView!
    weak var delegate: PhoneListCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func inviteBtnAction(_ sender: Any) {
    }
    @IBAction func checkedBtnAction(_ sender: Any) {
        self.delegate?.checkBtnClkEvent(index: (sender as AnyObject).tag)
    }
    
}
