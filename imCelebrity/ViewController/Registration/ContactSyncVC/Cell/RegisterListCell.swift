//
//  RegisterListCell.swift
//  imCelebrity
//
//  Created by Paramita  on 22/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class RegisterListCell: UITableViewCell {

    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var fansBtnOutlate: UIButton!
    @IBOutlet weak var connectBtnOutlate: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func fansBtnAction(_ sender: Any) {
    }
    @IBAction func connectBtnAction(_ sender: Any) {
    }
}
