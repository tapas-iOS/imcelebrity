//
//  ContactSync.swift
//  ImCelebrity
//
//  Created by Amstech on 8/22/17.
//  Copyright © 2017 Brainium InfoTech. All rights reserved.
//

import Foundation
import Contacts
import UIKit

class ContactsSync {
    
    static let shared = ContactsSync()
    
    //MARK: - Variables
    var contactStore = CNContactStore()
    var contacts = [CNContact]()
    
    func sync_Contacts(_ completionHandler: @escaping (_ contacts: [CNContact], _ errorString: String) -> Void){
        requestAccess(completionHandler: { (accessGranted,errorString) in
            if accessGranted{
                // let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey]
                let keys = [
                    CNContactFormatter.descriptorForRequiredKeys(for: CNContactFormatterStyle.fullName),
                    CNContactEmailAddressesKey,
                    CNContactPhoneNumbersKey,
                    CNContactImageDataKey,
                    CNContactImageDataAvailableKey,
                    CNContactThumbnailImageDataKey,
                    CNContactBirthdayKey] as [Any]
                
                // Get all the containers
                var allContainers: [CNContainer] = []
                do {
                    allContainers = try self.contactStore.containers(matching: nil)
                } catch {
                    print("Error fetching containers")
                }
                
                // Iterate all containers and append their contacts to our results array
                for container in allContainers {
                    let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                    
                    do {
                        let containerResults = try self.contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keys as! [CNKeyDescriptor])
                        self.contacts.append(contentsOf: containerResults)
                        
                    } catch {
                        print("Error fetching contacts")
                    }
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    //print(containerResults)
                    completionHandler( self.contacts, "")
                })
            }else{
                //print("Error String: \(errorString)")
                completionHandler( self.contacts, errorString)
            }
        })
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool, _ errorString: String) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true, "")
        case .denied:
            //showSettingsAlert(completionHandler)
            //print("User denied access for the user.......")
            completionHandler(false, "Please allow the app to access your contacts through the Settings.")
        case .restricted, .notDetermined:
            self.contactStore.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true, "")
                } else {
                    DispatchQueue.main.async {
                        //print("User denied access for the user")
                        completionHandler(false, "Please allow the app to access your contacts through the Settings.")
                        //self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    

    
   /*
    func requestForAccess(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
            
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let message = "\(accessError?.localizedDescription ?? "")\n\nPlease allow the app to access your contacts through the Settings."
                            print("Message: \(message)")
                            //showAlertView(title: "", msg: message, controller: getTopViewController(), okClicked: {
                                
                            //})
                        })
                    }
                }
            })
            
        default:
            completionHandler(false)
        }
    }
 */
}
