//
//  ContactSync+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 22/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
import Parchment

extension ContactSyncVC {
    func loadVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        phone = storyboard.instantiateViewController(withIdentifier: "PhoneVC") as! PhoneVC
        register = storyboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        phone.delegate = self
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [phone, register]) 
        pagingViewController.indicatorColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        pagingViewController.backgroundColor = UIColor.white //UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.selectedBackgroundColor = UIColor.white //UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.textColor = UIColor.black
        pagingViewController.selectedTextColor = UIColor.black
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        pagingViewController.selectedFont = UIFont(name:"OpenSans", size: 15.0)! //UIFont.boldSystemFont(ofSize: 15)
        
        // Make sure you add the PagingViewController as a child view
        // controller and constrain it to the edges of the view.
        addChild(pagingViewController)
        bgView.addSubview(pagingViewController.view)
        bgView.constrainToEdges(pagingViewController.view) 
        pagingViewController.didMove(toParent: self)
    }
    
    func syncDemoData() {
        for i in 0..<20 {
            print(i)
            let obj = ContactSyncOBJ()
            obj.checkStatus = false
            obj.name = "Randeep Sing"
            obj.mobilePhNo = ["8967543288"]
            self.contactDetailsArr.append(obj)
        }
        DispatchQueue.main.async {
            self.phone.contactPhoneDetailsArr = self.contactDetailsArr
            self.register.contactRegisterDetailsArr = self.contactDetailsArr
            self.phone.tableView.reloadData()
            self.register.tableView.reloadData()
            // self.sendNumbersToServer()
        }
    }

    //MARK: - Sync Mobile Numbers
    func syncMobile(){
        if !isMobileNumberSynced{
            self.mobileNumbersArr.removeAll()
            DispatchQueue.global().async {
                ContactsSync.shared.sync_Contacts { (contacts, errorString) in
                    if (contacts.count != 0){
                        for contact in contacts{
                            let obj = ContactSyncOBJ()
                            obj.checkStatus = false
                            obj.name = contact.givenName + " " + contact.familyName
                            for email in contact.emailAddresses{
                                obj.email = email.value as String
                            }
                            if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                                //print(countryCode)
                                for country in COUNTRY_ISD_ARRAY{
                                    if countryCode.lowercased() == (country["code"] as? String)?.lowercased(){
                                        obj.dialCode = country["dial_code"] as! String
                                        break
                                    }
                                }
                            }
                            for number in contact.phoneNumbers {
                                if number.value.stringValue != "" {
                                    if number.value.stringValue.hasPrefix("+") {
                                        if number.value.stringValue != ""{
                                            self.mobileNumbersArr.append(number.value.stringValue)
                                            obj.mobilePhNo.append(number.value.stringValue)
                                        }
                                    }else{
                                        if number.value.stringValue != ""{
                                            self.mobileNumbersArr.append(obj.dialCode+number.value.stringValue)
                                            obj.mobilePhNo.append(obj.dialCode+number.value.stringValue)
                                            
                                            //self.contactDetailsArr.append(obj)
                                        }
                                    }
                                }
                            }
                            
                            if obj.mobilePhNo.count > 1{
                                self.contactDetailsArr.append(obj)
                            }
                        }
                        DispatchQueue.main.async {
                            self.phone.contactPhoneDetailsArr = self.contactDetailsArr
                            self.register.contactRegisterDetailsArr = self.contactDetailsArr
                            self.phone.tableView.reloadData()
                            self.register.tableView.reloadData()
                            // self.sendNumbersToServer()
                        }
                    }else{
                        self.commonAlertMessage(message: errorString)
                    }
                }
            }
        }
    }
}
