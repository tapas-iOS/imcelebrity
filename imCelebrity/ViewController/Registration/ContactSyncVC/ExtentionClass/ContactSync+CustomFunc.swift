//
//  ContactSync+CustomFunc.swift
//  imCelebrity
//
//  Created by Paramita  on 25/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ContactSyncVC {
    func moveToDashbaordVC(){
        let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.navigationController?.pushViewController(dashboardVC, animated: true)
    }
}
