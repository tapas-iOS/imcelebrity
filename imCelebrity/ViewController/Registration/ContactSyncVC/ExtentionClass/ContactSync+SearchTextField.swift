//
//  ContactSync+SearchTextField.swift
//  imCelebrity
//
//  Created by Paramita  on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension ContactSyncVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.configureSearchView(isEditableMode: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         self.configureSearchView(isEditableMode: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if (updatedString?.count)! > 0{
            
        }else{
            
        }
        
        return true
    }
    
    
    func configureSearchView(isEditableMode: Bool){
        if isEditableMode{
            self.searchTextFiled.textColor = UIColor.black
           self.searchIconImageView.image = #imageLiteral(resourceName: "FooterSearchIcon")
        }else{
            self.searchTextFiled.textColor = UIColor.lightGray
            self.searchIconImageView.image = #imageLiteral(resourceName: "SeatchIcon")
        }
    }
}
