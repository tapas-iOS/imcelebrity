//
//  ContactSyncVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ContactSyncVC: UIViewController {

    @IBOutlet weak var searchIconImageView: UIImageView!
//    @IBOutlet weak var imageViewSearchIcon: s!
    @IBOutlet weak var searchTextFiled: UITextField!{
        didSet{
             self.searchTextFiled.delegate = self
        }
    }
    var isMobileNumberSynced : Bool = false
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewNavigationHeader: UIView!
    
    var mobileNumbersArr = Array<String>()
    var contactDetailsArr = Array<ContactSyncOBJ>()
    
    var phone = PhoneVC()
    var register = RegisterVC()
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addingTheShadowOnView()
        self.loadVC()
        self.syncMobile()
    }
    
    func addingTheShadowOnView(){
        self.viewNavigationHeader.addShadow(location: VerticalLocation.bottom, radius: 2.0)
    }
    
    @IBAction func notificationBtnAction(_ sender: Any) {
    }
    
    @IBAction func crossBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
