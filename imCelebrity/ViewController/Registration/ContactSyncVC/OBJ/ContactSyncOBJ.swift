//
//  ContactSyncOBJ.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 23/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ContactSyncOBJ: NSObject {
    var name = String()
    var email = String()
    var mobilePhNo = Array<String>()
    var dialCode = String()
    var checkStatus = Bool()
}

