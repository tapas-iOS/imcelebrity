//
//  PhoneVC.swift
//  DemoPageViewController
//
//  Created by Paramita  on 21/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol PhoneVCDelegate: class {
    func footerAddButtonClkEvent()
}


class PhoneVC: UIViewController {
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var footerViewHeightConstraintOutlate: NSLayoutConstraint!
    
    var contactPhoneDetailsArr = Array<ContactSyncOBJ>()
    weak var delegate: PhoneVCDelegate?
    @IBOutlet weak var tableView: UITableView!
    var checkCountArr = Array<String>()
    var checkCount = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkCount = 0
        self.addShadowFooterView()
    }
   

}

extension PhoneVC {
    func addShadowFooterView() {
        self.viewFooter.addShadow(location: VerticalLocation.top)
        self.hideFooterView()
    }
    func showFooterView() {
        self.viewFooter.isHidden = false
        self.footerViewHeightConstraintOutlate.constant = 54
    }
    func hideFooterView() {
        self.viewFooter.isHidden = true
        self.footerViewHeightConstraintOutlate.constant = 0
    }
}

extension PhoneVC {
    @IBAction func addBtnAction(_ sender: Any) {
        self.delegate?.footerAddButtonClkEvent()
    }
}
extension PhoneVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactPhoneDetailsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneListCell", for: indexPath) as? PhoneListCell
        if contactPhoneDetailsArr.count>0 {
            let obj = self.contactPhoneDetailsArr[indexPath.row]
            cell?.userName.text = obj.name //"\n\(obj.mobilePhNo[0])"
            if ( obj.mobilePhNo.count > 1){
                cell?.labelPhoneNumber.text = obj.mobilePhNo[0]
            }else{
                cell?.labelPhoneNumber.text = ""
            }
            cell?.checkedBtnOutlate.tag = indexPath.row
            cell?.delegate = self
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension PhoneVC: PhoneListCellDelegate {
    func checkBtnClkEvent(index: Int) {
        let indexpath = IndexPath(row: index, section: 0)
        let cell = tableView.cellForRow(at: indexpath) as? PhoneListCell
        
        let obj = self.contactPhoneDetailsArr[indexpath.row]
        if obj.checkStatus {
            obj.checkStatus = false
            cell?.checkedImgView.image = #imageLiteral(resourceName: "CheckBOxDelect")
//            self.checkCountArr = self.checkCountArr.filter {  $0 != obj.mobilePhNo[0]}
            checkCount -= 1
        }else {
            obj.checkStatus = true
            cell?.checkedImgView.image = #imageLiteral(resourceName: "CheckBoxSelected")
//            self.checkCountArr.append(obj.mobilePhNo[0])
            checkCount += 1
        }
        if self.checkCount>1 {
            self.showFooterView()
        }else {
            self.hideFooterView()
        }
    }
    
}
