//
//  RegisterVC.swift
//  DemoPageViewController
//
//  Created by Paramita  on 21/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
    
    var contactRegisterDetailsArr = Array<ContactSyncOBJ>()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
extension RegisterVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactRegisterDetailsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RegisterListCell", for: indexPath) as? RegisterListCell
        if contactRegisterDetailsArr.count>0 {
            let obj = contactRegisterDetailsArr[indexPath.row]
            cell?.userNameLbl.text = obj.name //"\(obj.name)\n\(obj.mobilePhNo[0])"
            cell?.labelPhoneNumber.text = obj.mobilePhNo[0]
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
