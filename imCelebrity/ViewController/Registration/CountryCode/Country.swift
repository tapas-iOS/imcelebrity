//
//  Country.swift
//  DrQuickfix
//
//  Created by Krishnendu Biswas on 19/03/19.
//  Copyright © 2019 Krishnendu Biswas. All rights reserved.
//

import UIKit

struct Country {
    let name: String?
    let code: String?
    let phoneCode: String?
    let flag: UIImage?
    
    init(name: String, code: String, phoneCode: String, flag: UIImage?) {
        self.name = name
        self.code = code
        self.phoneCode = phoneCode
        self.flag = flag
    }
    
    init(from structure: Response) {
        self.name = structure.name
        self.code = structure.code
        self.phoneCode = structure.dial_code
        self.flag = UIImage(named: structure.code)
    }
    
    struct Response: Codable {
        let name: String
        let dial_code: String
        let code: String
    }
}
