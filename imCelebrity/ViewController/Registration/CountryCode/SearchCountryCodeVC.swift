//
//  SearchCountryCodeVC.swift
//  DrQuickfix
//
//  Created by Krishnendu Biswas on 19/03/19.
//  Copyright © 2019 Krishnendu Biswas. All rights reserved.
//

import UIKit

protocol SearchCountryCodeVCDelegate {
    func didSelect(country: Country)
}
protocol SearchCountryCodeVCListDelegate {
    func didSelect(str: String, index: Int)
}

class SearchCountryCodeVC: UIViewController, UISearchResultsUpdating, UISearchControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var searchController: UISearchController?
    var list: [Country]?
    var results: [Country]?
    var delegate: SearchCountryCodeVCDelegate?
    var delegate1: SearchCountryCodeVCListDelegate?
    var isShowCountryDetails = Bool()
    var listingArr = Array<String>()
    var searchListingArr = Array<String>()
    var currentIndex = Int()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSearchBarController()
        if isShowCountryDetails {
            self.list = CommonClass.getCountries()
        }else {
            for i in 0..<10 {
                listingArr.append("Tapas\(i)")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        let imgBackArrow = UIImage(named: "back_arrow_32")
//
//        navigationController?.navigationBar.backIndicatorImage = imgBackArrow
//        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBackArrow
//
//        navigationItem.leftItemsSupplementBackButton = true
//        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        
        
        self.navigationController?.isNavigationBarHidden = false
        let btnCancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dismissController))
        btnCancel.tintColor = UIColor.red
        navigationItem.rightBarButtonItems = [btnCancel]
        
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc private func dismissController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func initSearchBarController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchResultsUpdater = self
        searchController?.delegate = self
        
        searchController?.obscuresBackgroundDuringPresentation = false
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            searchController?.dimsBackgroundDuringPresentation = false
            searchController?.hidesNavigationBarDuringPresentation = true
            searchController?.definesPresentationContext = true
            
            tableView.tableHeaderView = searchController?.searchBar
        }
        definesPresentationContext = true
    }
    
    private func getItem(at indexPath: IndexPath) -> Country {
        var array: [Country]!
        
        if let searchController = searchController, searchController.isActive && results != nil && results!.count > 0 {
            array = results
        } else {
            array = list
        }
        return array[indexPath.row]
    }
    
    private func getListItem(at indexPath: IndexPath) -> String {
        var arr = Array<String>()
        
        if let searchController = searchController, searchController.isActive  && searchListingArr.count > 0 {
            arr = searchListingArr
        } else {
            arr = listingArr
        }
        return arr[indexPath.row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isShowCountryDetails {
            if let searchController = searchController, searchController.isActive {
                if let count = searchController.searchBar.text?.count, count > 0 {
                    return results?.count ?? 0
                }
            }
            return list?.count ?? 0
        }else {
            if let searchController = searchController, searchController.isActive {
                if let count = searchController.searchBar.text?.count, count > 0 {
                    return searchListingArr.count
                }
            }
            return listingArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        if isShowCountryDetails {
            let country = getItem(at: indexPath)
            
            cell.textLabel?.text = country.name
            cell.detailTextLabel?.text = country.phoneCode
            cell.imageView?.image = country.flag
        }else {
            let str = getListItem(at: indexPath)
            cell.textLabel?.text = str
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
        if isShowCountryDetails {
            delegate?.didSelect(country: getItem(at: indexPath))
        }else {
            delegate1?.didSelect(str: getListItem(at: indexPath), index: currentIndex)
        }
        searchController?.isActive = false
        searchController?.searchBar.resignFirstResponder()
        dismissController()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if isShowCountryDetails {
            if list == nil {
                results?.removeAll()
                return
            } else if searchController.searchBar.text == "" {
                results?.removeAll()
                tableView.reloadData()
                return
            }
        }else {
            if listingArr.count == 0 {
                searchListingArr.removeAll()
                return
            } else if searchController.searchBar.text == "" {
                searchListingArr.removeAll()
                tableView.reloadData()
                return
            }
        }
       
        
        if let searchText = searchController.searchBar.text, searchText.count > 0 {
            if isShowCountryDetails {
                results = list!.filter({(item: Country) -> Bool in
                    if item.name?.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    } else if item.code?.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    } else if item.phoneCode?.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                    return false
                })
            }else {
                searchListingArr = listingArr.filter({(item: String) -> Bool in
                    if item.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                    return false
                })
            }
        }
        tableView.reloadData()
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        DispatchQueue.main.async { [unowned self] in
            self.searchController?.searchBar.becomeFirstResponder()
        }
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        if isShowCountryDetails {
            results?.removeAll()
        }else {
            results?.removeAll()
        }
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        dismissController()
    }
}
