//
//  OTPVC+APICall.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 14/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
extension OTPVC{
    
    func callVerifyOTPAPICall(){
        print("Phone Number: \(self.textInputAttributesArr[0].textFieldText) ,, OTP:\(self.textInputAttributesArr[1].textFieldText)")
        self.callVerifyOTPWebserviceMethod(_userOTP: self.textInputAttributesArr[1].textFieldText )
    }
    
    func callVerifyOTPWebserviceMethod( _userOTP: String ) {
        if CommonClass.isConnectedToNetwork() == true {
            CommonClass.addLoading(view: self.view)
            ApiCalling.userOTPCheck(strOTP:_userOTP)
            { (result : Any?, error :Error? ) in
                CommonClass.removeLoading(view: self.view)
                print("Result:------- \(result ?? "")")
                if let _response = result as? NSDictionary {
                    if let response_code = _response["response_code"] as? Int {
                        if( response_code == 2000 ){
                            if let response_data = _response["response_data"] as? NSDictionary {
                                self.moveToRegistrationSecondVC()
                            }
                        }else{
                            if let response_message = _response["response_message"] as? String{
                                self.showAutoCancelAlertWithMessage(message: response_message)
                            }
                        }
                    }else{
                        self.showAutoCancelAlertWithMessage(message: "Server Error. Retry after sometime.")
                    }
                }
            }
        }else {
            self.showAutoCancelAlertWithMessage(message: Constant.NoInternentMessgae)
        }
    }
    
    func callResendOTPAPICall(){
        print("Phone Number: \(self.textInputAttributesArr[0].textFieldText) ,, OTP:\(self.textInputAttributesArr[1].textFieldText)")
        self.callResendOTPMethod(_userPhoneNumber: self.textInputAttributesArr[1].textFieldText, _userCountryCode: "")
    }
    
    
    func callResendOTPMethod( _userPhoneNumber: String, _userCountryCode:String ) {
        if CommonClass.isConnectedToNetwork() == true {
            CommonClass.addLoading(view: self.view)
            ApiCalling.resendOTP(phoneNumber: _userPhoneNumber, countryCode: _userCountryCode) { (result : Any?, error :Error? ) in
                CommonClass.removeLoading(view: self.view)
                print("Result:------- \(result ?? "")")
                if let _response = result as? NSDictionary {
                    if let response_code = _response["response_code"] as? Int {
                        if( response_code == 2000 ){
                            if let response_data = _response["response_data"] as? NSDictionary {
                                //self.fetchResponseData(Dic: response_data)
                                self.changeButtonTextToVerifyOTP()
                            }
                        }else{
                            if let response_message = _response["response_message"] as? String{
                                self.showAutoCancelAlertWithMessage(message: response_message)
                            }
                        }
                    }else{
                        self.showAutoCancelAlertWithMessage(message: "Server Error. Retry after sometime.")
                    }
                }
            }
        }else {
            self.showAutoCancelAlertWithMessage(message: Constant.NoInternentMessgae)
        }
    }
}
