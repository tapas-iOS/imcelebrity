//
//  OTPVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension OTPVC{
    @IBAction func verifyOTPButtonAction(_ sender: UIButton) {
        print("\(sender.currentTitle ?? "") button clicked")
        if(sender.currentTitle == Enums.OTPVerifiction.ButtonTitle.ResendOTP.rawValue){
            
            //self.moveToRegistrationSecondVC()
            
        }else if(sender.currentTitle == Enums.OTPVerifiction.ButtonTitle.Verify.rawValue){
            self.invalidateOTPTimmer()
            self.validatePhoneOTPDetails()
            // self.moveToRegistrationSecondVC()
            
        }else{
            self.commonAlertMessage(message: "Please select the any button action")
        }
    }
    
    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.popToPerviousVC()
    }
}

extension OTPVC : CellTextFieldModelDelegate {
    func rightButtonClkAction(index: Int) {

    }
    
    func leftButtonClkAction() {
        let searchCountryCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchCountryCodeVC") as! SearchCountryCodeVC
        searchCountryCodeVC.delegate = self
        searchCountryCodeVC.isShowCountryDetails = true
        self.navigationController?.pushViewController(searchCountryCodeVC, animated: true)
    }
    
}
extension OTPVC : SearchCountryCodeVCDelegate {
    func didSelect(country: Country) {
        guard let phoneCode = country.phoneCode else {return}
        let indexpath = IndexPath(row: 1, section: 0)
        let cell = OTPTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.leftBtn.setTitle(phoneCode, for: .normal)
        
        self.counrtyPhoneCode = phoneCode
        self.countryCode = country.code ?? ""
    }
}
