//
//  OTPVC+Configuration.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension OTPVC {
    func registerNib() {
        self.OTPTableView.separatorStyle = .none
            self.OTPTableView.register(UINib(nibName: "CellLogoCell", bundle: nil), forCellReuseIdentifier: logoCellId)
        self.OTPTableView.register(UINib(nibName: "CellTextFieldModel", bundle: nil), forCellReuseIdentifier: textCellId)
    }
    
    func configureTextInput() {
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Mobile Number", textFieldText: self.phoneNumberForOTP, keyboardType: .numberPad, isSecureTextEntry: false, errorMessage: "Mobile number should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: true, isShowDropdownArrow: false), TextInputAttributes.init(placeholderText: "Verification Code", textFieldText: "", keyboardType: .numberPad, isSecureTextEntry: false, errorMessage: "Verification code should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false)]
    }
    
    func configureButttonView(){
        
    }
    
    func setTextFieldAccessoryViews(){
        accessaryView.frame = CGRect(x:0, y:0, width: CommonClass.ScreenSize.SCREEN_WIDTH, height: 44)
        accessaryView.backgroundColor = .red
        
        let doneButton : UIButton = UIButton()
        doneButton.frame = CGRect(x: CommonClass.ScreenSize.SCREEN_WIDTH - 60, y:0, width:60, height:44)
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonClicked(sender:)), for: .touchUpInside)
        doneButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(doneButton)
        
        let cancelButton : UIButton = UIButton()
        cancelButton.frame = CGRect(x:10, y:0, width:60, height:44)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked(sender:)), for: .touchUpInside)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(cancelButton)
    }
    
    func configurationVerifyOTPButton(){
        self.verifyOTPButtonOutlet.setTitle(Enums.OTPVerifiction.ButtonTitle.Verify.rawValue, for: UIControl.State.normal)
    }
    
    func addingTheShadowOnView(){
        //self.viewFooter.addShadow(radious: 100  , color: UIColor.gray)
        //self.dropShadow()
        self.viewFooter.addShadow(location: VerticalLocation.top)
    }
    
    func dropShadow(scale: Bool = true) {
        self.viewFooter.layer.masksToBounds = false
        self.viewFooter.layer.shadowColor = UIColor.black.cgColor
        self.viewFooter.layer.shadowOpacity = 1.0
        self.viewFooter.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.viewFooter.layer.shadowRadius = 10

        self.viewFooter.layer.shadowPath = UIBezierPath(rect: self.viewFooter.bounds).cgPath
        self.viewFooter.layer.shouldRasterize = true
        self.viewFooter.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func startOTPValidationTimmer(){
        self.resendOTPTimmer = Timer.scheduledTimer(timeInterval: 2.00, target: self, selector: #selector(self.updateOTPTime) , userInfo: nil, repeats: true)
    }
    
    func invalidateOTPTimmer(){
        if(self.resendOTPTimmer != nil){
            self.resendOTPTimmer.invalidate()
            self.resendOTPTimmer = nil
        }
    }
    
    @objc func updateOTPTime() {
        if(self.count > 1) {
            self.count = self.count - 1
            let strButtonTitle = "Get OTP on " + String(self.count)
            print(strButtonTitle)
        }else{
            //self.buttonOutletResendOTP.isUserInteractionEnabled = true
            //self.buttonOutletResendOTP.setTitle("Resend OTP", for: UIControlState.normal)
            self.changeButtonTextToResendOTP()
            self.invalidateOTPTimmer()
        }
    }
    
    func changeButtonTextToResendOTP(){
        self.verifyOTPButtonOutlet.setTitle(Enums.OTPVerifiction.ButtonTitle.ResendOTP.rawValue, for: UIControl.State.normal)
    }
    
    func changeButtonTextToVerifyOTP(){
            self.verifyOTPButtonOutlet.setTitle(Enums.OTPVerifiction.ButtonTitle.Verify.rawValue, for: UIControl.State.normal)
    }
    
    
    func getCountryPhonceCode(_ countryCode : String) -> String?{
        let countries = CommonClass.getCountries()
        guard let index = countries.firstIndex(where: {$0.code == countryCode}) else {return nil}
        guard let phoneCode = countries[index].phoneCode else {return nil}
        return phoneCode
    }
    func getCountryCode(_ countryCode : String) -> String?{
        let countries = CommonClass.getCountries()
        guard let index = countries.firstIndex(where: {$0.code == countryCode}) else {return nil}
        guard let code = countries[index].code else {return nil}
        return code
    }
}
