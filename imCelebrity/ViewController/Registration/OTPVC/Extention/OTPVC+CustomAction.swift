//
//  OTPVC+CustomAction.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import  UIKit

extension OTPVC{
    func validatePhoneOTPDetails(){
        if(!(self.textInputAttributesArr[0].textFieldText.isEmpty)){
                if(!(self.textInputAttributesArr[1].textFieldText.isEmpty)){
                    if(!((self.textInputAttributesArr[1].textFieldText.count) < 4)){
                        print("Phonel:: \(self.textInputAttributesArr[0].textFieldText)  |||| OTP:: \(self.textInputAttributesArr[1].textFieldText.isEmpty)")
                        self.callVerifyOTPAPICall()
                    }else{
                        self.commonAlertMessage(message: "Please Enter a valid OTP")
                    }
                }else{
                    self.commonAlertMessage(message: "Please Enter a OTP")
                }
        }else{
            self.commonAlertMessage(message: "Please Enter a phone number ")
        }
    }
    
    func toogleTableViewPostion(changeConstantHeight: CGFloat , isViewUp: Bool) {
        self.view.layoutIfNeeded()
        if isViewUp {
            isTableViewUp = true
            self.buttomConstantviewTableBackground.constant = self.keybaordHeight
        }else{
            isTableViewUp = false
            self.buttomConstantviewTableBackground.constant = 0
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            print("KEYBAORD HEIGHT:  \(keyboardHeight)")
            self.keybaordHeight = keyboardHeight
            self.buttomConstantviewTableBackground.constant = keyboardHeight + 5
        }
    }
    
    @objc func doneButtonClicked(sender:UIButton){
        if(self.accessaryView.tag == 0){
            let indexpath = IndexPath(row: 2, section: 0)
            let cell = self.OTPTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.becomeFirstResponder()
        }else if(self.accessaryView.tag == 1){
            let indexpath = IndexPath(row: 2, section: 0)
            let cell = self.OTPTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.resignFirstResponder()
        }else{
            self.resignFirstResponder()
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelButtonClicked(sender:UIButton){
        self.view.endEditing(true)
    }

}
