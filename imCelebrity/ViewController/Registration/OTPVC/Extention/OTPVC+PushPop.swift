//
//  OTPVC+PushPop.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
extension OTPVC {
    func moveToRegistrationSecondVC(){
        let registrationSecondVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationSecondVC") as! RegistrationSecondVC
        self.navigationController?.pushViewController(registrationSecondVC, animated: true)
        
    }
    
    func moveToRegistrationProfilePicUploadVC(){
        let registrationSecondVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoUploadedVC") as! ProfilePhotoUploadedVC
        self.navigationController?.pushViewController(registrationSecondVC, animated: true)
    }
    
    func popToPerviousVC(){
         self.navigationController?.popViewController(animated: true)
    }
    
}
