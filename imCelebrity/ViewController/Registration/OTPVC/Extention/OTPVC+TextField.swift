//
//  OTPVC+TextField.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import  UIKit

extension OTPVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.accessaryView.tag = textField.tag
        textField.inputAccessoryView = self.accessaryView
        self.invalidateOTPTimmer()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag >= 0 && textField.tag <= 0 {
            let indexpath = IndexPath(row: 0, section: 0)
            let cell = OTPTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.becomeFirstResponder()
            return false
        }else {
            self.view.endEditing(true)
            return true
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let txtAfterUpdate = textField.text! as NSString
        let updateText = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        
        print("Updated TextField:: \(updateText)")
        textInputAttributesArr[textField.tag].textFieldText = updateText as String
        
        if(textField.tag == 0){
            return updateText.length <= 10
        }else if(textField.tag == 1){
            return updateText.length <= 4
        }
        
        return true
    }
}
