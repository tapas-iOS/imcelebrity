//
//  OTPVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class OTPVC: UIViewController {
    @IBOutlet weak var verifyOTPButtonOutlet: UIButton!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBOutlet weak var OTPTableView: UITableView!
    @IBOutlet var buttomConstantviewTableBackground: NSLayoutConstraint!
    @IBOutlet weak var viewFooter: UIView!
    
    @IBOutlet weak var viewFooterBottomConstraint: NSLayoutConstraint!
    
    let textCellId = "CellTextFieldModel"
    let logoCellId = "CellLogoCell"
    let buttonCellId = "CellButtonModel"
    var textInputAttributesArr = Array<TextInputAttributes>()
    var isTableViewUp               = Bool()
    var keybaordHeight              = CGFloat()
    
    var phoneNumberForOTP = String()
    var duplicatePhoneNumberForOTP = String()
    var accessaryView = UIView()
    
    var count = 25
    weak var resendOTPTimmer : Timer!
    lazy var counrtyPhoneCode = ""
    lazy var countryCode = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.phoneNumberForOTP = "8016266975"
        self.registerNib()
        self.configureTextInput()
        self.addingTheShadowOnView()
        self.setTextFieldAccessoryViews()
        self.configurationVerifyOTPButton()
        self.startOTPValidationTimmer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        self.startOTPValidationTimmer()
        //self.setTheRegisterPhoneNumber()
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.invalidateOTPTimmer()
    }
    
//    func setTheRegisterPhoneNumber(){
//        let indexpath = IndexPath(row: 1, section: 0)
//        let cell = self.OTPTableView.cellForRow(at: indexpath) as? CellTextFieldModel
//        cell?.txtInput.text = self.phoneNumberForOTP
//        self.duplicatePhoneNumberForOTP = self.phoneNumberForOTP
//        self.OTPTableView.reloadData()
//    }
}
