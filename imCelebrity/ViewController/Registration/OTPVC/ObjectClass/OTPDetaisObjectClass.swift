
import UIKit

class UserDetailsObject: NSObject {
    
    
    var scaleUserId                 = String()
    var user_name                   = String()
    var user_Image                  = String()
    var user_email                  = String()
    var user_password               = String()
    var user_permission             = String()
    var user_LastLogin              = String()
    var userScaleUserId           = String()
    
    var authToken                      = String()
    var userId                      = String()
    var mobile                      = String()
    
    
    public func getUserDetails(userInfoDict: Dictionary<String, Any>?) {
        fetchUserInfo(userInfoDict: userInfoDict)
    }
    /*
    if let authtoken = response_data["authtoken"] as? String {
        USERDEFAULTS.set(authtoken, forKey: "authtoken")
    }
    if let profile_details = response_data["profile_details"] as? NSDictionary {
        if let quickblox_status = profile_details["quickblox_status"] as? String{
            if let user_id = profile_details["user_id"] as? String {
                self.userId = user_id
            }
            if let mobile = profile_details["mobile"] as? String {
                self.mobile = mobile
            }
            if quickblox_status == "false"{
                // DO QUICKBLOX Registration Then Verify
                //self.performSegue(withIdentifier: "registration1ToOtpVerificationSegue", sender: self)
            }else{
                if let verification_status = profile_details["verification_status"] as? String{
                    if verification_status == "false"{
                        //self.performSegue(withIdentifier: "registration1ToOtpVerificationSegue", sender: self)
                    }else{
                        if let user_id = profile_details["user_id"] as? String {
                            USERDEFAULTS.set(user_id, forKey: "user_id")
                        }
                        if let chatToken = profile_details["chatToken"] as? String {
                            USERDEFAULTS.set(chatToken, forKey: "chatToken")
                            SocketIOManager.sharedInstance.establishConnection()
                        }
                        if let name = profile_details["name"] as? String {
                            USERDEFAULTS.set(name, forKey: "name")
                        }
                        if let email = profile_details["email"] as? String {
                            USERDEFAULTS.set(email, forKey: "email")
                        }
                        if let profile_pic = profile_details["profile_pic"] as? String {
                            USERDEFAULTS.set(profile_pic, forKey: "profile_pic")
                        }
                        if let mobile = profile_details["mobile"] as? String {
                            USERDEFAULTS.set(mobile, forKey: "mobile")
                        }
                        if let dob = profile_details["dob"] as? Int {
                            USERDEFAULTS.set(dob, forKey: "dob")
                        }
                        if let sex = profile_details["sex"] as? String {
                            USERDEFAULTS.set(sex, forKey: "sex")
                        }
                        if let city = profile_details["city"] as? String {
                            USERDEFAULTS.set(city, forKey: "city")
                        }
                        if let country = profile_details["country"] as? String {
                            USERDEFAULTS.set(country, forKey: "country")
                        }
                        if let response_message = dictionary["response_message"] as? String{
                            showAlertView(title: "Success", msg: response_message, controller: self){
                                self.performSegue(withIdentifier: "otpVerificationToMainSegue", sender: self)
                            }
                        }
                    }
                }
            }
        }
    }
    */
    public func fetchUserInfo(userInfoDict: Dictionary<String, Any>?) {
        
        if let id = ((userInfoDict!["user_id"] ?? "") as? String){
            self.userId  = id
        }
        
        if let id = ((userInfoDict!["user_id"] ?? "") as? Int){
            self.userId  = String(id)
        }
        
        if let scaleId = ((userInfoDict!["scaleUserId"] ?? "") as? Int){
            self.userScaleUserId = String(scaleId)
        }
        if let scaleId = ((userInfoDict!["scaleUserId"] ?? "") as? String){
            self.userScaleUserId = scaleId
        }
        
        if let latestValue          = userInfoDict!["user_mac"] as? String {
            self.scaleUserId = latestValue
        }
        
        if let latestValue          = userInfoDict!["user_mac"] as? Int {
            self.scaleUserId = String(latestValue)
        }
        
        if let password          = userInfoDict!["user_password"] as? String {
            self.user_password  = password
        }
        
        if let password          = userInfoDict!["user_password"] as? Int {
            self.user_password  = String(password)
        }
        
        if let completeStatus          = userInfoDict!["user_Profile_Complete_Status"] as? Int {
            //self.user_Profile_Complete_Status  =  completeStatus
        }
        
        self.user_name              = ((userInfoDict!["user_name"] ?? "") as? String)!
        self.user_email             = ((userInfoDict!["user_email"] ?? "") as? String)!
        self.user_Image             = ((userInfoDict!["user_photo"] ?? "") as? String)!
        self.user_permission        = ((userInfoDict!["user_permission"] ?? "") as? String)!
        self.user_LastLogin         = ((userInfoDict!["user_LastLogin"] ?? "") as? String)!
        
        self.storeValueUsedefault()
    }
    
    func storeValueUsedefault() {

    }
    
}
