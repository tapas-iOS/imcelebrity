//
//  ProfilePhotoUploadedVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension ProfilePhotoUploadedVC {
    @IBAction func uploadPhotoBtnAction(_ sender: Any) {
        self.openActionSheet()
    }
    
    @IBAction func crossButtonAction(_ sender: UIButton) {
         self.popToPreviousVC()
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        if(self.userSelectedImage != nil){
            self.moveToContactSyncVC()
        }else{
            self.showAutoCancelAlertWithMessage(message: "Please select any image to procced..")
        }
    }
    
    @IBAction func skipButtonAction(_ sender: UIButton) {
       self.moveToContactSyncVC()
    }
}
