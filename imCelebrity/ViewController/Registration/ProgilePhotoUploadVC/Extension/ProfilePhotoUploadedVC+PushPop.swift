//
//  RegistrationVC+PushPop.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
extension ProfilePhotoUploadedVC {
    
    func moveToOTPVC(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        nextViewController.phoneNumberForOTP = "8016266975"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func moveToRegistrationSecondVC(){
        let registrationSecondVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationSecondVC") as! RegistrationSecondVC
        self.navigationController?.pushViewController(registrationSecondVC, animated: true)
        
    }
    
    
    
    func moveToContactSyncVC(){
        let contactsSyncVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactSyncVC") as! ContactSyncVC
        self.navigationController?.pushViewController(contactsSyncVC, animated: true)
    }
    
    func popToPreviousVC(){
        self.navigationController?.popViewController(animated: true)
    }
}
