//
//  ProfilePhotoUploadedVC.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class ProfilePhotoUploadedVC: UIViewController {

    @IBOutlet weak var buttonOutletCross: UIButton!

    @IBOutlet weak var imageViewUploadImageBackground: UIImageView!
    @IBOutlet weak var buttonOutletSave: UIButton!
    @IBOutlet weak var buttonOutletSkip: UIButton!
    @IBOutlet weak var uploadedImgView: UIImageView!
    let imagePicker = UIImagePickerController()
    var userSelectedImage: UIImage? = nil
    @IBOutlet weak var viewFooter: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addingTheShadowOnView()
        self.uploadedImgView.addCornerRadious(radious: self.uploadedImgView.frame.width/2)
//        self.uploadedImgView.addBorder(borderWidth: 2, color: UIColor.red)
    }
    
    func addingTheShadowOnView(){
        self.viewFooter.addShadow(location: VerticalLocation.top)
    }

   
    
}
