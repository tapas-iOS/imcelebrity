//
//  AboutMeCell.swift
//  imCelebrity
//
//  Created by Paramita  on 14/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class AboutMeCell: UITableViewCell {

    @IBOutlet weak var selectLineHeightConstraintOutlate: NSLayoutConstraint!
    @IBOutlet weak var selectedLine: UILabel!
    @IBOutlet weak var aboutMeTxtView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
