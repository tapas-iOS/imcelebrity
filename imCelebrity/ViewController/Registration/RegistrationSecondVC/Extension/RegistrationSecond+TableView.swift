//
//  RegistrationSecond+TableView.swift
//  imCelebrity
//
//  Created by Paramita  on 14/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension RegistrationSecondVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textInputAttributesArr.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: logoCellId, for: indexPath) as! CellLogoCell
            return cell
        }else  if(indexPath.row == textInputAttributesArr.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutMeCell", for: indexPath) as! AboutMeCell
            cell.aboutMeTxtView.delegate = self
            cell.aboutMeTxtView.tag = indexPath.row
            //cell.aboutMeTxtView.inputView = self.accessaryView
            self.accessaryView.tag = indexPath.row
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath) as! CellTextFieldModel
            if(indexPath.row == 1 || indexPath.row == 4) {
                cell.txtInput.isUserInteractionEnabled = false
            }else {
                cell.txtInput.isUserInteractionEnabled = true
            }
            let attText = textInputAttributesArr[indexPath.row-1]
            cell.populateAllData(attributes: attText)
            cell.txtInput.tag = indexPath.row-1
            self.accessaryView.tag = indexPath.row-1
            cell.txtInput.delegate = self
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return (self.view.frame.height/2) - (self.view.frame.height/4)
        }else  if(indexPath.row == textInputAttributesArr.count) {
            return 210
        }else {
            return 60
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 || indexPath.row == 4 {
            let searchCountryCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchCountryCodeVC") as! SearchCountryCodeVC
            searchCountryCodeVC.currentIndex = indexPath.row
            searchCountryCodeVC.delegate1 = self
            searchCountryCodeVC.isShowCountryDetails = false
            self.navigationController?.pushViewController(searchCountryCodeVC, animated: true)
        }
    }
}
