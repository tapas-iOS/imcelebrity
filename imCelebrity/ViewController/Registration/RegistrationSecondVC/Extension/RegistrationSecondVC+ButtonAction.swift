//
//  RegistrationSecondVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 14/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit


extension RegistrationSecondVC : CellTextFieldModelDelegate {
    
    @IBAction func buttonActionSave(_ sender: UIButton) {
        
    }
    
    @IBAction func buttonActionSkip(_ sender: UIButton) {
        self.moveToRegistrationProfilePicUploadVC()
    }
    
    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.popToPreviousVC()
    }
    
    func leftButtonClkAction() {
        //Not necessary
        //self.moveToRegistrationProfilePicUploadVC()
    }
    
    
    func rightButtonClkAction(index: Int) {
        //self.moveToRegistrationProfilePicUploadVC()
    }
    
    
    @objc func doneButtonClicked(sender:UIButton){
        if(isTableViewUp == true){
            toogleTableViewPostion(changeConstantHeight: 0, isViewUp: false)
        }
        self.resignFirstResponder()
        self.view.endEditing(true)
        if(self.accessaryView.tag == 2){
            let indexpath = IndexPath(row: 2, section: 0)
            let cell = registrationSecondTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.text = "\(sexArray[sexPicker.selectedRow(inComponent: 0)])"
            self.textInputAttributesArr[1].textFieldText = cell?.txtInput.text ?? ""
            
            let indexpath1 = IndexPath(row: 3, section: 0)
            let cell1 = registrationSecondTableView.cellForRow(at: indexpath1) as? CellTextFieldModel
            cell1?.txtInput.becomeFirstResponder()
        }else if(self.accessaryView.tag == (self.textInputAttributesArr.count - 1)){
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelButtonClicked(sender:UIButton){
        self.view.endEditing(true)
    }
}
extension RegistrationSecondVC : SearchCountryCodeVCListDelegate {
    func didSelect(str: String, index: Int) {
        let indexpath = IndexPath(row: index, section: 0)
        let cell = registrationSecondTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = str
        self.textInputAttributesArr[index-1].textFieldText = cell?.txtInput.text ?? ""
    }

}
