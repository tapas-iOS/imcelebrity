//
//  RegistrationSecondVC+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension RegistrationSecondVC {
    func registerNib() {
        self.registrationSecondTableView.separatorStyle = .none
        self.registrationSecondTableView.register(UINib(nibName: logoCellId, bundle: nil), forCellReuseIdentifier: logoCellId)
        self.registrationSecondTableView.register(UINib(nibName: textCellId, bundle: nil), forCellReuseIdentifier: textCellId)
    }
    
    func addingTheShadowOnView(){
        self.viewFooter.addShadow(location: VerticalLocation.top)
    }
    
    func configureTextInput() {
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Profession", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Profession should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "Gender", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Gender should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "Role Models", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Role Models should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false), TextInputAttributes.init(placeholderText: "Skills", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Skills should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: true), TextInputAttributes.init(placeholderText: "About Me", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "About Me should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false)]
    }
    
    func setTextFieldAccessoryViews(){
        
        accessaryView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:44)
        accessaryView.backgroundColor = .red
        
        let doneButton : UIButton = UIButton()
        doneButton.frame = CGRect(x:SCREENRECT.width-60, y:0, width:60, height:44)
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonClicked), for: .touchUpInside) 
        doneButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(doneButton)
        
        let cancelButton : UIButton = UIButton()
        cancelButton.frame = CGRect(x:10, y:0, width:60, height:44)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked(sender:)), for: .touchUpInside)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(cancelButton)
        
        sexPicker.frame = CGRect(x:0, y:0, width:SCREENRECT.width, height:216)
        sexPicker.delegate = self
        sexPicker.dataSource = self
        sexPicker.backgroundColor = .white
    }
}
