//
//  RegistrationSecondVC+PickerView.swift
//  imCelebrity
//
//  Created by Paramita  on 14/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension RegistrationSecondVC : UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sexArray.count 
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sexArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let indexpath = IndexPath(row: 2, section: 0)
        let cell = registrationSecondTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.txtInput.text = sexArray[row]
    }
}
