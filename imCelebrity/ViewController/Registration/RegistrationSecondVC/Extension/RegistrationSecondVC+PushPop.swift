//
//  RegistrationSecondVC+PushPop.swift
//  imCelebrity
//
//  Created by Paramita  on 15/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
extension RegistrationSecondVC {
    
    func moveToOTPVC(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        nextViewController.phoneNumberForOTP = "8016266975"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func moveToRegistrationProfilePicUploadVC(){
        let registrationSecondVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoUploadedVC") as! ProfilePhotoUploadedVC
        self.navigationController?.pushViewController(registrationSecondVC, animated: true)
    }
    
    func popToPreviousVC(){
        self.navigationController?.popViewController(animated: true)
    }
}
