//
//  RegistrationSecondVC+TextField.swift
//  imCelebrity
//
//  Created by Paramita  on 14/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension RegistrationSecondVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(isTableViewUp == false){
            toogleTableViewPostion(changeConstantHeight: 250, isViewUp: true)
        }
        if textField.tag == 1 {
            textField.inputView = sexPicker
            textField.inputAccessoryView = accessaryView
        }
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            print("KEYBAORD HEIGHT:  \(keyboardHeight)")
            self.keybaordHeight = keyboardHeight
            self.registrationTableButtonConstraint.constant = (keyboardHeight + 5) - 61
        }
    }
    @objc func keyboardWillHide(_ notification: Notification) {
       self.registrationTableButtonConstraint.constant = 0
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag >= 0 && textField.tag <= 1 {
            let indexpath = IndexPath(row: textField.tag+2, section: 0)
            let cell = registrationSecondTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.becomeFirstResponder()
            return false
        }else if textField.tag == 2 {
            let indexpath = IndexPath(row: 5, section: 0)
            let cell = registrationSecondTableView.cellForRow(at: indexpath) as? AboutMeCell
            cell?.aboutMeTxtView.becomeFirstResponder()
            return false
        } else {
            self.view.endEditing(true)
            if(isTableViewUp == true){
                toogleTableViewPostion(changeConstantHeight: 0, isViewUp: false)
            }
            return true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let txtAfterUpdate = textField.text! as NSString
        let updateText: String = txtAfterUpdate.replacingCharacters(in: range, with: string) as String
        textInputAttributesArr[textField.tag].textFieldText = updateText
        
        return true
    }
    
    func toogleTableViewPostion(changeConstantHeight: CGFloat , isViewUp: Bool) {
        
        self.view.layoutIfNeeded()
        
        if isViewUp {
            isTableViewUp = true
            self.registrationTableButtonConstraint.constant = self.keybaordHeight
        }else{
            isTableViewUp = false
            self.registrationTableButtonConstraint.constant = 0
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
}

