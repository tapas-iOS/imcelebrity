//
//  RegistrationVC+TextView.swift
//  imCelebrity
//
//  Created by Paramita  on 18/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension RegistrationSecondVC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let indexpath = IndexPath(row: 5, section: 0)
        let cell = registrationSecondTableView.cellForRow(at: indexpath) as? AboutMeCell
        cell?.selectedLine.backgroundColor = UIColor.darkGray
        cell?.selectLineHeightConstraintOutlate.constant = 2
        //cell?.aboutMeTxtView.becomeFirstResponder()
        
       
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
         textView.inputAccessoryView = self.accessaryView
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if(isTableViewUp == true){
            toogleTableViewPostion(changeConstantHeight: 0, isViewUp: false)
        }
        
        let indexpath = IndexPath(row: 5, section: 0)
        let cell = registrationSecondTableView.cellForRow(at: indexpath) as? AboutMeCell
        cell?.selectedLine.backgroundColor = UIColor.lightGray
        cell?.selectLineHeightConstraintOutlate.constant = 1
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let txtAfterUpdate = textView.text! as NSString
        let updateText: String = txtAfterUpdate.replacingCharacters(in: range, with: text) as String
        textInputAttributesArr[4].textFieldText = updateText
        return true
    }
}
