//
//  RegistrationSecondVC.swift
//  imCelebrity
//
//  Created by Paramita  on 13/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class RegistrationSecondVC: UIViewController {
    
    @IBOutlet weak var registrationTableButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonOutletCross: UIButton!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var buttonOutletSkip: UIButton!
    @IBOutlet weak var buttonOutletDone: UIButton!
    
    
    let textCellId = "CellTextFieldModel"
    let logoCellId = "CellLogoCell"
    let sexPicker = UIPickerView()
    var accessaryView = UIView()
    let SCREENRECT = UIScreen.main.bounds
    @IBOutlet weak var registrationSecondTableView: UITableView!
    var textInputAttributesArr = Array<TextInputAttributes>()
    let sexArray = ["MALE", "FEMALE", "OTHER"]
    var isTableViewUp:Bool = false
    var keybaordHeight              = CGFloat()


    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.addingTheShadowOnView()
        super.viewDidLoad()
        self.registerNib()
        self.configureTextInput()
        self.setTextFieldAccessoryViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    

    func callForUpdate(){
        if(isTableViewUp == false){
            toogleTableViewPostion(changeConstantHeight: 250, isViewUp: true)
        }
    }

}
