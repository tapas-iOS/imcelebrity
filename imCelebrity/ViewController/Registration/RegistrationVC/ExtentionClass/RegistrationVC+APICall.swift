//
//  RegistrationVC+APICall.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
extension RegistrationVC {
    
    // Name, DOB, Mobile, Password
    
    func callSocialLogingApi() {
        if CommonClass.isConnectedToNetwork() == true {
            CommonClass.addLoading(view: self.view)
            ApiCalling.userSocialSignupCheck(name : self.social_name, email : self.social_email, imageUrl: self.social_image_url, socialID: self.social_id, sex: self.social_sex, socialType: self.social_type) { (loginModel) in
                CommonClass.removeLoading(view: self.view)
            }}else {
            self.showAutoCancelAlertWithMessage(message: Constant.NoInternentMessgae)
        }
    }
    
    func callEmaillRegistrationApi() {
        if CommonClass.isConnectedToNetwork() == true {
            CommonClass.addLoading(view: self.view)
            ApiCalling.userEmailSignupCheck(name : self.textInputAttributesArr[0].textFieldText, mobile : self.textInputAttributesArr[2].textFieldText, countryCode: self.counrtyPhoneCode, appType: Constant.appTypeiOS, DoB: self.textInputAttributesArr[1].textFieldText, password: self.textInputAttributesArr[3].textFieldText.sha1(), completion: {
                (result : Any?, error :Error? ) in
                CommonClass.removeLoading(view: self.view)
                
                if (result != nil){
                    print("Result:------- \(result!)")
                    if let _response = result as? NSDictionary {
                        let status = _response["response_code"] as! Int
                        if(status == 2000){
                            if let responseData = _response["response_data"] as? NSDictionary{
                                if let userProfileDetailsDic =  responseData["profile_details"] as? Dictionary<String, Any>{
                                    let objUserDetailsClass = UserDetailsObject()
                                    objUserDetailsClass.getUserDetails(userInfoDict: userProfileDetailsDic )
                                    }
                                }
                            
                            self.moveToOTPVC()
                            
                        }else  if(status == 5000){
                            let message = _response["response_message"] as! String
                            self.showAutoCancelAlertWithMessage(message: message)
                        }else{
                             self.showAutoCancelAlertWithMessage(message: "Server Error. Retry after sometime.")
                        }
                    }else{
                        
                        self.showAutoCancelAlertWithMessage(message: "Server Error. Retry after sometime.")
                    }
                }
            })
        }else {
            self.showAutoCancelAlertWithMessage(message: Constant.NoInternentMessgae)
        }
    }
    
    func isValidateUserDetails() -> Bool{
        let name = self.textInputAttributesArr[0].textFieldText
        let DoB  = self.textInputAttributesArr[1].textFieldText
        let mobile = self.textInputAttributesArr[2].textFieldText
        let password = self.textInputAttributesArr[3].textFieldText
        let encryptedPassword = self.textInputAttributesArr[3].textFieldText.sha1()
        
        print("User Details:--->  Name:: \(name) \n Date Of Birth:: \(DoB)\n Mobile:: \(mobile)\n Counry Code:: \(self.counrtyPhoneCode)\n Password:: \(password)\n Encrypted password:: \(encryptedPassword)")
        
        if name.isEmpty{
            self.commonAlertMessage(message: "Please enter your name.")
            return false
        }else if DoB.isEmpty{
            self.commonAlertMessage(message: "Please enter your Date of Birth.")
            return false
        }else if self.counrtyPhoneCode.isEmpty{
            self.commonAlertMessage(message: "Please select any country code")
            return false
        }else if mobile.isEmpty{
            self.commonAlertMessage(message: "Please enter your mobile number")
            return false
        }else if password.isEmpty {
            self.commonAlertMessage(message: "Please enter your password")
            return false
        }else if password.count < 7{
            self.commonAlertMessage(message: "Password must be gretter than 8 character")
            return false
        }else{
            print("All Validation Complete Successfully. Please Call Signup API.")
            return true
        }
    }
    
    
    
}
