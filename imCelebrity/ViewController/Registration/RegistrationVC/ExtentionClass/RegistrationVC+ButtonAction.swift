//
//  RegistrationVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit


extension RegistrationVC : CellButtonModelDelegate, CellSocialLoginModelDelegate {
    func saveButtonAction() {
       if  self.isValidateUserDetails(){
            self.callEmaillRegistrationApi()
        }
        
        //self.moveToOTPVC()
    }
    
    func facebookBtnClkEvent() {
        self.getFacebookLoginDetails()
    }
    
    func googlePlusBtnClkEvent() {
        self.configureGoogleSignInDelegate()
    }
}
extension RegistrationVC {
    
    @IBAction func loginButtonAction(_ sender: Any) {
        self.moveToLoginVC()
    }
    
    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.popToPreviousVC()
    }
    
    @objc func doneButtonClicked(sender:UIButton){
        print(accessaryView.tag)
        if accessaryView.tag == 1 {
            self.resignFirstResponder()
            self.view.endEditing(true)
            let indexpath = IndexPath(row: 2, section: 0)
            let cell = registrationTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.text = changeDateTimeFormat(inputDateTime: dobPicker.date, outPutTimeZone: NSTimeZone.local, outPutFormat: "dd MMMM yyyy")
            self.textInputAttributesArr[1].textFieldText = cell?.txtInput.text ?? ""
            
            let indexpath1 = IndexPath(row: 3, section: 0)
            let cell1 = registrationTableView.cellForRow(at: indexpath1) as? CellTextFieldModel
            cell1?.txtInput.becomeFirstResponder()
        }else {
            let indexpath1 = IndexPath(row: 4, section: 0)
            let cell1 = registrationTableView.cellForRow(at: indexpath1) as? CellTextFieldModel
            cell1?.txtInput.becomeFirstResponder()
        }
    }
    
    @objc func cancelButtonClicked(sender:UIButton){
        self.view.endEditing(true)
    }
}

extension RegistrationVC : CellTextFieldModelDelegate {
    func rightButtonClkAction(index: Int) {
        if index == 1 {
            let indexpath = IndexPath(row: 2, section: 0)
            let cell = registrationTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            cell?.txtInput.inputView = dobPicker
            cell?.txtInput.inputAccessoryView = accessaryView
            cell?.txtInput.becomeFirstResponder()
        }else if index == 3 {
            let indexpath: IndexPath = NSIndexPath(row: index+1, section: 0) as IndexPath
            let cell = registrationTableView.cellForRow(at: indexpath) as? CellTextFieldModel
            if isShowPassword {
                isShowPassword = false
                cell?.txtInput.isSecureTextEntry = true
                cell?.rightBtn.setImage(#imageLiteral(resourceName: "PasswordHide"), for: .normal)
            }else {
                isShowPassword = true
                cell?.txtInput.isSecureTextEntry = false
                cell?.rightBtn.setImage(#imageLiteral(resourceName: "PasswordShow"), for: .normal)
            }
        }
    }
    
    func leftButtonClkAction() {
        let searchCountryCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchCountryCodeVC") as! SearchCountryCodeVC
        searchCountryCodeVC.delegate = self
        searchCountryCodeVC.isShowCountryDetails = true
        
        self.navigationController?.pushViewController(searchCountryCodeVC, animated: true)
    }

}
extension RegistrationVC : SearchCountryCodeVCDelegate {
    func didSelect(country: Country) {
        guard let phoneCode = country.phoneCode else {return}
        let indexpath = IndexPath(row: 3, section: 0)
        let cell = registrationTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.leftBtn.setTitle(phoneCode, for: .normal) 
        
        self.counrtyPhoneCode = phoneCode
        self.countryCode = country.code ?? ""
    }
}
