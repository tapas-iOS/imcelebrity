//
//  RegistrationVC+Configuration.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
extension RegistrationVC {
    func registerNib() {
        self.registrationTableView.separatorStyle = .none
        self.registrationTableView.register(UINib(nibName: logoCellId, bundle: nil), forCellReuseIdentifier: logoCellId)
        self.registrationTableView.register(UINib(nibName: textCellId, bundle: nil), forCellReuseIdentifier: textCellId)
        self.registrationTableView.register(UINib(nibName: termAndConditionCellId, bundle: nil), forCellReuseIdentifier: termAndConditionCellId)
        self.registrationTableView.register(UINib(nibName: buttonCellId, bundle: nil), forCellReuseIdentifier: buttonCellId)
        self.registrationTableView.register(UINib(nibName: orLblCellId, bundle: nil), forCellReuseIdentifier: orLblCellId)
        self.registrationTableView.register(UINib(nibName: socialCellId, bundle: nil), forCellReuseIdentifier: socialCellId)
        
    }
    func configureTextInput() {
        textInputAttributesArr = [TextInputAttributes.init(placeholderText: "Name", textFieldText: "", keyboardType: .emailAddress, isSecureTextEntry: false, errorMessage: "Name should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false,isShowCountryCodes: false, isShowDropdownArrow: false), TextInputAttributes.init(placeholderText: "Date of Birth", textFieldText: "", keyboardType: .default, isSecureTextEntry: false, errorMessage: "Date of Birth should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: true,isShowCountryCodes: false, isShowDropdownArrow: false), TextInputAttributes.init(placeholderText: "Mobile Number", textFieldText: "", keyboardType: .numberPad, isSecureTextEntry: false, errorMessage: "Mobile Number should not be empty", returnKey: .next, isViewPasswordButton: false, isShowCalenderButton: false,isShowCountryCodes: true, isShowDropdownArrow: false), TextInputAttributes.init(placeholderText: "Password", textFieldText: "", keyboardType: .default, isSecureTextEntry: true, errorMessage: "Password should not be empty", returnKey: .done, isViewPasswordButton: true, isShowCalenderButton: false, isShowCountryCodes: false, isShowDropdownArrow: false)]
    }
    func setTextFieldAccessoryViews(){
        accessaryView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.width, height:44)
        accessaryView.backgroundColor = .red
        
        let doneButton : UIButton = UIButton()
        doneButton.frame = CGRect(x:SCREENRECT.width-60, y:0, width:60, height:44)
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonClicked(sender:)), for: .touchUpInside)
        doneButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(doneButton)
        
        let cancelButton : UIButton = UIButton()
        cancelButton.frame = CGRect(x:10, y:0, width:60, height:44)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked(sender:)), for: .touchUpInside)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        accessaryView.addSubview(cancelButton)
        
        dobPicker.frame = CGRect(x:0, y:0, width:SCREENRECT.width, height:216)
        dobPicker.datePickerMode = .date
        dobPicker.maximumDate = Date()
        dobPicker.backgroundColor = .white
    }
    func getCounrtyPhoneCode(){
        let currentLocale = NSLocale.current
        guard let countryCode = currentLocale.regionCode else {return}
        self.counrtyPhoneCode = getCountryPhonceCode(countryCode) ?? ""
        self.countryCode = getCountryCode(countryCode) ?? ""
        
        let indexpath = IndexPath(row: 3, section: 0)
        let cell = registrationTableView.cellForRow(at: indexpath) as? CellTextFieldModel
        cell?.leftBtn.setTitle(counrtyPhoneCode, for: .normal)
    }

    func getCountryPhonceCode(_ countryCode : String) -> String?{
        let countries = CommonClass.getCountries()
        guard let index = countries.firstIndex(where: {$0.code == countryCode}) else {return nil}
        guard let phoneCode = countries[index].phoneCode else {return nil}
        return phoneCode
    }
    func getCountryCode(_ countryCode : String) -> String?{
        let countries = CommonClass.getCountries()
        guard let index = countries.firstIndex(where: {$0.code == countryCode}) else {return nil}
        guard let code = countries[index].code else {return nil}
        return code
    }
}
