//
//  RegistrationVC+CustomFunction.swift
//  imCelebrity
//
//  Created by Paramita  on 12/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseAuth
import FacebookLogin
import FBSDKLoginKit

extension RegistrationVC {
    
    
    func getFacebookLoginDetails(){
        if let accessToken = FBSDKAccessToken.current(){
            print("\(accessToken)")
            self.getFBSDKAccessToken { (success, accessToken, resultDict, error) in
                //"fields": "id, name, first_name, last_name, picture.type(large), email"
            }
        }else {
            let loginManager = LoginManager()
            loginManager.logIn(readPermissions: [.email], viewController: self){ loginResult in
                switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success:
                    self.getFBSDKAccessToken { (success, accessToken, resultDict, error) in
                        print(resultDict)
                        //Api call...
                        self.social_name = resultDict["name"]! as! String
                        self.social_email = ""//resultDict["email"]! as! String
                        self.social_image_url = ((resultDict["picture"]! as! NSDictionary)["data"] as! NSDictionary)["url"] as! String
                        self.social_id = resultDict["id"]! as! String
                        self.social_type = "FACEBOOK"
                        
                        self.callSocialLogingApi()
                    }
                }
            }
        }
    }
    
    func getGoogleLoginDetails(userDetails: GIDGoogleUser) {
        self.social_name = userDetails.profile.name
        self.social_email = ""//userDetails.profile.email
        self.social_image_url = userDetails.profile.imageURL(withDimension: 0).absoluteString
        self.social_id = userDetails.userID
        self.social_type = "GOOGLE"
        
        self.callSocialLogingApi()
    }
    
}
