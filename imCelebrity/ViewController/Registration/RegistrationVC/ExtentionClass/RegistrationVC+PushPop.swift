//
//  RegistrationVC+PushPop.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
extension RegistrationVC {
    
    func moveToOTPVC(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        nextViewController.phoneNumberForOTP = "8016266975"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func moveToRegistrationSecondVC(){
        let registrationSecondVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationSecondVC") as! RegistrationSecondVC
        self.navigationController?.pushViewController(registrationSecondVC, animated: true)
        
    }
    
    func moveToLoginVC(){
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    func moveToRegistrationProfilePicUploadVC(){
        let registrationSecondVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoUploadedVC") as! ProfilePhotoUploadedVC
        self.navigationController?.pushViewController(registrationSecondVC, animated: true)
    }
    
    func popToPreviousVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
