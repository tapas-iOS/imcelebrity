//
//  RegistrationVC+TableView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit
extension RegistrationVC :UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textInputAttributesArr.count + 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: logoCellId, for: indexPath) as! CellLogoCell
            return cell
        }else if(indexPath.row > 0 && indexPath.row < textInputAttributesArr.count+1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath) as! CellTextFieldModel
            let attText = textInputAttributesArr[indexPath.row-1]
            cell.populateAllData(attributes: attText)
            cell.rightBtn.tag = indexPath.row-1
            cell.txtInput.delegate = self
            cell.txtInput.tag = indexPath.row-1
            cell.delegate = self 
            return cell
        }else if (indexPath.row == textInputAttributesArr.count+1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: termAndConditionCellId, for: indexPath) as! CellTermAndConditionModel
            return cell
        }else if (indexPath.row == textInputAttributesArr.count+2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: buttonCellId, for: indexPath) as!
            CellButtonModel
            cell.cellButtonOutlet.setTitle("SIGN UP", for: .normal)
            cell.delegate = self 
            return cell
        }else if (indexPath.row == textInputAttributesArr.count+3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: orLblCellId, for: indexPath) as! CellOrLable
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: socialCellId, for: indexPath) as! CellSocialLoginModel
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return 160
        }else {
            return 60
        }
    }
}
