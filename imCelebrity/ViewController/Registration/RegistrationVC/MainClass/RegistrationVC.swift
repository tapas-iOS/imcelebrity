//
//  RegistrationVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 09/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController {
    

    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBOutlet weak var registrationTableView: UITableView!{
        didSet{
            registrationTableView.delegate = self
            registrationTableView.dataSource = self
        }
    }
    let logoCellId = "CellLogoCell"
    let socialCellId = "CellSocialLoginModel"
    let textCellId = "CellTextFieldModel"
    let orLblCellId = "CellOrLable"
    let buttonCellId = "CellButtonModel"
    let forgotCellId = "CellForgotLabelModel"
    var termAndConditionCellId = "CellTermAndConditionModel"
    var textInputAttributesArr = Array<TextInputAttributes>()
    let dobPicker = UIDatePicker()
    let isdPicker = UIPickerView()
    var accessaryView = UIView()
    let countryAccessaryView = UIView()
    let SCREENRECT = UIScreen.main.bounds
    var isShowPassword = false
    lazy var counrtyPhoneCode = ""
    lazy var countryCode = ""
    
    //Social..
    var social_name : String = ""
    var social_email : String = ""
    var social_id : String = ""
    var social_sex : String = ""
    var social_type : String = ""
    var social_image_url : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerNib()
        self.configureTextInput()
        self.setTextFieldAccessoryViews()
        self.getCounrtyPhoneCode()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
}
