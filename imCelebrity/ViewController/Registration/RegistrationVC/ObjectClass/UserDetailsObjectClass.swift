//
//  UserDetailsObjectClass.swift
//  imCelebrity
//
//  Created by Paramita  on 01/06/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class UserDetailsObjectClass: NSObject {
    
    var authToken = String()
    var profile_type = String()
    var city = String()
    var country = String()
    var country_code  = String()
    var userDoB = Int()
    var email = String()
    var userMobile = Int()
    var name = String()
    var profile_pic = String()
    var quickblox_status = String()
    var sex = String()
    var user_id = String()
    var verification_status = String()
    
    public func getUserDetails(userInfoDict: Dictionary<String, Any>?) {
        fetchUserInfo(userInfoDict: userInfoDict)
    }
    
    public func fetchUserInfo(userInfoDict: Dictionary<String, Any>?) {
        
        if let token =  userInfoDict!["authtoken"] as? String{
           self.authToken = token
        }
        
        if let countryCode =  userInfoDict!["country_code"] as? String{
            self.country_code = countryCode
        }
        
        if let dob =  userInfoDict!["dob"] as? Int{
            self.userDoB = dob
        }
        
        if let mobile =  userInfoDict!["mobile"] as? Int{
            self.userMobile = mobile
        }
        
        if let username =  userInfoDict!["name"] as? String{
            self.name = username
        }
        
        if let id =  userInfoDict!["user_id"] as? String{
             self.user_id = id
        }
        
        self.storeValueUsedefault()
    }
    
    func storeValueUsedefault() {
       
         UserDefaults.standard.set(self.authToken, forKey: Constant.UserDefaultKeyName.AccessToken)
        UserDefaults.standard.set(self.country_code, forKey: Constant.UserDefaultKeyName.CountryCode)
         UserDefaults.standard.set(self.userMobile, forKey: Constant.UserDefaultKeyName.PhoneNumber)
         UserDefaults.standard.set(self.user_id, forKey: Constant.UserDefaultKeyName.UserID)
        UserDefaults.standard.set(self.name, forKey: Constant.UserDefaultKeyName.UserName)
        
        UserDefaults.standard.synchronize()
        
    }

}
