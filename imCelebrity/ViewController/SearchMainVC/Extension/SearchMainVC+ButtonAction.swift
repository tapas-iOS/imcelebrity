//
//  SearchMainVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 17/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension SearchMainVC {
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
