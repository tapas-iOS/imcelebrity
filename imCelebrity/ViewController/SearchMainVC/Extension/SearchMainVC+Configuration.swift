//
//  SearchMainVC+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 17/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit
import Parchment

extension SearchMainVC {
    func loadVC() {
        let storyboard = UIStoryboard.searchStoryBoard()
        videoList = storyboard.instantiateViewController(withIdentifier: "SearchVideoVC") as! SearchVideoVC
        snapList = storyboard.instantiateViewController(withIdentifier: "SearchSnapVC") as! SearchSnapVC
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [videoList, snapList])
        pagingViewController.indicatorColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        pagingViewController.backgroundColor = UIColor(red: 253/255, green: 253/255, blue: 253/255, alpha: 1.0)
        pagingViewController.selectedBackgroundColor = UIColor(red: 253/255, green: 253/255, blue: 253/255, alpha: 1.0)
        pagingViewController.textColor = UIColor.black
        pagingViewController.selectedTextColor = UIColor.black
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        pagingViewController.selectedFont = UIFont.boldSystemFont(ofSize: 15)
        
        // Make sure you add the PagingViewController as a child view
        // controller and constrain it to the edges of the view.
        addChild(pagingViewController)
        bgView.addSubview(pagingViewController.view)
        bgView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
    }
    func fetchVideoJSON(){
        if let path = Bundle.main.path(forResource: "Video", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print("JSON Result is: \(jsonResult)")
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    let resultArray = jsonResult["result"] as! Array<[String: Any]>
                    self.videoList.arrVideoListing = DashbaordDetailsObjectClass.fetchDashboardDetails(dashbaordDetailsArray: resultArray) as! [DashbaordDetailsObjectClass]
//                    self.videoList.tableView.reloadData()
                }
            } catch {
                // handle error
            }
        }
    }
    func fetchSnapJSON(){
        if let path = Bundle.main.path(forResource: "Snap", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print("JSON Result is: \(jsonResult)")
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    let resultArray = jsonResult["result"] as! Array<[String: Any]>
                    self.snapList.arrSnapListing = DashbaordDetailsObjectClass.fetchDashboardDetails(dashbaordDetailsArray: resultArray) as! [DashbaordDetailsObjectClass]
//                    self.snapList.tableView.reloadData()
                }
            } catch {
                // handle error
            }
        }
    }
}
