//
//  SearchMainVC.swift
//  imCelebrity
//
//  Created by Paramita  on 17/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class SearchMainVC: UIViewController {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var bgView: UIView!
   
    var videoList = SearchVideoVC()
    var snapList = SearchSnapVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVC()
        self.fetchVideoJSON()
        self.fetchSnapJSON()
    }
    
    
    

}
