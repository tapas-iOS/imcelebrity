//
//  SearchVideoVC.swift
//  imCelebrity
//
//  Created by Paramita  on 17/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class SearchVideoVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var arrVideoListing = Array<DashbaordDetailsObjectClass>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    

}

extension SearchVideoVC: UITableViewDelegate, UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int  {
        return self.arrVideoListing.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "DashboardListedCell", for: indexPath) as? DashboardListedCell
//        let obj = arrVideoListing[indexPath.section]
        cell?.selectionStyle = .none
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "DashboardSectionHeaderCell") as? DashboardSectionHeaderCell
        headerCell?.backgroundColor = UIColor.white
        headerCell?.tag = section
        headerCell?.buttonOutletAllButton.addTarget(self, action: #selector(self.showAllButtonAction(sender:)), for: .touchUpInside)
        let obj = arrVideoListing[section]
        headerCell?.headertitleLbl.text = obj.name
        headerCell?.allLbl.text = "All " + obj.typeString
        headerCell?.seuupUI()
        return headerCell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
}

extension SearchVideoVC {
    @objc func showAllButtonAction(sender : UIButton) {
        
    }
}
