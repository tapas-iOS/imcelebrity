

import UIKit

extension SettingsVC{
    /*
     SettingsBulletIcon
     SettingsContactUsIcon
     SettingsDeleteAccountIcon
     SettingsHelpUsIcon
     SettingsProfilePrivecyIcon
     SettingsTermsIcon
     SettingsToogleOffIcon
     SettingsToogleOnIcon
     */
    
    func CreateProfileSettingsArray(){
        self.arrayProfilesSettings = [
            [
                "settingsImage": "SettingsProfilePrivecyIcon",
                "settingsText": "Profile Privacy"
            ],
            [
                "settingsImage": "SettingsContactUsIcon",
                "settingsText": "Contact Us"
            ],
            [
                "settingsImage": "SettingsHelpUsIcon",
                "settingsText": "Help and FAQ"
            ],
            [
                "settingsImage": "SettingsTermsIcon",
                "settingsText": "Terms and Conditions"
            ],
            [
                "settingsImage": "SettingsDeleteAccountIcon",
                "settingsText": "Delete Account"
            ]
        ]
    }
    
    func CreateNotificationSettingsArray(){
        let connectsObject = SettingsObjectClass()
        connectsObject.settingsTitle = "Show Connect List"
        self.arrayNotificationSettings.append(connectsObject)

        let postObject = SettingsObjectClass()
        postObject.settingsTitle = "Show Post List"
        self.arrayNotificationSettings.append(postObject)
    }
    
}
