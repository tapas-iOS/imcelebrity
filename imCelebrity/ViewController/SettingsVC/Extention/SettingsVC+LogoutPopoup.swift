//
//  SettingsVC+LogoutPopoup.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension SettingsVC : LogoutCustomViewDelegate {
    
    func noBtnClkEvent() {
        appDelegate.customLogoutView.isHidden = true
        self.removeAnimate(popupView: appDelegate.customLogoutView)
    }
    
    func yesBtnClkEvent() {
        appDelegate.customLogoutView.isHidden = true
        UserDefaults.standard.set(nil, forKey: Constant.UserDefaultKeyName.AccessToken)
        self.removeAnimate(popupView: appDelegate.customLogoutView)
        
        self.moveToDasbaordVC()
    }
    
    func moveToDasbaordVC(){
        let dashboardVC = UIStoryboard.otherStoryBoard().instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.navigationController?.pushViewController(dashboardVC, animated: true)
    }
    
    func showAnimate(popupView:UIView)
    {
        popupView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        popupView.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            popupView.alpha = 1.0
            popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate(popupView:UIView)
    {
        UIView.animate(withDuration: 0.25, animations: {
            popupView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            popupView.alpha = 0.0
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                //                    popupView.removeFromSuperview()
            }
        });
    }
    
}
