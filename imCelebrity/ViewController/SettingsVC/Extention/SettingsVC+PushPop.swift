//
//  SettingsVC+PushPop.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 05/05/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension SettingsVC {
    func moveToPrivacyPolicyVC(){
        let privacyVC = UIStoryboard.settingsStoryBoard().instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        self.navigationController?.pushViewController(privacyVC, animated: true)
    }
}
