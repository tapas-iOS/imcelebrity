//
//  SettingsVC+TableView.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import  UIKit

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  section == 0{
            return self.arrayNotificationSettings.count
        }else{
            return self.arrayProfilesSettings.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchCell", for: indexPath) as! SettingsSwitchCell
            cell.selectionStyle = .none
            let object = self.arrayNotificationSettings[indexPath.row]
            cell.delegate = self
            cell.configureCell(indexRow: indexPath.row, object: object)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! SettingsCell
            cell.selectionStyle = .none
            let settingsDic = self.arrayProfilesSettings[indexPath.row] as Dictionary<String,Any>
            cell.configureCell(indexRow: indexPath.row, dic: settingsDic)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                self.moveToPrivacyPolicyVC()
                break
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 65
        }else{
            return 75
        }
    }
}

extension SettingsVC: SettingsSwitchCellDelegate{
    func switchToogleButtonAction(tagIndex: Int, status: Bool){
        let obj = self.arrayNotificationSettings[tagIndex]
        obj.switchStatus = status
    }
}
