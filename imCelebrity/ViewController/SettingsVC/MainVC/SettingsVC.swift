//
//  SettingsVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    @IBOutlet weak var settingsTableView: UITableView!{
        didSet{
            settingsTableView.delegate = self
            settingsTableView.dataSource = self
        }
    }
    
    @IBOutlet weak var buttonOutletLogout: UIButton!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var viewNavigationHeader: UIView!
    @IBOutlet weak var notificationButtonOutlet: UIButton!
    
    var arrayProfilesSettings = Array<Dictionary<String,Any>>()
    var arrayNotificationSettings = Array<SettingsObjectClass>()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addingTheShadowOnView()
        self.CreateProfileSettingsArray()
        self.CreateNotificationSettingsArray()
        self.settingsTableView.reloadData()
    }
    
    func addingTheShadowOnView(){
        self.viewFooter.addShadow(location: VerticalLocation.top)
         self.viewNavigationHeader.addShadow(location: VerticalLocation.bottom)
    }
}
