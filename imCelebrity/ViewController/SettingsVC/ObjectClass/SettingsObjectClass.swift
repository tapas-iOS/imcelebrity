//
//  SettingsObjectClass.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 28/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class SettingsObjectClass: NSObject {
    var settingsTitle           = String()
    var switchStatus: Bool = false
    var settingsIcon: UIImage = #imageLiteral(resourceName: "SettingsBulletIcon")
}
