//
//  SettingsCell.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {
    
    @IBOutlet weak var imageViewSettingsIcon: UIImageView!
    @IBOutlet weak var lableSettingsDetails: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(indexRow: Int, dic: Dictionary<String,Any>){
        let strImageName = dic["settingsImage"] as! String
        let strTitle = dic["settingsText"] as! String

        self.imageViewSettingsIcon.image = UIImage(named: strImageName)
        self.lableSettingsDetails.text = strTitle
    }

}
