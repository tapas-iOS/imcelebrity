//
//  SettingsSwitchCell.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

protocol SettingsSwitchCellDelegate: class {
    func switchToogleButtonAction(tagIndex: Int, status: Bool)
}

class SettingsSwitchCell: UITableViewCell {
    
    @IBOutlet weak var imageViewSettingsIcon: UIImageView!
    @IBOutlet weak var lableSettingsDetails: UILabel!
    @IBOutlet weak var toogleSwitchSettingsIcon: UISwitch!
    weak var delegate: SettingsSwitchCellDelegate?


    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageViewSettingsIcon.image = #imageLiteral(resourceName: "SettingsBulletIcon")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureCell(indexRow: Int, object: SettingsObjectClass){
        self.lableSettingsDetails.text = object.settingsTitle
        self.toogleSwitchSettingsIcon.tag = indexRow
        self.toogleSwitchSettingsIcon.setOn(object.switchStatus, animated: true)
    }
    func configurePrivacyCell(indexRow: Int, obj: PrivacyPolicyTextAttribute){
        self.lableSettingsDetails.text = obj.privacyName
        self.toogleSwitchSettingsIcon.tag = indexRow
        self.toogleSwitchSettingsIcon.setOn(obj.privacyStatus, animated: true)
    }
    func configureNotificationSettingsCell(indexRow: Int, obj: NotificationSettingsTextAttribute){
        self.lableSettingsDetails.text = obj.notificationName
        self.toogleSwitchSettingsIcon.tag = indexRow
        self.toogleSwitchSettingsIcon.setOn(obj.notificationStatus, animated: true)
    }
    
    
    @IBAction func swtichAction(_ sender: UISwitch) {
        if sender.isOn{
            delegate?.switchToogleButtonAction(tagIndex: sender.tag, status: true)
        }else{
            delegate?.switchToogleButtonAction(tagIndex: sender.tag, status: false)
        }
    }
}
