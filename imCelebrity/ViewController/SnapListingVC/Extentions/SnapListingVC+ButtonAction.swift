//
//  SnapListingVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import UIKit

extension SnapListingVC{
    @IBAction func profileButtonAction(_ sender: UIButton) {
    }
    @IBAction func notificationButtonAction(_ sender: UIButton) {
        let notificationVC = UIStoryboard.NotificationsStoryBoard().instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func homeButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
