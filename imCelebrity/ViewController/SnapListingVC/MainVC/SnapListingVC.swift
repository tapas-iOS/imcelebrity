//
//  SnapListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class SnapListingVC: UIViewController {

    @IBOutlet weak var buttonOuletNotification: UIButton!
    @IBOutlet weak var buttonOutletHome: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    var footerView = FooterCustomView()
    var plusButtonPopup = PlusButtonOptionPopup()
    
    var fansSnapVC = FansSnapListingVC()
    var celebSnapVC = CelebritySnapListingVC()
    var popularSnapVC = PopularSnapListingVC()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVC()
        self.loadPlusButtonPopupView()
        self.loadCustomView()
    }

}
