//
//  CelebritySnapListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class CelebritySnapListingVC: UIViewController {
    
    @IBOutlet weak var celebsCollectionView: UICollectionView!{
        didSet{
            celebsCollectionView.delegate = self
            celebsCollectionView.dataSource = self
        }
    }
    
    var footerIdentifier = "FooterCellIdentifire"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //celebsCollectionView?.contentInsetAdjustmentBehavior = .always
        self.loadCustionCollectionCell()
    }
    
    func loadCustionCollectionCell(){
        self.celebsCollectionView.register(UINib(nibName: "SnapListingCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SnapListingCollectionCell")
        celebsCollectionView.register(CollectionViewFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerIdentifier)
    }
}
