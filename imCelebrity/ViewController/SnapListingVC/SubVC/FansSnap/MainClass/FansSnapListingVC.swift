//
//  FansSnapListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class FansSnapListingVC: UIViewController {
    
    @IBOutlet weak var fansCollectionView: UICollectionView!{
        didSet{
            fansCollectionView.delegate = self
            fansCollectionView.dataSource = self
        }
    }
    
    
    var footerIdentifier = "FooterCellIdentifire"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCustionCollectionCell()
    }

    func loadCustionCollectionCell(){
        self.fansCollectionView.register(UINib(nibName: "SnapListingCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SnapListingCollectionCell")
        fansCollectionView.register(CollectionViewFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerIdentifier)
    }

    
}
