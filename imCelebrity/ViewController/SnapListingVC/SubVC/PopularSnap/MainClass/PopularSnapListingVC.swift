//
//  PopularSnapListingVC.swift
//  imCelebrity
//
//  Created by Weaver web 5 on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

class PopularSnapListingVC: UIViewController {
    @IBOutlet weak var popularCollectionView: UICollectionView!{
        didSet{
            popularCollectionView.delegate = self
            popularCollectionView.dataSource = self
        }
    }
    
    
    var footerIdentifier = "FooterCellIdentifire"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCustionCollectionCell()
    }
    
    func loadCustionCollectionCell(){
        self.popularCollectionView.register(UINib(nibName: "SnapListingCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SnapListingCollectionCell")
        popularCollectionView.register(CollectionViewFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerIdentifier)
    }

}
