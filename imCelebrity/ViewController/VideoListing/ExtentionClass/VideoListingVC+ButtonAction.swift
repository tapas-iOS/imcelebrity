//
//  VideoListingVC+ButtonAction.swift
//  imCelebrity
//
//  Created by Paramita  on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension VideoListingVC {
    @IBAction func homeBtnACtion(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func notificationBtnAction(_ sender: Any) {
        let notificationVC = UIStoryboard.NotificationsStoryBoard().instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
}
