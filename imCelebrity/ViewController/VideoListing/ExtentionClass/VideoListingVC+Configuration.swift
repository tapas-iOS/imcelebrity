//
//  VideoListingVC+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 26/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import Foundation
import Parchment

extension VideoListingVC{
    
    func loadVC() {
        let storyboard = UIStoryboard.videoStoryBoard()
        fansVideoVC = storyboard.instantiateViewController(withIdentifier: "FansVideoListingVC") as! FansVideoListingVC
        celebVideoVC = storyboard.instantiateViewController(withIdentifier: "CelebrityVideoListingVC") as! CelebrityVideoListingVC
        popularVideoVC = storyboard.instantiateViewController(withIdentifier: "PopularVideoListingVC") as! PopularVideoListingVC
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [celebVideoVC, popularVideoVC, fansVideoVC])
        pagingViewController.indicatorColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        pagingViewController.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.selectedBackgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pagingViewController.textColor = UIColor.black
        pagingViewController.selectedTextColor = UIColor.black
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        pagingViewController.selectedFont = UIFont.boldSystemFont(ofSize: 15)
        
        let count = CGFloat(pagingViewController.viewControllers.count)
        let width = self.view.frame.width/count
        pagingViewController.menuItemSize = .fixed(width: width, height: 40)
        
        // Make sure you add the PagingViewController as a child view
        // controller and constrain it to the edges of the view.
        addChild(pagingViewController)
        bgView.addSubview(pagingViewController.view)
        bgView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
    }
    
    func loadPlusButtonPopupView() {
        self.loadPlusButttonCustomView(popupView: plusButtonPopup)
        
    }

    func showFooterPlusView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.plusButtonPopup.heightConstraintOutlate.constant = 200
            self.plusButtonPopup.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideFooterPlusView() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.plusButtonPopup.heightConstraintOutlate.constant = 0
            self.plusButtonPopup.alpha = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

}
