//
//  VideoListingVC+FooterView.swift
//  imCelebrity
//
//  Created by Paramita  on 26/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension VideoListingVC : FooterCustomViewDelegate {
    func profileBtnClkEvent() {
        
    }
    
    func searchBtnClkEvent() {
        let searchVC = UIStoryboard.searchStoryBoard().instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func messageBtnClkEvent() {
        
    }
    
    func callBtnClkEvent() {
        
    }
    
    
    func loadCustomView() {
        self.view.loadFooterCustomView(footerView: footerView)
        self.footerView.delegate = self
        self.setupUIFooterView()
    }
    func setupUIFooterView() {
        self.footerView.shadowViewBgColor = UIColor.black
        self.footerView.middleImg = #imageLiteral(resourceName: "VideoAddIcon")
        self.footerView.middleBgImg = #imageLiteral(resourceName: "VideoFooterBgIcon")
        self.footerView.leftFirstImg = #imageLiteral(resourceName: "VideoConnectsIcon")
        self.footerView.leftSecondImg = #imageLiteral(resourceName: "VideoSearchIcon")
        self.footerView.rightFirstImg = #imageLiteral(resourceName: "VideoPhoneIcon")
        self.footerView.rightSecondImg = #imageLiteral(resourceName: "ViideoMessageIcon")
    }
    
    func middleBtnClkEvent() {
//        self.showFooterPlusView()
    }
}
