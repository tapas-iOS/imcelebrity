//
//  VideoListingVC.swift
//  ImCelebrity
//
//  Created by Weaver web 5 on 03/04/19.
//  Copyright © 2019 Brainium InfoTech. All rights reserved.
//

import UIKit

class VideoListingVC: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
    
    var footerView = FooterCustomView()
    var plusButtonPopup = PlusButtonOptionPopup()
    
    var fansVideoVC = FansVideoListingVC()
    var celebVideoVC = CelebrityVideoListingVC()
    var popularVideoVC = PopularVideoListingVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVC()
        self.loadPlusButtonPopupView()
        self.loadCustomView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
