//
//  FanVideoListing+Configuration.swift
//  imCelebrity
//
//  Created by Paramita  on 27/04/19.
//  Copyright © 2019 paramitapas. All rights reserved.
//

import UIKit

extension FansVideoListingVC {
    func configureCollectionView() {
        collectionView.register(CollectionViewFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerIdentifier)
        self.collectionView.register(UINib(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
    }
}
